<?php
  session_start();
  $connection = mysql_connect('localhost', 'root');
  // Selecting Database
  $con = mysqli_connect("localhost","root","","ehrms");
  $db = mysql_select_db("ContentManagement", $connection);
  $error = "";

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--MODAL-->
    <script src="lib/js/jquery-1.11.2.min.js"></script>
    <script src="lib/js/bootstrap.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      $("#myModal").on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);  // Button that triggered the modal
            var titleData = button.data('title'); // Extract value from data-* attributes
            $(this).find('.modal-title').text(titleData + ' Form');
        });
    });
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./../Supervisor.php">eBiZolution | Supervisor</a>
            </div>
            <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="../logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

            

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                    echo "<li> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>





            <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="../Supervisor.php"><i class="fa fa-home"></i> Back to Main Menu</a>
                        </li>
                        <li>
                            <a href="OrganizationDescription.php"> Organization Description</a>
                        </li>
                        <li>
                            <a href="ServiceProvided.php"> Service Provided</a>
                        </li>
                        <li>
                            <a href="CompanyPhilosophy.php"> Company Philosophy</a>
                        </li>
                        <li>
                            <a href="NatureOfEmployment.php"> Nature of Employment</a>
                        </li>
                        <li>
                            <a href="Recruitment.php"> Recruitment</a>
                        </li>
                        <li>
                            <a href="Attendance.php"> Attendance</a>
                        </li>
                        <li>
                            <a href="RecordingAndReporting.php"> Recording & Reporting</a>
                        </li>
                        <li>
                            <a href="Holidays.php"> Holidays</a>
                        </li>
                        <li>
                            <a href="Overtime.php"> Overtime</a>
                        </li>
                        <li>
                            <a href="LeavingForOfficialBusiness.php"> Leaving for Official Business</a>
                        </li>
                        <li>
                            <a href="OfficialLeaves.php"> Offical Leaves & AWOL</a>
                        </li>
                        <li>
                            <a href="AbsenceWithPay.php"> Absence with Pay</a>
                        </li>
                        <li>
                            <a href="RulesOnNotification.php"> Rules on Notification</a>
                        </li>
                        <li>
                            <a href="PersonnelFiles.php"> Personnel Files</a>
                        </li>
                        <li>
                            <a href="SalaryAdministration.php"> Salary Administration</a>
                        </li>
                        <li>
                            <a href="EmployeeBenefits.php"> Employee Benefits</a>
                        </li>
                        <li>
                            <a href="WorkingCondition.php"> Working Condition</a>
                        </li>
                        <li>
                            <a href="Sources.php"> Sources</a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header"> OVERTIME</h1>
                    </div>
                        
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            <?php
                      $result = mysql_query("SELECT * FROM overtime",$connection);
                            
                    while($row = mysql_fetch_array($result)){ 

                        echo $row['Overtime1'] . "<br><br>";
                        echo $row['Overtime2'] . "<br><br>";
                        echo $row['Overtime3'] . "<br><br>";
                        echo $row['Overtime4'] . "<br><br>";
                        echo $row['Overtime5'] . "<br><br>";
                        echo $row['Overtime6'] . "<br><br>";

                      echo "<b>Procedure for Overtime Work</b><br><br>";
                        echo $row['POW1'] . "<br><br>";
                        echo $row['POW2'] . "<br><br>";

                      echo "<b>Computation:</b><br><br>";
                        echo $row['Comp1'] . "<br><br>";
                        echo $row['Comp2'] . "<br><br>";
                        echo $row['Comp3'] . "<br><br>";
                        echo $row['Comp4'] . "<br><br>";
                        echo $row['Comp5'] . "<br><br>";
                        echo $row['Comp6'] . "<br>";
                        echo $row['Comp7'] . "<br>";
                        echo $row['Comp8'] . "<br>";
                        echo $row['Comp9'] . "<br><br>";
                        echo $row['Comp10'] . "<br><br>";
                        echo $row['Comp11'] . "<br><br>";
                        echo $row['Comp12'] . "<br><br>";

                       echo "<b>Computation:</b><br><br>";
                        echo $row['Comp13'] . "<br><br>";
                        echo $row['Comp14'] . "<br><br>";
                        echo $row['Comp15'] . "<br><br>";
                        echo $row['Comp16'] . "<br>";
                        echo $row['Comp17'] . "<br>";
                        echo $row['Comp18'] . "<br>";
                        echo $row['Comp19'] . "<br><br>";
                        echo $row['Comp20'] . "<br><br>";
                        echo $row['Comp21'] . "<br><br>";
                        echo $row['Comp22'] . "<br><br>";
                        echo $row['Comp23'] . "<br><br>";

                      echo "<b>Computation:</b><br><br>";
                        echo $row['Comp24'] . "<br>";
                        echo $row['Comp25'] . "<br>";
                        echo $row['Comp26'] . "<br><br>";
                        echo $row['Comp27'] . "<br><br>";
                        echo $row['Comp28'] . "<br><br>";
                        echo $row['Comp29'] . "<br><br>";
                        echo $row['Comp30'] . "<br><br>";

                      echo "<b>Computation:</b><br><br>";
                        echo $row['Comp31'] . "<br><br>";
                        echo $row['Comp32'] . "<br>";
                        echo $row['Comp33'] . "<br>";
                        echo $row['Comp34'] . "<br>";
                        echo $row['Comp35'] . "<br>";
                        echo $row['Comp36'] . "<br>";
                        echo $row['Comp37'] . "<br><br>";
                        echo $row['Comp38'] . "<br><br>";
                        echo $row['Comp39'] . "<br><br>";
                        echo $row['Comp40'] . "<br><br>";
                        echo $row['Comp41'] . "<br><br>";
                       
                       echo "<b>Computation:</b><br><br>";
                        echo $row['Comp42'] . "<br>";
                        echo $row['Comp43'] . "<br>";
                        echo $row['Comp44'] . "<br>";
                        echo $row['Comp45'] . "<br>";
                        echo $row['Comp46'] . "<br>";
                        echo $row['Comp47'] . "<br><br>";
                        echo $row['Comp48'] . "<br>";
                        echo $row['Comp49'] . "<br>";
                        echo $row['Comp50'] . "<br><br>";   

                      }

                  ?>
                        </div>

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--MODAL-->
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Window</h4>
                </div>
                    <div class="modal-body">
                        

                        <?php
                       $result = mysql_query("SELECT * FROM overtime",$connection);
while($row = mysql_fetch_array($result)){

  
echo "   <form name='input' method='post'> ";
echo"      <label><font face='tahoma'>Overtime</font>&nbsp;&nbsp;</label><br> 
           <textarea cols='80' rows='10' name='Overtime1' id='Overtime1' class='form-control' required>" . $row['Overtime1'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Overtime2' id='Overtime2' class='form-control' required>" . $row['Overtime2'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Overtime3' id='Overtime3' class='form-control' required>" . $row['Overtime3'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Overtime4' id='Overtime4' class='form-control' required>" . $row['Overtime4'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Overtime5' id='Overtime5' class='form-control' required>" . $row['Overtime5'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Overtime6' id='Overtime6' class='form-control' required>" . $row['Overtime6'] . "</textarea><br><br>";

echo"      <label><font face='tahoma'>Procedure for Overtime Work</font>&nbsp;&nbsp;</label><br> 
           <textarea cols='80' rows='10' name='POW1' id='POW1' class='form-control' required>" . $row['POW1'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='POW2' id='POW2' class='form-control' required>" . $row['POW2'] . "</textarea><br><br>";

echo"      <label><font face='tahoma'>Computation</font>&nbsp;&nbsp;</label><br><br> 
           <textarea cols='80' rows='10' name='Comp1' id='Comp1' class='form-control' required>" . $row['Comp1'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp2' id='Comp2' class='form-control' required>" . $row['Comp2'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp3' id='Comp3' class='form-control' required>" . $row['Comp3'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp4' id='Comp4' class='form-control' required>" . $row['Comp4'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp5' id='Comp5' class='form-control' required>" . $row['Comp5'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp6' id='Comp6' class='form-control' required>" . $row['Comp6'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp7' id='Comp7' class='form-control' required>" . $row['Comp7'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp8' id='Comp8' class='form-control' required>" . $row['Comp8'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp9' id='Comp9' class='form-control' required>" . $row['Comp9'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp10' id='Comp10' class='form-control' required>" . $row['Comp10'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp11' id='Comp11' class='form-control' required>" . $row['Comp11'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp12' id='Comp12' class='form-control' required>" . $row['Comp12'] . "</textarea><br><br>";


echo"      <label><font face='tahoma'>Computation</font>&nbsp;&nbsp;</label><br><br> 
           <textarea cols='80' rows='10' name='Comp13' id='Comp13' class='form-control' required>" . $row['Comp13'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp14' id='Comp14' class='form-control' required>" . $row['Comp14'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp15' id='Comp15' class='form-control' required>" . $row['Comp15'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp16' id='Comp16' class='form-control' required>" . $row['Comp16'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp17' id='Comp17' class='form-control' required>" . $row['Comp17'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp18' id='Comp18' class='form-control' required>" . $row['Comp18'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp19' id='Comp19' class='form-control' required>" . $row['Comp19'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp20' id='Comp20' class='form-control' required>" . $row['Comp20'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp21' id='Comp21' class='form-control' required>" . $row['Comp21'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp22' id='Comp22' class='form-control' required>" . $row['Comp22'] . "</textarea><br><br>";   

echo"      <label><font face='tahoma'>Computation</font>&nbsp;&nbsp;</label><br><br> 
           <textarea cols='80' rows='10' name='Comp23' id='Comp23' class='form-control' required>" . $row['Comp23'] . "</textarea><br>";        
echo"      <textarea cols='80' rows='10' name='Comp24' id='Comp24' class='form-control' required>" . $row['Comp24'] . "</textarea><br>";  
echo"      <textarea cols='80' rows='10' name='Comp25' id='Comp25' class='form-control' required>" . $row['Comp25'] . "</textarea><br>";  
echo"      <textarea cols='80' rows='10' name='Comp26' id='Comp26' class='form-control' required>" . $row['Comp26'] . "</textarea><br>";  
echo"      <textarea cols='80' rows='10' name='Comp27' id='Comp27' class='form-control' required>" . $row['Comp27'] . "</textarea><br>";  
echo"      <textarea cols='80' rows='10' name='Comp28' id='Comp28' class='form-control' required>" . $row['Comp28'] . "</textarea><br>";  
echo"      <textarea cols='80' rows='10' name='Comp29' id='Comp29' class='form-control' required>" . $row['Comp29'] . "</textarea><br><br>"; 

echo"      <label><font face='tahoma'>Computation</font>&nbsp;&nbsp;</label><br><br> 
           <textarea cols='80' rows='10' name='Comp30' id='Comp30' class='form-control' required>" . $row['Comp30'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp31' id='Comp31' class='form-control' required>" . $row['Comp31'] . "</textarea><br>";   
echo"      <textarea cols='80' rows='10' name='Comp32' id='Comp32' class='form-control' required>" . $row['Comp32'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp33' id='Comp33' class='form-control' required>" . $row['Comp33'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp34' id='Comp34' class='form-control' required>" . $row['Comp34'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp35' id='Comp35' class='form-control' required>" . $row['Comp35'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp36' id='Comp36' class='form-control' required>" . $row['Comp36'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp37' id='Comp37' class='form-control' required>" . $row['Comp37'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp38' id='Comp38' class='form-control' required>" . $row['Comp38'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp39' id='Comp39' class='form-control' required>" . $row['Comp39'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp40' id='Comp40' class='form-control' required>" . $row['Comp40'] . "</textarea><br><br>"; 

echo"      <label><font face='tahoma'>Computation</font>&nbsp;&nbsp;</label><br><br> 
           <textarea cols='80' rows='10' name='Comp41' id='Comp41' class='form-control' required>" . $row['Comp41'] . "</textarea><br>";
echo"      <textarea cols='80' rows='10' name='Comp42' id='Comp42' class='form-control' required>" . $row['Comp42'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp43' id='Comp43' class='form-control' required>" . $row['Comp43'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp44' id='Comp44' class='form-control' required>" . $row['Comp44'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp45' id='Comp45' class='form-control' required>" . $row['Comp45'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp46' id='Comp46' class='form-control' required>" . $row['Comp46'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp47' id='Comp47' class='form-control' required>" . $row['Comp47'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp48' id='Comp48' class='form-control' required>" . $row['Comp48'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp49' id='Comp49' class='form-control' required>" . $row['Comp49'] . "</textarea><br>"; 
echo"      <textarea cols='80' rows='10' name='Comp50' id='Comp50' class='form-control' required>" . $row['Comp50'] . "</textarea><br><br>"; 

          

echo"      <Input Type='Hidden' name='ID' id='ID' class='form-control' value=". $row['ID'] . ">
           <br>"; 

echo"     <p align='left'>
           <input type='submit' value='Submit' class='btn btn-primary' name='submit'/>
           <input type='reset' value='Reset' class='btn btn-danger' name='reset'/>
           </p></form>";


    }
?>


                    </div>
                </div>
       
</body>
</html>

<?php

if(isset($_POST['submit']))
    {


        $ID = $_POST['ID'];
        $Overtime1 = $_POST['Overtime1'];
        $Overtime2 = $_POST['Overtime2'];
        $Overtime3 = $_POST['Overtime3'];
        $Overtime4 = $_POST['Overtime4'];
        $Overtime5 = $_POST['Overtime5'];
        $Overtime6 = $_POST['Overtime6'];
        $POW1 = $_POST['POW1'];
        $POW2 = $_POST['POW2'];
        $Comp1 = $_POST['Comp1'];
        $Comp2 = $_POST['Comp2'];
        $Comp3 = $_POST['Comp3'];
        $Comp4 = $_POST['Comp4'];
        $Comp5 = $_POST['Comp5'];
        $Comp6 = $_POST['Comp6'];
        $Comp7 = $_POST['Comp7'];
        $Comp8 = $_POST['Comp8'];
        $Comp9 = $_POST['Comp9'];
        $Comp10 = $_POST['Comp10'];
        $Comp11 = $_POST['Comp11'];
        $Comp12 = $_POST['Comp12'];
        $Comp13 = $_POST['Comp13'];
        $Comp14 = $_POST['Comp14'];
        $Comp15 = $_POST['Comp15'];
        $Comp16 = $_POST['Comp16'];
        $Comp17 = $_POST['Comp17'];
        $Comp18 = $_POST['Comp18'];
        $Comp19 = $_POST['Comp19'];
        $Comp20 = $_POST['Comp20'];
        $Comp21 = $_POST['Comp21'];
        $Comp22 = $_POST['Comp22'];
        $Comp23 = $_POST['Comp23'];
        $Comp24 = $_POST['Comp24'];
        $Comp25 = $_POST['Comp25'];
        $Comp26 = $_POST['Comp26'];
        $Comp27 = $_POST['Comp27'];
        $Comp28 = $_POST['Comp28'];
        $Comp29 = $_POST['Comp29'];
        $Comp30 = $_POST['Comp30'];
        $Comp31 = $_POST['Comp31'];
        $Comp32 = $_POST['Comp32'];
        $Comp33 = $_POST['Comp33'];
        $Comp34 = $_POST['Comp34'];
        $Comp35 = $_POST['Comp35'];
        $Comp36 = $_POST['Comp36'];
        $Comp37 = $_POST['Comp37'];
        $Comp38 = $_POST['Comp38'];
        $Comp39 = $_POST['Comp39'];
        $Comp40 = $_POST['Comp40'];
        $Comp41 = $_POST['Comp41'];
        $Comp42 = $_POST['Comp42'];
        $Comp43 = $_POST['Comp43'];
        $Comp44 = $_POST['Comp44'];
        $Comp45 = $_POST['Comp45'];
        $Comp46 = $_POST['Comp46'];
        $Comp47 = $_POST['Comp47'];
        $Comp48 = $_POST['Comp48'];
        $Comp49 = $_POST['Comp49'];
        $Comp50 = $_POST['Comp50'];
       


      
        
      mysql_query("UPDATE overtime SET Overtime1='$Overtime1' , Overtime2='$Overtime2' , Overtime3='$Overtime3' , Overtime4='$Overtime4' , Overtime5='$Overtime5' , Overtime6='$Overtime6' , POW1='$POW1' , POW2='$POW2' , Comp1='$Comp1' , Comp2='$Comp2' , Comp3='$Comp3' , Comp4='$Comp4' , Comp5='$Comp5' , Comp6='$Comp6' , Comp7='$Comp7' , Comp8='$Comp8' , Comp9='$Comp9' , Comp10='$Comp10' , Comp11='$Comp11' , Comp12='$Comp12' , Comp13='$Comp14' , Comp15='$Comp15' , Comp16='$Comp16' , Comp17='$Comp17' , Comp18='$Comp18' , Comp19='$Comp19' , Comp20='$Comp20' , Comp21='$Comp21' , Comp22='$Comp22' , Comp23='$Comp23' , Comp24='$Comp24' , Comp25='$Comp25' , Comp26='$Comp26' , Comp27='$Comp27' , Comp30='$Comp30' , Comp31='$Comp31' , Comp32='$Comp32' , Comp33='$Comp33' , Comp34='$Comp34' , Comp35='$Comp35' , Comp36='$Comp36' , Comp37='$Comp37' , Comp38='$Comp38' , Comp39='$Comp39' , Comp40='$Comp40' , Comp41='$Comp41' , Comp42='$Comp42' , Comp43='$Comp43' , Comp44='$Comp44' , Comp45='$Comp45' , Comp46='$Comp46' , Comp47='$Comp47' , Comp48='$Comp48' , Comp49='$Comp49' , Comp50='$Comp50' WHERE ID='$ID'", $connection);




   echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location.href='Overtime.php';
        </SCRIPT>");
      
      
    }

    ?>