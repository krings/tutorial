<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
<style>
#ids {
    resize: none;
    }
</style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
    #msg {
        resize: none;
    }
    </style>
</head>

<body>

    <div id="wrapper">

<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="Supervisor.php">eBiZolution | Supervisor</a>
            </div>
            <!-- /.navbar-header -->

           <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
         

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

           <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                           <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="Loan.php">Loan</a>
                                </li>
                            </ul>

                       <li>
                            <a href="Approvals.php"><i class="fa fa-check-square-o"></i>  Approvals</a>
                        </li>

                        
                        <li>
                            <a href="#.php"><i class="fa fa-check-square-o"></i>  Team Management<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level">
                                <li>
                                    <a href="group.php">Team Creation</a>
                                </li>
                                <li>
                                    <a href="assign.php">Team Assignment</a>
                                </li>
                            </ul>

                        </li>

                        <li>
                            <a href="SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Performance Appraisal</h1>                   
                         <?php

 $ratee = $_GET['Ratee'];
 $testid = $_GET['TestId'];
 $Me = $_SESSION['Id'];
 $myname = $_SESSION['Name'];
 $id = $_GET['MsgId'];

 echo "<form method = 'post' enctype='multipart/form-data' name='Form' action='viewtest.php?TestId=" . $testid . "&Ratee=". $ratee . "&MsgId=" . $id . "' onSubmit='return checkForm(this);'> ";

 $testquery = mysqli_query($con,"SELECT * FROM testtb t inner join appraisaltb a on t.testID = a.TestId Where a.TestId = '$testid'");
 $testquery1 = mysqli_query($con,"SELECT testname FROM testtb Where testID = '$testid'");
 $rateequery = mysqli_query($con,"SELECT * FROM masteremptb ma inner join masterpersonaltb as m on ma.EmpId = m.EmpId inner join positionstb as p on p.PosId = m.PosId where ma.EmpId = '$ratee'");
           echo "<div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            Performance Appraisal Form                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div class='dataTable_wrapper'>
                           

                                <table class='table table-striped table-bordered table-hover' width='100%'>";
                                  while($r = mysqli_fetch_array($rateequery)){
                                echo "<tr><td><b>Employee Name: </b></td><td colspan='3'>" . $r['Name'] . "</td></tr>";    
                                echo "<tr><td><b>Company : </b></td><td>" . $r['Company'] . " </td> <td><b>Department : </b> </td> <td>" . $r['Department'] . " </td> </tr>";
                                echo "<tr><td><b>Position : </b></td><td>" . $r['Description'] ."</td> <td><b>Date Hired : </b></td> <td>" . $r['DateHire'] . " </td> </tr>";
                                echo "<tr><th colspan='4'><center> <b>Evaluation Period: " . date("F Y", strtotime($r['LastAppraisal'])) . " - " . date('F Y') . " </b></th></tr>";   
                                  }                  

                                echo "
                                <tr><th colspan='4'><center><h4><b><u>Performance Rating Guide</b></u></h4></th></tr>
                                <tr><td colspan='1'><center><b>Excellent - 5</b></td><td colspan='3'><center>Very Exceptional performance; rarely equaled.</td></tr>
                                <tr><td colspan='1'><center><b>Very Satisfactory - 4</b></td><td colspan='3'><center> Better performance than is normally expected, usually exceeds requirements of position.</td></tr>
                                <tr><td colspan='1'><center><b><b>Satisfactory - 3</b></td><td colspan='3'><center> Normally expected performance; consistently meets requirements of position.</td></tr>
                                <tr><td colspan='1'><center><b><b>Below Satisfactory - 2</b></td><td colspan='3'><center> Performance is less than normally expected.</td></tr>
                                <tr><td colspan='1'><center><b><b>Poor - 1</td><td colspan='3'><center> Fails to meet even the minimum standards.</td></tr></table>
                                ";
                                $ti = mysqli_fetch_array($testquery1);
                                
                                echo "<table class='table table-striped table-bordered table-hover'>";
                                echo "<thead><tr><th colspan='8'><center><b><h4><u>" . $ti['testname'] . "</u></b></h4></th></tr>";
                                echo "
                                        
                                        <th> </th>
                                        <th><center> Question</th>
                                        <th><center> Excellent  5</th>
                                        <th><center> Very Satisfactory  4</th>
                                        <th><center> Satisfactory  3</th>
                                        <th><center> Below Satisfactory  2</th>
                                        <th><center> Poor  1</th>
                                        <th><center> Remarks</th>
                                        </thead><tbody>
                                ";
                                $count = 0;

                                while($t = mysqli_fetch_array($testquery)){
                                    echo "<tr><td>" . $t['jobcompetence'] . " <input type='hidden' class='qname' value='" . $t['jobcompetence'] . "'></td>";
                                    echo "<td>" . $t['definition'] . " <input type='hidden' class='question' value='" . $t['definition'] . "'</td>";
                                    echo "<td><center><input type='radio' name='q". $count ."' value='5' class='competency'></td>";
                                    echo "<td><center><input type='radio' name='q". $count ."' value='4' ></td>";
                                    echo "<td><center><input type='radio' name='q". $count ."' value='3' ></td>";
                                    echo "<td><center><input type='radio' name='q". $count ."' value='2' ></td>";
                                    echo "<td><center><input type='radio' name='q". $count ."' value='1' ></td>";
                                    echo "<td><center><textarea cols='30' class='remarks' rows='10' name='remarks". $count ."' id='remarks". $count ."'></textarea></td></tr>";
                                    $count+=1;

                                }
                                echo "</table>";
                                $style = 0;
                                while($style <= $count)
                                {
                                    echo "

                                            <style>

                                                #remarks$style {
                                                resize: none;
                                                }

                                            </style>

                                    ";
                                    $style +=1;
                                }
                                echo "<table width='100%' border='0'>";
                                echo "<tr><td><p align='left'><a href='HR.php' role='button' type='button' class='btn btn-default btn-outline'>Back</a></p></td>";
                                echo "<td><p align='right'>
                                <input type='Submit' name='submit2' value='Submit' class='btn btn-primary btn-outline'/>
                                <input type='reset' value='Reset' onClick='confirm('Are you sure?')' class='btn btn-danger btn-outline'/></p></td></tr></table>";

                                   echo " </form>
                            </div>
                            <!-- /.table-responsive -->
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>";
            ?>

<script type="application/javascript">
function checkForm(form)
{    
      if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
    
    var inv2 = document.getElementsByClassName("qname");
    var inv3 = document.getElementsByClassName("question");
    var inv4 = document.getElementsByClassName("remarks");
    var xx = document.getElementsByClassName("competency").length;
    var i = 1;
        while (i <= xx){


                var inv = 'q'+ (i - 1);
                var grade = $('input[name=' + inv + ']:checked').val();
                var ratee = <?php echo $ratee; ?>; 
                var rater = <?php echo $Me; ?>;
                var testid = <?php echo $testid; ?>;
                var remarks = inv4[i - 1].value;  
                var questionname = inv2[i - 1].value; 
                var question = inv3[i - 1].value;   
                var dt = new Date();
                var adate = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
                var msgid = <?php echo $id; ?>;

                var params = "ratee=" + ratee + "&rater=" + rater + "&testid=" + testid + "&remarks=" + remarks + "&grade=" + grade + "&questionname=" + questionname + "&question=" + question + "&adate=" + adate + "&MsgId=" + msgid;
                xmlhttp.open("POST","AddAppraisalCode.php", false);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send(params);  
                i+=1;

            }

    xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    }}
      
        sendnoti();
        window.alert('Evaluation Successful!');
        window.href('Supervisor.php');
        return true;
           
    
}


function sendnoti(){

              if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
    
         var rateee = <?php echo $_GET['Ratee']; ?>;
         var testidd = <?php echo $_GET['TestId']; ?>;
         var appref = <?php echo $_GET['MsgId']; ?>;
 
        var paramss = "Ratee=" + rateee + "&TestId=" + testidd + "&AppRef=" + appref;
        xmlhttp.open("POST","AddNotiCode.php",true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send(paramss);   

            xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    }}

}
</script>
                        

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>