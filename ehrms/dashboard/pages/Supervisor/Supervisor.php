<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}

$res = mysqli_query($con,"SELECT *, DATEDIFF(Now(),LastAppraisal) as Month from masteremptb");
while($dow = mysqli_fetch_array($res)){
    $datefrom = $dow['Month'];  
    if($datefrom >= 173 && $datefrom <= 217)
    {
        $idof = $dow['EmpId'];
        mysqli_query($con,"UPDATE masteremptb SET AStat = '1' Where EmpId = '$idof'");
    }
}

$a = mysqli_query($con,"SELECT *,mp.Name as KS from accounttb as a inner join masterpersonaltb as mp on a.EmpId = mp.EmpId inner join positionstb as p on mp.PosId = p.PosId");
while($b = mysqli_fetch_array($a)){
    if($b['Level'] == 3){
         $c = mysqli_query($con,"SELECT * FROM masteremptb as ma inner join masterpersonaltb as p on ma.EmpId = p.EmpId inner join positionstb as pa on p.PosId = pa.PosId where AStat = '1' AND SysStat = '0'");
         while($d = mysqli_fetch_array($c)){
             $idsko = $b['EmpId'];
             $dat = date('Y-m-d');
             $name = $d['Name'];
             $company = $d['Company'];
             $pos = $d['Description'];
             $dept = $d['Department'];
             $mess = "Name: $name \r\n";
             $mess .= "Company: $company \r\n";
             $mess .= "Department: $dept \r\n";
             $mess .= "Position:  $pos \r\n";
             $mess .= "\r\nIs ready to be Appraised! Please processes Immediately!";
          mysqli_query($con,"INSERT INTO dashboardtb (Subject, Sender, Reciever, Date, Status, Type, Message) VALUES('Performance Appraisal Update','0','$idsko','$dat','1','1','$mess')");
        }
    }
}
mysqli_query($con,"UPDATE masteremptb SET SysStat = '1' where AStat = '1'");
  
 ?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                "order": [ 4, 'asc' ]
        });
    });
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="Supervisor.php">eBiZolution | Supervisor </a>
            </div>
            <!-- /.navbar-header -->

           <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

                       <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc, `Date` Desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>



            <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                           <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="Loan.php">Loan</a>
                                </li>
                            </ul>

                       <li>
                            <a href="Approvals.php"><i class="fa fa-check-square-o"></i>  Approvals</a>
                        </li>

                        
                        <li>
                            <a href="#.php"><i class="fa fa-check-square-o"></i>  Team Management<span class="fa arrow"></span></a>
                              <ul class="nav nav-second-level">
                                <li>
                                    <a href="group.php">Team Creation</a>
                                </li>
                                <li>
                                    <a href="assign.php">Team Assignment</a>
                                </li>
                            </ul>

                        </li>

                        <li>
                            <a href="SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS </h1>
                    </div>
               <?php
                $me =  $_SESSION["Id"];
 $result = mysqli_query($con,"SELECT * FROM dashboardtb where Reciever = '$me' Order by `Status` desc, `Date` Desc") or die('Error: ' .  mysqli_error($con));
 
          echo "<div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            Dashboard
                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div class='dataTable_wrapper'>
                                <table class='table table-striped table-bordered table-hover' id='dataTables-example'>
                                    <thead>
                                        <tr>
                                            <td align='center'><b>Subject</b></td>
                                            <td align='center'><b>Sender</b></td>
                                            <td align='center'><b>Date</b></td>
                                            <td align='center'><b>Status</b></td>
                                            <td align='center'><b>Option</b></td>
                                        </tr>
                                    </thead>



                                     <tbody> ";
                                        while($row = mysqli_fetch_array($result)) {

                                            $send = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $row['Sender'] . "' ");
                                            $sn = mysqli_fetch_array($send);

                                            if($row['Status'] == 1)
                                            {
                                                echo "<tr  style='background-color:#cccccc'>";
                                                echo "<td><b><center>" . $row['Subject'] . "</td>";
                                                echo "<td><b><center>" . $sn['Name'] . "</td>";
                                                echo "<td><b><center>" . date("M j, Y", strtotime($row['Date'])) . "</td>";
                                                echo "<td><b><center>Unread Message</td>";
                                                if($row['Type'] == 1)
                                                {
                                                    echo "<td><center><a href='ViewMessage.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";
                                                }
                                               else if($row['Type'] == 2)
                                                {
                                                    echo "<td><center><a href='ViewMessage2.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";
                                                }
                                                else if($row['Type'] == 3){
                                                    echo "<td><center><a href='ViewMessage3.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";

                                                }
                                                else if($row['Type'] == 5){
                                                    echo "<td><center><a href='ViewMessage5.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";

                                                }

                                                
                                            }
                                            else
                                            {
                                                echo "<tr>";
                                                echo "<td><b><center>" . $row['Subject'] . "</td>";
                                                echo "<td><b><center>" . $sn['Name'] . "</td>";
                                                echo "<td><b><center>" . date("M j, Y", strtotime($row['Date'])) . "</td>";
                                                echo "<td><b><center>Message Read</td>";
                                                if($row['Type'] == 1)
                                                {
                                                    echo "<td><center><a href='ViewMessage.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";
                                                }
                                                else if($row['Type'] == 2)
                                                {
                                                    echo "<td><center><a href='ViewMessage2.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";
                                                }
                                                else if($row['Type'] == 3){
                                                    echo "<td><center><a href='ViewMessage3.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";

                                                }
                                                 else if($row['Type'] == 5){
                                                    echo "<td><center><a href='ViewMessage5.php?MsgId=". $row['Id'] ."' ><input type='button' value='View' class='btn btn-primary btn-outline' name='edit'/></a></td>";
                                                    echo "</tr>";

                                                }
                                                
                                            }
                                           

                                        }      
                                   echo " </tbody> 



                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>";
            ?>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    

</body>

</html>
