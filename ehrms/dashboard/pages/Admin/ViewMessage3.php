<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
<style>
#ids {
    resize: none;
    }
</style>

  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./Admin.php">eBiZolution | Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
           

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

             <!-- NAVIGATION BARS -->

              <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>

                        <li>
                        <a href="Approvals.php"><i class="fa fa-check-square-o"></i> Approvals</a>
                        </li>    

                         <li>
                            <a href="Reports.php"><i class="fa fa-folder-open"></i> Reports</a>
                        </li>

                         <li>
                            <a href="RecruitmentMonitoring.php"><i class="fa fa-users"></i> Recruitment Monitoring</a>
                        </li>

                        <li>
                            <a href="SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>




        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Messages</h1>
                         <?php
 $id = $_GET['MsgId'];

 mysqli_query($con,"UPDATE dashboardtb SET Status = '0' Where Id='$id'")or die('Error: ' .  mysqli_error($con));; 
 $result = mysqli_query($con,"SELECT * FROM dashboardtb Where Id='$id'") or die('Error: ' .  mysqli_error($con));
 
          echo "<div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            View Message 
                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div class='dataTable_wrapper'>
                            <form method = 'post' enctype='multipart/form-data' name='Form' action='ViewMessage.php?MsgId=$id' '>

                                <table class='table table-striped table-bordered table-hover' width='100%'>";
                                        $res = mysqli_query($con,"SELECT * FROM masterpersonaltb m inner join positionstb p where m.PosId = p.PosId Order By Name") or die('Error: ' .  mysqli_error($con));
                                        while($row = mysqli_fetch_array($result)) {
                                            $send = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $row['Sender'] . "' ");
                                            $sn = mysqli_fetch_array($send);
                                            echo "<input type='hidden' id='empid' name='empid' value=" . $row['Sender'] . "";
                                            echo "<tr><td width='50%'><center><b>From</td><td width='50%'><center>" . $sn['Name'] . "</td></tr>";
                                            echo "<tr><td width='50%'><center><b>Subject</td><td width='50%'><center>" . $row['Subject'] . "</td></tr>";
                                            echo "<tr><td width='50%'><center><b>Message</td><td width='50%'><center><textarea class='form-control' cols='100' rows='7' disabled id='ids' readonly>" . $row['Message'] . "</textarea></td></tr>";
                                            $se = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $row['Ratee'] . "' ");
                                            $snd = mysqli_fetch_array($se);
                                            echo "<tr><td width='50%'><center><b>Evaluee</td><td width='50%'><center>" . $snd['Name'] . "</td></tr>";
                                            $fe = mysqli_query($con,"Select testname FROM testtb Where testID = '" . $row['TestId'] . "' ");
                                            $fnd = mysqli_fetch_array($fe);
                                            echo "<tr><td width='50%'><center><b>Test</td><td width='50%'><center>" . $fnd['testname'] . "</td></tr>";
                                            echo "<th colspan='2'><a href='viewtest3.php?AppId=". $row['AppRef'] ."' role='button' type='button' class='btn btn-lg btn-block btn-primary btn-outline'>Review Evaluation </a></p></th>";  
                                            
                                            echo "</table>";
                                            }      
                                            echo "<table width='100%' border='0'>";
                                            echo "<tr><td><p align='left'><a href='HR.php' role='button' type='button' class='btn btn-default btn-outline'>Back</a></p></td></td></tr></table>";

                                   echo " </form>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>";
            ?>
                        

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>