<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
 ?>

<!DOCTYPE html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>

function isNumberKey(evt)
          {
             var charCode = (evt.which) ? evt.which : event.keyCode
             if (charCode > 32 && (charCode < 48 || charCode > 57))
                {return false;}
             return true;
          }
    
function isLetterKey(evt)
      {
      evt = (evt) ? evt : event;
      var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
        if (charCode > 32 && (charCode < 65 || charCode > 90) &&
                (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
      }
    </script>

</head>

<body>
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./Admin.php">eBiZolution | Admin</a>
            </div>
            <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

             <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

            <!-- NAVIGATION BARS -->

              <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>

                        <li>
                        <a href="Approvals.php"><i class="fa fa-check-square-o"></i> Approvals</a>
                        </li>    

                         <li>
                            <a href="Reports.php"><i class="fa fa-folder-open"></i> Reports</a>
                        </li>

                         <li>
                            <a href="RecruitmentMonitoring.php"><i class="fa fa-users"></i> Recruitment Monitoring</a>
                        </li>

                        <li>
                            <a href="SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Ticket Information</h1>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <b>Ticket Information</b>
                        </div>
                        <div class="panel-body">


<?php  
$host='localhost';
$user='root';
$pass="";

mysql_connect($host,$user,$pass) or die (mysql_error());
mysql_select_db('ehrms') or die (mysql_error());

$id = $_GET["TicketId"];


$qry = "SELECT * FROM tickettb as T INNER JOIN ticketpersonaltb as E WHERE T.TicketId = '$id' AND E.TicketId = '$id'";
$result = mysql_query($qry) or die('Error: ');

//added

$result_two = mysql_query("SELECT * FROM tempuploadstb WHERE TicketId = '$id'");
while($row = mysql_fetch_array($result_two))

{

$pic = $row['Location'];

}

//added



while($row = mysql_fetch_array($result))
{
    $Name = $row['Name']; //check
    $Address = $row['Address']; //check
    $celnum = $row['CellNum']; //check
    $Mstatus = $row['Mstatus']; //check
    $Email = $row['Email']; //check
    $bday = $row['Dob']; //check
    $Position = $row['PosId']; //check
    $Pagibig = $row['Pagibig'];
    $SSS = $row['SSS']; //check
    $Tin = $row['Tin']; //check
    $Phealth = $row['Philhealth']; //check
    $Dependency = $row['Dependency'];
}

$qryp = "SELECT * FROM positionstb as T INNER JOIN ticketpersonaltb as E WHERE T.PosId = '$Position' AND E.PosId = '$Position' ";
$result_three = mysql_query($qryp) or die('Error: ');
while($row = mysql_fetch_array($result_three))

{

$PosId = $row['PosId'];
$Description = $row['Description'];
$Definition = $row['Definition'];
$Company = $row['Company'];


}
 $strbday = date("M j, Y", strtotime($bday));

//echo $Address;
//!---------------------------------------------------------------------->
echo "<br><table class='table'>";


echo "<tr>";
    echo "<tr rowspan='2'>";
    echo "<td width='178' rowspan='7'>
    <img src='$pic' height='300' width='282' data-toggle='modal' data-target='#myModalpic'></img>";

    
    echo "<td align='left'><b>Employee Name   </td>";
    echo "<td>: &nbsp; &nbsp;$Name</td>";

    echo "<td align='left'><b>Company </td>";
    echo "<td> : &nbsp; &nbsp;$Company</td><tr>";

    echo "<tr rowspan='2'>";
    echo "<td align='left'><b>Birthday </td>";
    echo "<td>: &nbsp; &nbsp;$strbday</td>";

    echo "<td align='left''><b>Position </td>";
    echo "<td>: &nbsp; &nbsp;$Description</td></tr>";

    echo "<tr>";
    echo "<td align='left''><b>Marital Status    </td>";
    echo "<td>: &nbsp; &nbsp;$Mstatus</td>";

    echo "<td align='left''><b>Pag-Ibig Number    </td>";
    echo "<td>: &nbsp; &nbsp;$Pagibig</td></tr>";

    echo "<tr><td align='left''><b>Home Address </td>";
    echo "<td>: &nbsp; &nbsp;$Address</td>";

    echo "<td align='left''><b>TIN Number </td>";
    echo "<td>: &nbsp; &nbsp;$Tin</td></tr>";

    echo "<tr><td align='left''><b>E-Mail Address </td>";
    echo "<td>: &nbsp; &nbsp;$Email</td>";

    echo "<td align='left''><b>SSS Number </td>";
    echo "<td>: &nbsp; &nbsp;$SSS</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td align='left''><b>Cellphone Number    </td>";
    echo "<td>: &nbsp; &nbsp;$celnum</td>";

    echo "<td align='left''><b>PhilHealth Number </td>";
    echo "<td>: &nbsp; &nbsp;$Phealth</td>";
    echo "</tr>";

    echo "<tr><td></td><td></td><td></td><td align='left'><b>Dependency </td>";
    echo "<td>: &nbsp; &nbsp;$Dependency</td>";
    echo "</tr>";

echo "</table>";



//Job information
echo "  <div class='row'>
        <div class='col-md-8'>
            <div class='panel panel-primary'>
                <div class='panel-heading'>
                Job Information
                </div>
                
                <div class='panel-body'>
                <p>
                &nbsp;&nbsp;<b>" . $Description . "</b>
                <br><br>
                &nbsp;&nbsp;" . $Definition . "
                <br><br></p>
                
                </div>
            </div>
        </div>";

//Eligible Benefits
echo "  <div class='col-md-4'>
        <div class='panel panel-red'>
            <div class='panel-heading'>
                Eligible Benefits
            </div>

            <div class='panel-body'>
                <ul>
                <li>Paid Time Off and Leaves of Absence</li>
                <li>Transportation Benefits</li>
                <li>Rice Subsidy</li>
                <li>Meal Allowance every 4hrs of OT</li>
                </ul>
            </div>
        </div>
    </div>
        </div> ";

$result=mysql_query("SELECT * FROM ticketeductb where TicketId='$id'")or die (mysql_error());
 
echo " <table class='table' border='0'><thead>";
echo "<tr><th colspan = '5'>EDUCATIONAL BACKGROUND</th></tr>";
echo "<tr><center>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Date Of Graduation</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>School</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Qualification</td>";
echo "<td colspan='3' bgcolor='#D4D8D1'><div align='center'><b>Option</td></div></tr>";

while($row1= mysql_fetch_array($result))
{
echo "<tr>";
echo "<td><div align='center'>";echo date("M j, Y", strtotime($row1['Date'])); echo "<br></td>";
echo "<td><div align='center'>";echo $row1['School'];echo "<br></td>";
echo "<td><div align='center'>";echo $row1['Qualification'];echo "<br></td>";
}


echo "</tr></table></thread>";

//end

//start

$result=mysql_query("SELECT * FROM ticketskillstb where TicketId= '$id'")or die (mysql_error());

echo " <table class='table' width='100%' border='0'><thead>";
echo "<tr><th colspan = '4'>Skills</th></tr>";
echo "<tr><center>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Skill ID</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Skills</td>";
echo "<td colspan='3' bgcolor='#D4D8D1'><div align='center'><b>Option</td></div></tr>";
while($row2= mysql_fetch_array($result))
{
echo "<tr>";
echo "<td width='10%'><div align='center'>";echo $row2['SkillsID'];echo "<br></td>";
echo "<td width='70%'><div align='center'>";echo $row2['skills'];echo "<br></td>";
}



//start work

$result=mysql_query("SELECT * FROM ticketworktb where TicketId= '$id'")or die (mysql_error());

echo " <table class='table' width='100%' border='0'><thead>";
echo "<tr><th colspan = '4'>Work</th></tr>";
echo "<tr><center>";
//echo "<td bgcolor='#D4D8D1'><div align='center'><b>Skill ID</td>";

echo "<td bgcolor='#D4D8D1'><div align='center'><b>Employer</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Address</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Date Start</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Date End</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Position</td>";
echo "<td bgcolor='#D4D8D1'><div align='center'><b>Reason</td>";
echo "<td colspan='2' bgcolor='#D4D8D1'><div align='center'><b>Option</td></div></tr>";

while($row3= mysql_fetch_array($result))
{
echo "<tr>";


echo "<td width='10%'><div align='center'>";echo $row3['Employer'];echo "<br></td>";
echo "<td width='10%'><div align='center'>";echo $row3['Address'];echo "<br></td>";
echo "<td width='10%'><div align='center'>";echo date("M j, Y", strtotime($row3['DateStart']));echo "<br></td>";
echo "<td width='10%'><div align='center'>";echo date("M j, Y", strtotime($row3['DateEnd']));echo "<br></td>";
echo "<td width='10%'><div align='center'>";echo $row3['Position'];echo "<br></td>";
echo "<td width='10%'><div align='center'>";echo $row3['Reason'];echo "<br></td>";

}

echo "</table></thread>";

//end





?>
                    
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

 <!--MODAL CONTENT______________________________________________________________________________________________________________-->
<!-- FOR EDUCATIONAL BACKGROUND -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Educational Background</h4>
            </div>
                <div class="modal-body">
                  <form action method="POST">
                        <table border='0' width='100%'>
                            <tr>
                                <td width='30%'><label><font color='#505050'>Grad School:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="text" name="gSchool" onkeypress="return isLetterKey(event)" id="grad" required placeholder='Grad School'></td>
                            </tr>
                                <tr><td><br></td></tr>
                            <tr>
                                <td><label><font color='#505050'>Grad Date:</font>&nbsp;&nbsp;</label> </td>
                                <td><input class="form-control" type="date" class="date" name="gdate" required id="date"></td></tr>
                            </tr>
                                <tr><td><br></td></tr>
                            <tr>
                                <td><label><font color='#505050'>Grad Qualification:</font>&nbsp;&nbsp;</label> <br></td>
                                <td><input class="form-control" type ="text" name="gdetails" onkeypress="return isLetterKey(event)" id="details" required placeholder='Grad Qualification'></td>
                            </tr>
                        </table>
                      </div>
                      <div class='modal-footer'>
                          <button type='button' class='btn btn-outline btn-danger' data-dismiss='modal'>Cancel</button>
                          <input type='submit' name='submit' id='submit' class='btn btn-outline btn-primary'>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--FOR SKILLS-->
<div id="myModal1" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Skills</h4>
            </div>
                <div class="modal-body">
                  <form action method="POST">
                        <table border='0' width='100%'>
                            <tr>
                                <td width='10%'><label><font color='#505050'>Skill:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="text" name="skills" onkeypress="return isLetterKey(event)" id="skills" required placeholder='Skill'></td>
                            </tr><!-- 
                                <tr><td><br></td></tr>
                            <tr>
                                <td><label><font color='#505050'>Grad Date:</font>&nbsp;&nbsp;</label> </td>
                                <td><input class="form-control" type="date" class="date" name="gdate" required id="date"></td></tr>
                            </tr>
                                <tr><td><br></td></tr>
                            <tr>
                                <td><label><font color='#505050'>Grad Qualification:</font>&nbsp;&nbsp;</label> <br></td>
                                <td><input class="form-control" type ="text" name="gdetails" id="details" placeholder='Grad Qualification'></td>
                            </tr> -->
                        </table>
                      </div>
                      <div class='modal-footer'>
                          <button type='button' class='btn btn-outline btn-danger' data-dismiss='modal'>Cancel</button>
                          <input type='submit' name='submit1' id='submit' class='btn btn-outline btn-primary'>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--FOR WORK-->
<div id="myModal2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Work</h4>
            </div>
                <div class="modal-body">
                  <form action method="POST">
                        <table border='0' width='100%'>
                            <tr>
                                <td width='15%'><label><font color='#505050'>Employer:</font>&nbsp;&nbsp;</label></td>
                                <td>

                                    <select id='Employer' name='Employer' required class='form-control'>
                                        <option value=''>Option</option>
                                        <option value='eBiZolution'>Ebizolution</option>
                                        <option value='Kreativo Koncepto'>Kreativo Koncepto</option>
                                        <option value='Optomization'>Optomization</option>
                                    </select>

                                </td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr>
                                <td width='15%'><label><font color='#505050'>Address:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="text" name="Address" onKeyPress="return isLetterKey(event)" id="Address" required placeholder='Address'></td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr>
                                <td width='15%'><label><font color='#505050'>Date Start:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="date" name="DateStart" onKeyPress="return isNumberKey(event)" id="DateStart" required placeholder='DateStart'></td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr>
                                <td width='15%'><label><font color='#505050'>Date End:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="date" name="DateEnd" onKeyPress="return isNumberKey(event)" id="DateEnd" required placeholder='DateEnd'></td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr>
                                <td width='15%'><label><font color='#505050'>Position:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="text" name="Position" onKeyPress="return isLetterKey(event)" id="Position" required placeholder='Position'></td>
                            </tr>
                            <tr><td><br></td></tr>
                            <tr>
                                <td width='15%'><label><font color='#505050'>Reason:</font>&nbsp;&nbsp;</label></td>
                                <td><input class="form-control" type="text" name="Reason" onKeyPress="return isLetterKey(event)" id="Reason" required placeholder='Reason'></td>
                            </tr>




                            <!-- 
                                <tr><td><br></td></tr>
                            <tr>
                                <td><label><font color='#505050'>Grad Date:</font>&nbsp;&nbsp;</label> </td>
                                <td><input class="form-control" type="date" class="date" name="gdate" required id="date"></td></tr>
                            </tr>
                                <tr><td><br></td></tr>
                            <tr>
                                <td><label><font color='#505050'>Grad Qualification:</font>&nbsp;&nbsp;</label> <br></td>
                                <td><input class="form-control" type ="text" name="gdetails" id="details" placeholder='Grad Qualification'></td>
                            </tr> -->
                        </table>
                      </div>
                      <div class='modal-footer'>
                          <button type='button' class='btn btn-outline btn-danger' data-dismiss='modal'>Cancel</button>
                          <input type='submit' name='submit2' id='submit2' class='btn btn-outline btn-primary'>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>




<!--KAREEEEEEEEENNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN IMG-->
<div id="myModalpic" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Picture</h4>
            </div>
                <div class="modal-body" align='center'>
                    <?php echo "<img src='$pic' height='500' width='450'>"; ?>          
                </div>
                      <div class='modal-footer' align='center'>
                          <button type='button' class='btn btn-outline btn-danger' data-dismiss='modal'>Close</button>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>




</body>
</html>
<?php  
  $con = mysql_connect('localhost', 'root', '');
  $db = mysql_select_db("ehrms", $con);
  
if(isset($_POST['submit']))
    {
      
      $grad = $_POST['gSchool'];
      $graddate = $_POST['gdate']; 
      $graddetails = $_POST['gdetails'];
      $TicketId = $id;

      
      mysql_query("INSERT INTO ticketeductb (TicketId, School, Date, Qualification) VALUES ('$TicketId', '$grad', '$graddate', '$graddetails')", $con) or die('Error: ' . mysql_error($con));
  
        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('New ticket was successfully added.')
        window.location.href='TicketProfile.php?TicketId=" . $TicketId . "';
        </SCRIPT>");
        
        if (!mysql_query($sql,$connection)) 
        {
            die('Error: ' . mysql_error($connection));
        }
        else
        {
          header("location: TicketProfile.php?TicketId=" . $TicketId . "");
        }

    }
  
if(isset($_POST['submit1']))
    {
      
      $skills = $_POST['skills'];
      $TicketId = $id;
      
      mysql_query("INSERT INTO ticketskillstb (TicketId, skills) VALUES ('$TicketId', '$skills')", $con) or die('Error: ' . mysql_error($con));
  
        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('New skill was successfully added.')
        window.location.href='TicketProfile.php?TicketId=" . $TicketId . "';
        </SCRIPT>");
        
        if (!mysql_query($sql,$connection)) 
        {
            die('Error: ' . mysql_error($connection));
        }
        else
        {
          header("location: TicketProfile.php?TicketId=" . $TicketId . "");
        }

    }


    if(isset($_POST['submit2']))
    {
      
      $TicketId = $id;
      $Employer = $_POST['Employer'];
      $Address = $_POST['Address'];
      $DateStart = $_POST['DateStart'];
      $DateEnd = $_POST['DateEnd'];
      $Position = $_POST['Position'];
      $Reason = $_POST['Reason'];
      
      mysql_query("INSERT INTO ticketworktb (TicketId, Employer, Address, DateStart, DateEnd, Position, Reason) VALUES ('$TicketId', '$Employer', '$Address', '$DateStart', '$DateEnd', '$Position', '$Reason')", $con) or die('Error: ' . mysql_error($con));
  
        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('New Work was successfully added.')
        window.location.href='TicketProfile.php?TicketId=$TicketId';
        </SCRIPT>");
        
        if (!mysql_query($sql,$connection)) 
        {
            die('Error: ' . mysql_error($connection));
        }
        else
        {
          header("location: TicketProfile.php?TicketId=" . $TicketId . "");
        }

    }
?>
