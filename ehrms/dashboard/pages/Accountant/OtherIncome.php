<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ./Accountant.php");
}
  
 ?>

 <script>

function deduct(ids){
            if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
                                 
            var params = "id=" + ids; 
            xmlhttp.open("GET","getIncome.php?" + params, true);
            xmlhttp.send();
        

          xmlhttp.onreadystatechange=function() {
             if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    document.getElementById("rsp_deduct").innerHTML=xmlhttp.responseText;
                }
                }

}


 </script>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./Accountant.php">eBiZolution | Accountant</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
                        <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

                                   <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="PersonalInformation.php">Personal Information</a>
                                </li>
                                
                                <li>
                                    <a href="SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                             </li>
                       <li>
                            <a href="#"><i class="fa fa-usd"></i> Payroll<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="ImportDTR.php"><i class="fa fa-server"></i> Import DTR</a>
                                </li>
                                <li>
                                    <a href="DtrAdjustment.php"><i class="fa fa-server"></i> DTR Adjustment</a>
                                </li>
                                <li>
                                    <a href="Holiday.php"><i class="fa fa-server"></i> Holidays</a>
                                </li>
                                <li>
                                    <a href="OtherIncome.php"><i class="fa fa-server"></i> Other Income</a>
                                </li>
                                <li>
                                    <a href="Payroll.php"><i class="fa fa-money"></i> Payroll</a>
                                </li>
                                </li>
                            </li>
                         <li>
                                    <a href="#">Deductions<span class="fa arrow"></span></a>
                             <ul class="nav nav-third-level">
                                    <li>
                                    <a href="OtherDeductions.php"><i class="fa fa-sticky-note"></i> Other Deductions</a>
                                    </li>
                                <li><a href="ImportDTR.php">
                                    <i class="fa fa-database"></i> Deduction Management<span class="fa arrow"></span></a>
                                    <ul class="nav nav-fourth-level">
                                         <li>
                                            <a href="sss.php"><img src='../Uploads/Images/sss.jpg' width='16' height='16'> SSS</a>
                                            </li>
                                            <li>
                                            <a href="pagibig.php"><img src='../Uploads/Images/pagibig.jpg' width='16' height='16'> Pagibig</a>
                                            </li> 
                                            <li>
                                            <a href="philhealth.php"><img src='../Uploads/Images/phil.jpg' width='16' height='16'> Philhealth</a> 
                                            </li> 
                                            <li>
                                            <a href="tax.php"><img src='../Uploads/Images/tax.jpg' width='16' height='16'> Tax</a>
                                            </li> 
                                    </ul>  
                                </li> 
                            </ul>  
                        </li>
                    

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                           <li>
                             <a href="#"><i class="fa fa-bar-chart"></i> Reports<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="Anually.php">Annually</a>
                                        </li>
                                        <li>
                                            <a href="Monthly.php">Monthly</a>
                                        </li>
                                    </ul>
                                </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="Loan.php">Loan</a>
                                </li>
                            </ul>

                        <li>
                            <a href="SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Other Income</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                <center><a href='./AddIncome.php'><img src='../Uploads/Images/income.png' width='100' height='100'></a>
                        <h3><i class="fa fa-plus"></i>&nbsp;Add Income</h3>
                        <table class='table table-striped table-condensed table-bordered table-hover'><tr>

             <?php
                          $results = mysqli_query($con,"SELECT * FROM masterpersonaltb m inner join positionstb p where m.PosId = p.PosId Order By Name") or die('Error: ' .  mysqli_error($con));
                          echo '<td><h4><p align="middle">Other Income For: <p></h4></td><td><select class="form-control" name="empid" onChange="deduct(this.options[this.selectedIndex].value)">';
                          echo "<option value=''> </option>";
                                        while($rows = mysqli_fetch_array($results)) {
                                     echo '<option value="' . $rows["EmpId"] . '">' . $rows["Name"] . ' / ' . $rows['Description'] .  '</option>';
                                        }     
                        echo "</option></td></tr>"; 


            ?>
                </table> 

            <div id='rsp_deduct'><div>
           
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>