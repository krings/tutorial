<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ./Accountant.php");
}
  
 ?>

<script type="application/javascript">

function TaxAdd(tax){

    if(parseFloat(tax))
    {

        var deduct = parseFloat(document.getElementById('grossdeduc').value);
        var totaldeduct = parseFloat(document.getElementById('totaldeduct').value);
        var net = parseFloat(document.getElementById('netpay').value);  
        var totalded = totaldeduct + parseFloat(tax);
        var totalnet = net - parseFloat(tax);
        var deduct = deduct + parseFloat(tax);

        document.getElementById('grossdeduc').value = deduct;
        document.getElementById('totaldeduct').value = totalded;
        document.getElementById('netpay').value = totalnet;
    }
    else
    {
        alert('Invalid Value in Tax');
    }

    
}


 </script>

<?php

                    if(isset($_POST['SUBMIT'])){
                        
                        $id = $_POST['EmpId'];    
                        $date1 = $_POST['start'];
                        $date2 = $_POST['end'];
                        $gross = $_POST['hrsworkpay'];
                        $OT = $_POST['hrsotpay'];
                        $UT = $_POST['hrsut'];
                        $sss = $_POST['sssee'];
                        $pg = $_POST['pagibig'];
                        $phil = $_POST['phil'];
                        $tax = $_POST['tax'];
                        $grossdeduc = $_POST['grossdeduc'];
                        $grosspay = $_POST['grosspay'];
                        $totaldeduct = $_POST['totaldeduct'];
                        $net = $_POST['netpay'];
                        $ok = 1;
                        $a = mysqli_query($con,"SELECT * FROM payrolldatatb");
                        while($p = mysqli_fetch_array($a)){
                            if($p['PStart'] == $date1 && $p['PEnd'] == $date2 && $p['EmpId'] == $id){
                                echo ("<SCRIPT LANGUAGE='JavaScript'>
                                window.alert('Payroll Already Created!');
                                return 0;
                                </SCRIPT>");
                                $ok = 0; 
                            }
                        }

                        if($ok == 1){
                         $query = "INSERT INTO payrolldatatb(EmpId,PStart,PEnd,Gross,OT,UT,tax,sss,philhealth,pagibig,regdeduct,TotalGross,TotalDeduct,Net) VALUES('$id','$date1','$date2','$gross','$OT','$UT','$tax','$sss','$phil','$pg','$grossdeduc','$grosspay','$totaldeduct','$net')";                        
                         mysqli_query($con,$query) or die(mysqli_error($con));
                         mysqli_query($con,"UPDATE payrolltb SET Status = '1' Where PStart = '$date1' AND PEnd ='$date2' AND EmpId = '$id'");
                       
                        $in = mysqli_query($con,"SELECT * FROM incometb where EmpId = '$id' AND IStart = '$date1' AND IEnd ='$date2' AND Status = '1'");
                        while($ii = mysqli_fetch_array($in)){  
                            if(isset($ii['InId'])){
                                $inID = $ii['InId'];
                                mysqli_query($con,"UPDATE incometb SET Status = '0' WHERE InId = 'inID'");      
                            }
                           
                        }
          
                        $od = mysqli_query($con,"SELECT * FROM deductionstb where EmpId = '$id' AND Start = '$date1' AND End ='$date2' AND Status = '1'");
                        while($odd = mysqli_fetch_array($od)){  
                            if(isset($ii['DeductionId'])){
                                $inID = $ii['DeductionId'];
                                mysqli_query($con,"UPDATE deductionstb SET Status = '0' WHERE DeductionId = 'inID'");      
                            }
                                  
                        }

                         echo ("<SCRIPT LANGUAGE='JavaScript'>
                               window.alert('Payroll Success');
                                window.location.href='Payroll.php';
                                </SCRIPT>");
                        }
                    }

 ?>



<!DOCTYPE html>
<html lang="en">

<head>

     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./Accountant.php">eBiZolution | Accountant</a>
            </div>
            <!-- /.navbar-header -->

           <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
                       <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by Status desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by Status desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

                       <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="PersonalInformation.php">Personal Information</a>
                                </li>
                                
                                <li>
                                    <a href="SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                             </li>
                       <li>
                            <a href="#"><i class="fa fa-usd"></i> Payroll<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="ImportDTR.php"><i class="fa fa-server"></i> Import DTR</a>
                                </li>
                                <li>
                                    <a href="DtrAdjustment.php"><i class="fa fa-server"></i> DTR Adjustment</a>
                                </li>
                                <li>
                                    <a href="Holiday.php"><i class="fa fa-server"></i> Holidays</a>
                                </li>
                                <li>
                                    <a href="OtherIncome.php"><i class="fa fa-server"></i> Other Income</a>
                                </li>
                                <li>
                                    <a href="Payroll.php"><i class="fa fa-money"></i> Payroll</a>
                                </li>
                                </li>
                            </li>
                         <li>
                                    <a href="#">Deductions<span class="fa arrow"></span></a>
                             <ul class="nav nav-third-level">
                                    <li>
                                    <a href="OtherDeductions.php"><i class="fa fa-sticky-note"></i> Other Deductions</a>
                                    </li>
                                <li><a href="ImportDTR.php">
                                    <i class="fa fa-database"></i> Deduction Management<span class="fa arrow"></span></a>
                                    <ul class="nav nav-fourth-level">
                                         <li>
                                            <a href="sss.php"><img src='../Uploads/Images/sss.jpg' width='16' height='16'> SSS</a>
                                            </li>
                                            <li>
                                            <a href="pagibig.php"><img src='../Uploads/Images/pagibig.jpg' width='16' height='16'> Pagibig</a>
                                            </li> 
                                            <li>
                                            <a href="philhealth.php"><img src='../Uploads/Images/phil.jpg' width='16' height='16'> Philhealth</a> 
                                            </li> 
                                            <li>
                                            <a href="tax.php"><img src='../Uploads/Images/tax.jpg' width='16' height='16'> Tax</a>
                                            </li> 
                                    </ul>  
                                </li> 
                            </ul>  
                        </li>
                    

                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                           <li>
                             <a href="#"><i class="fa fa-bar-chart"></i> Reports<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="Anually.php">Annually</a>
                                        </li>
                                        <li>
                                            <a href="Monthly.php">Monthly</a>
                                        </li>
                                    </ul>
                                </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="Loan.php">Loan</a>
                                </li>
                            </ul>

                        <li>
                            <a href="SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Payroll</h1>                   
                    </div>
                    <!-- /.col-lg-12 -->
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            Payroll
                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>



<?php
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())   {   echo "Failed to connect to MySQL: " . mysqli_connect_error();   } 

$id = $_GET['PId'];
echo "<form action='createpayroll.php?PId=". $id ."' method='post'>";
$rr = mysqli_query($con,"SELECT * FROM payrolltb where PId = '$id'");
while($row=mysqli_fetch_array($rr)){
    $EmpId = $row['EmpId'];
    $frum = $row['PStart'];
    $too = $row['PEnd'];
}

$qry = "SELECT *,m.Name as Na,m.Company as C FROM masteremptb ma 
inner join masterpersonaltb m on ma.EmpId = m.EmpId
inner join masteruploadtb u on u.EmpId = m.EmpId
inner join positionstb pa on pa.PosId = m.PosId Where m.EmpId = '$EmpId'";


$resultd = mysqli_query($con,$qry) or die('Error: '. mysqli_error($con));
 while($rows=mysqli_fetch_array($resultd))
 {
    $basicpay = $rows['BasicPay'];
    $dep = $rows['Dependency'];
    $mstat = $rows['Mstatus'];  
        echo "<table class='table'><center><thead>";
         echo "<tr rowspan='2'>";

                    echo "<tr rowspan='2'>";
                    echo "<td width='178' rowspan='7' align='center'>
                    <img src='". $rows['Location'] . "' height='100' width='120'></img></td>";

                    echo "<td align='left'><b>Employee Number: </td>";
                    echo "<td>" . $rows['EmpId'] . "</td>";

                    echo "<td align='left'><b>Employee Name: </b> </td>";
                    echo "<td>" . $rows['Na'] . "</td></tr>";

                    echo "<tr><td align='left'><b>Designation:</b> </td>";
                    echo "<td>" . $rows['Description'] . "</td>
                    <td align='left'><b>Company:</b> </td>";
                    echo "<td>" . $rows['C'] . "</td>

                    </tr>";
                    echo "<tr><th colspan='4'><center> Payroll Period: " . date("M j, Y", strtotime($frum)) . " - " . date("M j, Y", strtotime($too))  . " </th></tr>";
                    echo "</table><thead>";
                //end
}

$reghrs = 0;
$othrs = 0;
$uthrs = 0;
$additionalpay = 0;
$additionalot = 0;

$hh = mysqli_query($con,"SELECT * FROM masteremptb as ma inner join mustardtb as mu on ma.MusId = mu.MusId inner join masterpersonaltb as p on ma.EmpId = p.EmpId inner join positionstb as pos on pos.PosId = p.PosId where ma.EmpId = '$EmpId' AND mu.StartDate >= '$frum' AND mu.EndDate <= '$too'") or die("Error " . mysqli_error($con));
while($how = mysqli_fetch_array($hh))
{
        $a = $how['BasicPay'] / 261;
        $payperday = $a * 12;
        $payperhour = $payperday / 8;
        $regular = 0;

        $ho = mysqli_query($con,"SELECT * FROM holidaytb");
        while($holi = mysqli_fetch_array($ho)){

            if($how['Date'] == $holi['Date']){

                if($holi['Type'] == 1){
                    $legalpay = $payperhour * 2;
                    $legalot = ($legalpay * .3) + $legalpay;
                    $additionalpay = $additionalpay + ($how['HrsWork'] * $legalpay);
                    $additionalot =  $additionalot + ($how['HrsOT'] * $legalot);
                    $reghrs = $reghrs + 8;
                    $uthrs = $uthrs + 0;
                    $regular = 1;
                }else{

                    $legalpay = ($payperhour * .3) + $payperhour;
                    $legalot = ($legalpay * .3) + $legalpay;
                    $additionalpay = $additionalpay + ($how['HrsWork'] * $legalpay);
                    $additionalot =  $additionalot + ($how['HrsOT'] * $legalot);
                    $uthrs = $uthrs + 0;
                    $regular = 1;
                }//end if type    
            }//end if date
            
        }

        if($regular == 0)
        {
                      $reghrs = $reghrs + $how['HrsWork'];
                      $othrs = $othrs + $how['HrsOT'];
                      $uthrs = $uthrs + $how['HrsUT'];
                      $regular = 1;
        }
        //end while

}

        $hrsworkpay = ($basicpay / 2) + $additionalot + $additionalpay;
        $OTPay = ($payperhour * .25) + $payperhour;
        $hrsotpay = $OTPay * $othrs;
        $hrsutpay = $payperhour * $uthrs;
                    echo "<input type='hidden' value='$frum' name='start'><input type='hidden' value='$too' name='end'><input type='hidden' value='$EmpId' name='EmpId'>";

                    echo "<table class='table' border='0' width='100%'><center><thead>";
                    
                    echo "<tr><td colspan='2' width='50%'><b>Basic Pay</b></td>";
                    echo "<td colspan='2'><b>Overtime Pay</b></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td width='15%'>Reg Hours Work: </td>";
                    echo "<td><input type = 'number' class='form-control' readonly name='hrswork' id='hrswork' value='".  number_format($reghrs, 2, '.',',') ."'></td>";
                    
                    echo "<td width='15%'>Overtime Hours:</td>";
                    echo "<td><input type = 'number' class='form-control' readonly name='otwork' id='otwork' value='".  number_format($othrs, 2, '.',',') ."'></td></tr>";

                    echo "</tr>";

                    echo "<tr align= 'left'>";
                    echo "<td width='15%'>Reg Pay/Hour:</td>";

                    echo "<td><input type = 'number' class='form-control' name='workperhr' id='workperhr' readonly value='". number_format($payperhour, 2, '.',',') ."'></td>";

                    echo "<td>OT Pay/Hour:</td>";
                    echo "<td><input type = 'number' class='form-control' name='otperhr' id='otperhr' readonly value='" .number_format($OTPay, 2, '.',','). "'></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td width='15%'>Gross Income:</td>";
                    echo "<td><input type = 'text' readonly name='hrsworkpay' class='form-control' id='hrsworkpay' value='". $hrsworkpay ."'></td>";

                    echo "<td>Gross OT Pay:</td>";
                    echo "<td><input type = 'text' readonly  class='form-control' name='hrsotpay' id='hrsotpay' value='". $hrsotpay ."'></td></tr>";

                    echo "</tr>";

                    echo "<table class='table' width='100%' border='0'><center><thead>";
                    
                    echo "<tr><td colspan='2' width='100%'><b>Undertime Deduction</b></td></tr>";
                    echo "<tr align= 'left'>";

                    echo "<td width='25%'>Total Undertime Hours:</td>";
                    echo "<td><input type = 'number' class='form-control' readonly name='utwork' id='utwork' value='".  number_format($uthrs, 2, '.',',') ."'></td></tr>";

                    echo "<td>Regular Pay/Hour:</td>";
                    echo "<td><input type = 'number' class='form-control' name='utperhr' id='utperhr' readonly value='" .number_format($payperhour, 2, '.',','). "'></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td>Gross Undertime Deduction:</td>";
                    echo "<td><input type = 'text' readonly class='form-control' name='hrsut' id='hrsut' value='". $hrsutpay ."'></td></tr>";

                    echo "</tr></table></thead>";

        //Other Income
         echo "<table class='table'><center><thead>";
         echo "<tr><th>Other Income</th></tr>";
         echo "
                <th>Income Id</th>
                <th>Income Name</th>
                <th>Income Amount</th>
                </thead><tbody>
         ";
        $inamount = 0;
        $in = mysqli_query($con,"SELECT * FROM incometb where EmpId = '$EmpId' AND IStart = '$frum' AND IEnd ='$too' AND Status = '1'");
        while($ii = mysqli_fetch_array($in)){  
             $inamount = $inamount + $ii['Amount'];
                    echo "<tr>";
                    echo "<td><input type = 'text' readonly name='oiid' id='oiid' class='form-control OtherIncome' value='".$ii['InId']."'></td>";
                    echo "<td><input type = 'text' readonly name='oiname' id='oiname' class='form-control OtherIName' value='" . $ii['IncomeName'] . "'></td>";
                    echo "<td><input type = 'text' readonly name='iamount' id='iamount' class='form-control OtherIAmount' value='". number_format($ii['Amount'], 2, '.',',') ."'></td></td><tr>";
       }
          echo "<tr><td><b>Total Amount </td><td> &nbsp;</td><td><input type = 'text' readonly name='TIamount' id='TIamount' class='form-control' value='". number_format($inamount, 2, '.',',') ."'></td></td><tr>";
          echo "</tr></tbody></table>";

        //Other Deductions
         echo "<table class='table'><center><thead>";
         echo "<tr><th>Other Deductions</th></tr>";
         echo "
                <th>Deduction Id</th>
                <th>Deduction Name</th>
                <th>Deduction Amount</th>
                </thead><tbody>
         ";
        $odamount = 0;
        $od = mysqli_query($con,"SELECT * FROM deductionstb where EmpId = '$EmpId' AND Start = '$frum' AND End ='$too' AND Status = '1'");
        while($dd = mysqli_fetch_array($od)){  
             $odamount = $odamount + $dd['DeducAmount'];
                    echo "<tr><td><input type = 'text' readonly name='oiid' id='oiid' class='form-control OtherDeductions' value='".$dd['DeductionId']."'></td>";
                    echo "<td><input type = 'text' readonly name='oiname' id='oiname' class='form-control OtherDName' value='" . $dd['DeducName'] . "'></td>";
                    echo "<td><input type = 'text' readonly name='iamount' id='iamount' class='form-control OtherDAmount' value='". number_format($dd['DeducAmount'], 2, '.',',') ."'></td></td><tr>";
       }
          echo "<tr><td><b>Total Amount </td><td> &nbsp;</td><td><input type = 'text' readonly name='ODamount' id='ODamount' class='form-control' value='". number_format($odamount, 2, '.',',') ."'></td></td><tr>";
          echo "</tr></tbody></table>";
                
          //Regular Deductions
            $totalregdeduc = 0;
            echo "<table class='table' border='0' width='100%' bordercolor='red'><thead>";
            echo "<tr><td colspan='2'><b>Regular Deductions</b></td></tr>";
            $reg = mysqli_query($con,"SELECT * FROM ssstb Where '$basicpay' >= MinRange AND '$basicpay' <= MaxRange");
            while($sss = mysqli_fetch_array($reg)){
            $totalregdeduc = $totalregdeduc + ($sss['EE'] / 2);
            echo "<tr>";
            echo "<td width='25%' align='left'>SSS: </td>";
            echo "<td><input type = 'text' class='form-control' readonly name='sssee' id='sssee' value='" . ($sss['EE'] / 2) . "'></td></tr>";
            }
           

            $reg2 = mysqli_query($con,"SELECT * FROM pagibigtb Where '$basicpay' >= RangeMin AND '$basicpay' <= RangeMax");
            while($pagibig = mysqli_fetch_array($reg2)){
            $pg = 50;    
            $totalregdeduc = $totalregdeduc + $pg;
            echo "<tr rowspan='2'>";
            echo "<td align='left'>Pagibig: </td>";
            echo "<td><input type = 'text' class='form-control' readonly name='pagibig' id='pagibig' value='" . $pg . "'></td></tr>";
            }

            $reg3 = mysqli_query($con,"SELECT * FROM philhealthtb Where '$basicpay' >= RangeMin AND '$basicpay' <= RangeMax");
            while($phil = mysqli_fetch_array($reg3)){
            $totalregdeduc = $totalregdeduc + ($phil['EE'] / 2);
            echo "<tr rowspan='2'>";
            echo "<td align='left'>Philhealth: </td>";
            echo "<td><input type = 'text' class='form-control' readonly name='phil' id='phil' value='" . ($phil['EE'] / 2) . "'></td></tr>";
            }

            // $salary = $basicpay - $totalregdeduc;

            // $reg4 = mysqli_query($con,"SELECT * FROM taxtb where '$salary' >= taxablemin and '$salary' <= taxablemax and status = '$mstat' and dependencies='$dep'");
            // while($tax = mysqli_fetch_array($reg4)){
            // if($basicpay <= 11000){
            //     $taxam = 0;
            // }
            // else
            // {
            //     $taxam = (($salary - $tax['exemp']) * $tax['perce']) + $tax['basetax'];
            // }
            // 
            // }
            // $totalregdeduc = $totalregdeduc + $taxam;

            echo "<tr rowspan='2'>";
            echo "<td align='left'>Tax: </td>";
            ?>
            <td><input type = 'number' class='form-control' onChange='TaxAdd(this.value)' value='0' name='tax' id='tax'></td></tr>
            <?php
            echo "<tr rowspan='2'>";
            echo "<td align='left'><b>Total Regular Deduction:  </td>";
            echo "<td><input type = 'text' name='grossdeduc' id='grossdeduc' readonly class='form-control' value='" .  $totalregdeduc . "'></td><tr>";

         echo "</table></thead>";
                //end

                $TotalGrossPay = $hrsworkpay + $hrsotpay + $inamount;
                $TotalDeductions = $totalregdeduc + $odamount + $hrsutpay;
                $TotalNet = $TotalGrossPay - $TotalDeductions;

                echo "<table class='table' width='100%' border='0'><center><thead>";
                    
                    echo "<tr><th>Net and Gross</th></tr>";
                    
                    echo "<tr align= 'left'>";
                    echo "<td width='25%'>Total Gross Pay:</td>";
                    echo "<td><input type = 'text' class='form-control' readonly name='grosspay' value='" .  $TotalGrossPay . "'></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td>Total Deductions:</td>";
                    echo "<td><input type = 'text' class='form-control' readonly name='totaldeduct' id='totaldeduct' value='" .  $TotalDeductions . "'></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td><b>Total Net Pay:</td>";
                    echo "<td><input type = 'text' class='form-control' readonly name='netpay' id='netpay' value='" .  $TotalNet . "'></td></tr>";


    ?>              <tr><th colspan='4'><center><input type='submit' name='SUBMIT' class='form-control btn btn-primary btn-block' value='Submit Payroll'></th></tr>

                    </table></form>

                        </div>
                    </div>
                </div>
            </div>
        <!-- /.container-fluid -->
        </div>
    <!-- /#page-wrapper -->
    </div>
<!-- /#wrapper -->

</body>
</html>