<?php
  session_start();
  $connection = mysql_connect('localhost', 'root');
  // Selecting Database
  $con = mysqli_connect("localhost","root","","ehrms");
  $db = mysql_select_db("ContentManagement", $connection);
  $error = "";

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--MODAL-->
    <script src="lib/js/jquery-1.11.2.min.js"></script>
    <script src="lib/js/bootstrap.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      $("#myModal").on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);  // Button that triggered the modal
            var titleData = button.data('title'); // Extract value from data-* attributes
            $(this).find('.modal-title').text(titleData + ' Form');
        });
    });
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./../Accountant.php">eBiZolution | Accountant </a>
            </div>
            <!-- /.navbar-header -->

           <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./../logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
                    <!-- /.dropdown-user -->
            <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>         
                            <a href="./../Accountant.php"><i class="fa fa-home"></i> Back to Main Menu</a>
                        </li>
                        <li>
                            <a href="OrganizationDescription.php"> Organization Description</a>
                        </li>
                        <li>
                            <a href="ServiceProvided.php"> Service Provided</a>
                        </li>
                        <li>
                            <a href="CompanyPhilosophy.php"> Company Philosophy</a>
                        </li>
                        <li>
                            <a href="NatureOfEmployment.php"> Nature of Employment</a>
                        </li>
                        <li>
                            <a href="Recruitment.php"> Recruitment</a>
                        </li>
                        <li>
                            <a href="Attendance.php"> Attendance</a>
                        </li>
                        <li>
                            <a href="RecordingAndReporting.php"> Recording & Reporting</a>
                        </li>
                        <li>
                            <a href="Holidays.php"> Holidays</a>
                        </li>
                        <li>
                            <a href="Overtime.php"> Overtime</a>
                        </li>
                        <li>
                            <a href="LeavingForOfficialBusiness.php"> Leaving for Official Business</a>
                        </li>
                        <li>
                            <a href="OfficialLeaves.php"> Offical Leaves & AWOL</a>
                        </li>
                        <li>
                            <a href="AbsenceWithPay.php"> Absence with Pay</a>
                        </li>
                        <li>
                            <a href="RulesOnNotification.php"> Rules on Notification</a>
                        </li>
                        <li>
                            <a href="PersonnelFiles.php"> Personnel Files</a>
                        </li>
                        <li>
                            <a href="SalaryAdministration.php"> Salary Administration</a>
                        </li>
                        <li>
                            <a href="EmployeeBenefits.php"> Employee Benefits</a>
                        </li>
                        <li>
                            <a href="WorkingCondition.php"> Working Condition</a>
                        </li>
                        <li>
                            <a href="Sources.php"> Sources</a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Company Philosophy</h1>
                    </div>
                        
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            
                           <?php

  $result = mysql_query("SELECT * FROM CompanyPhilosophy",$connection);
                              
  while($row = mysql_fetch_array($result)){ 

    echo "<b>Equal Emploment Opportunity</b><br>";

      echo $row['EEO'] . "<br><br>";
      echo $row['EEO1'] . "<br><br>";
      echo $row['EEO2'] . "<br><br>";
      echo $row['EEOA'] . "<br><br>";
      echo $row['EEOB'] . "<br><br>";
      echo $row['EEOC'] . "<br><br>";
      echo $row['EEOD'] . "<br><br>";

    echo "<center>-Exerpt from Resolution 89-463</center><br>";

      echo $row['Resolution1'] . "<br><br>";
      echo $row['Resolution2'] . "<br><br>";

    echo "<b>ANTI-SEXUAL HARRASMENT POLICY</b><br>";

      echo $row['ASHP1'] . "<br><br>";

    echo "<center>-Anti-Sexual Harassment Act of 1995 (NO. 7877)</center><br>";

      echo "<ul>";

      echo "<li>" . $row['ASHP2'] . "</li><br>";
      echo "<li>" . $row['ASHP3'] . "</li><br>";
      echo "<li>" . $row['ASHP4'] . "</li><br>";
      echo "<li>" . $row['ASHP5'] . "</li><br>";
      echo "<li>" . $row['ASHP6'] . "</li><br>";
      echo "<li>" . $row['ASHP7'] . "</li><br>";
      echo "<li>" . $row['ASHP8'] . "</li><br>";
      echo "<li>" . $row['ASHP9'] . "</li><br>";
      echo "<li>" . $row['ASHP10'] . "</li><br>";
      echo "<li>" . $row['ASHP11'] . "</li><br>";
      echo "<li>" . $row['ASHP12'] . "</li><br>";
      echo "<li>" . $row['ASHP13'] . "</li><br>";
      echo "<li>" . $row['ASHP14'] . "</li><br>";
      echo "</ul>";
  }

?>
                        </div>

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--MODAL-->
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Window</h4>
                </div>
                    <div class="modal-body">

                        <?php
                       $result = mysql_query("SELECT * FROM CompanyPhilosophy",$connection);
                    while($row = mysql_fetch_array($result))
                  {

                    echo "   <form name='input' method='post'> ";

                      echo "<b>Equal Emploment Opportunity</b><br>";
                        echo "<textarea cols='80' rows='10' name='EEO' id='EEO'>" . $row['EEO'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='EEO1' id='EEO1'>" . $row['EEO1'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='EEO2' id='EEO2'>" . $row['EEO2'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='EEOA' id='EEOA'>" . $row['EEOA'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='EEOB' id='EEOB'>" . $row['EEOB'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='EEOC' id='EEOC'>" . $row['EEOC'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='EEOD' id='EEOD'>" . $row['EEOD'] . "</textarea><br><br>";

                      echo "<center>-Exerpt from Resolution 89-463</center><br>";

                        echo "<textarea cols='80' rows='10' name='Resolution1' id='Resolution1'>" . $row['Resolution1'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='Resolution2' id='Resolution2'>" . $row['Resolution2'] . "</textarea><br><br>";

                      echo "<b>Anti-Sexual Harassment Policy</b><br>";

                        echo "<textarea cols='80' rows='10' name='ASHP1' id='ASHP1'>" . $row['ASHP1'] . "</textarea><br><br>";

                      echo "<center>-Anti-Sexual Harassment Act of 1995 (NO. 7877)</center><br>";

                        echo "<textarea cols='80' rows='10' name='ASHP2' id='ASHP2'>" . $row['ASHP2'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP3' id='ASHP3'>" . $row['ASHP3'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP4' id='ASHP4'>" . $row['ASHP4'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP5' id='ASHP5'>" . $row['ASHP5'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP6' id='ASHP6'>" . $row['ASHP6'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP7' id='ASHP7'>" . $row['ASHP7'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP8' id='ASHP8'>" . $row['ASHP8'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP9' id='ASHP9'>" . $row['ASHP9'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP10' id='ASHP10'>" . $row['ASHP10'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP11' id='ASHP11'>" . $row['ASHP11'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP12' id='ASHP12'>" . $row['ASHP12'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP13' id='ASHP13'>" . $row['ASHP13'] . "</textarea><br><br>";
                        echo "<textarea cols='80' rows='10' name='ASHP14' id='ASHP14'>" . $row['ASHP14'] . "</textarea><br><br>";

                      echo "<input type='Hidden' name='ID' id='ID' class='form-control' value=". $row['ID'] . "><br>"; 

                    echo " <p align='left'>
                           <input type='submit' value='Submit' class='btn btn-primary' name='submit'/>
                           <input type='reset' value='Reset' class='btn btn-danger' name='reset'/>
                           </p></form>";

    }
?>


                    </div>
                </div>
       
</body>
</html>

<?php

if(isset($_POST['submit']))
    {
        $ID = $_POST['ID'];
        $EEO = $_POST['EEO'];
        $EEO1 = $_POST['EEO1'];
        $EEO2 = $_POST['EEO2'];
        $EEOA = $_POST['EEOA'];
        $EEOB = $_POST['EEOB'];
        $EEOC = $_POST['EEOC'];
        $EEOD = $_POST['EEOD'];
        $Resolution1 = $_POST['Resolution1'];
        $Resolution2 = $_POST['Resolution2'];
        $ASHP1 = $_POST['ASHP1'];
        $ASHP2 = $_POST['ASHP2'];
        $ASHP3 = $_POST['ASHP3'];
        $ASHP4 = $_POST['ASHP4'];
        $ASHP5 = $_POST['ASHP5'];
        $ASHP6 = $_POST['ASHP6'];
        $ASHP7 = $_POST['ASHP7'];
        $ASHP8 = $_POST['ASHP8'];
        $ASHP9 = $_POST['ASHP9'];
        $ASHP10 = $_POST['ASHP10'];
        $ASHP11 = $_POST['ASHP11'];
        $ASHP12 = $_POST['ASHP12'];
        $ASHP13 = $_POST['ASHP13'];
        $ASHP14 = $_POST['ASHP14'];
        
        mysql_query("UPDATE CompanyPhilosophy SET EEO='$EEO', EEO1='$EEO1', EEO2='$EEO2', EEOA='$EEOA',
                                                  EEOB='$EEOB', EEOC='$EEOC', EEOD='$EEOD',
                                                  Resolution1='$Resolution1', Resolution2='$Resolution2',
                                                  ASHP1='$ASHP1', ASHP2='$ASHP2', ASHP3='$ASHP3',
                                                  ASHP4='$ASHP4', ASHP5='$ASHP5', ASHP6='$ASHP6',
                                                  ASHP7='$ASHP7', ASHP8='$ASHP8', ASHP9='$ASHP9',
                                                  ASHP10='$ASHP10', ASHP11='$ASHP11', ASHP12='$ASHP12',
                                                  ASHP13='$ASHP13', ASHP14='$ASHP14'
                                                  WHERE ID='$ID'", $connection);

         echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location.href='CompanyPhilosophy.php';
        </SCRIPT>");
    }

    ?>