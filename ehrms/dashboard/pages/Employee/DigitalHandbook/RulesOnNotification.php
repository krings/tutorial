<?php
  session_start();
  $connection = mysql_connect('localhost', 'root');
  // Selecting Database
  $con = mysqli_connect("localhost","root","","ehrms");
  $db = mysql_select_db("ContentManagement", $connection);
  $error = "";

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
 ?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--MODAL-->
    <script src="lib/js/jquery-1.11.2.min.js"></script>
    <script src="lib/js/bootstrap.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      $("#myModal").on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);  // Button that triggered the modal
            var titleData = button.data('title'); // Extract value from data-* attributes
            $(this).find('.modal-title').text(titleData + ' Form');
        });
    });
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./../Employee.php">eBiZolution | Employee</a>
            </div>
            <!-- /.navbar-header -->

         <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="../logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

           <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                    echo "<li> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>



            <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="../Employee.php"><i class="fa fa-home"></i> Back to Main Menu</a>
                        </li>
                        <li>
                            <a href="OrganizationDescription.php"> Organization Description</a>
                        </li>
                        <li>
                            <a href="ServiceProvided.php"> Service Provided</a>
                        </li>
                        <li>
                            <a href="CompanyPhilosophy.php"> Company Philosophy</a>
                        </li>
                        <li>
                            <a href="NatureOfEmployment.php"> Nature of Employment</a>
                        </li>
                        <li>
                            <a href="Recruitment.php"> Recruitment</a>
                        </li>
                        <li>
                            <a href="Attendance.php"> Attendance</a>
                        </li>
                        <li>
                            <a href="RecordingAndReporting.php"> Recording & Reporting</a>
                        </li>
                        <li>
                            <a href="Holidays.php"> Holidays</a>
                        </li>
                        <li>
                            <a href="Overtime.php"> Overtime</a>
                        </li>
                        <li>
                            <a href="LeavingForOfficialBusiness.php"> Leaving for Official Business</a>
                        </li>
                        <li>
                            <a href="OfficialLeaves.php"> Offical Leaves & AWOL</a>
                        </li>
                        <li>
                            <a href="AbsenceWithPay.php"> Absence with Pay</a>
                        </li>
                        <li>
                            <a href="RulesOnNotification.php"> Rules on Notification</a>
                        </li>
                        <li>
                            <a href="PersonnelFiles.php"> Personnel Files</a>
                        </li>
                        <li>
                            <a href="SalaryAdministration.php"> Salary Administration</a>
                        </li>
                        <li>
                            <a href="EmployeeBenefits.php"> Employee Benefits</a>
                        </li>
                        <li>
                            <a href="WorkingCondition.php"> Working Condition</a>
                        </li>
                        <li>
                            <a href="Sources.php"> Sources</a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header"> RULES ON NOTIFICATION</h1>
                    </div>
                        
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            
                             <?php
                      $result = mysql_query("SELECT * FROM rulesonnotification",$connection);
                            
                    while($row = mysql_fetch_array($result)){ 
                        echo $row['RulesonNotification'] . "<br><br>";
                       echo "<b>Paternity Leave</b><br><br>";
                        echo $row['Paternityleave1'] . "<br><br>";
                        echo $row['Paternityleave2'] . "<br><br>";  
                      echo "<b><center>- Paternity Leave Act of 1996</center></b><br><br>";
                        echo $row['Paternityleave3'] . "<br><br>";
                        echo $row['Paternityleave4'] . "<br><br>";
                        echo $row['Paternityleave5'] . "<br><br>";
                      echo "<b>Solo or Parents Leave</b><br><br>";
                        echo $row['Soloparentsleave1'] . "<br><br>";
                      echo "<b>-Excerpt from Republic Act No. 8972 or The Solo Parents' Welfare Act of 2000 </b><br><br>";
                        echo $row['Soloparentsleave2'] . "<br><br>";
                        echo $row['Soloparentsleave3'] . "<br><br>";
                        echo $row['Soloparentsleave4'] . "<br><br>";
                        echo $row['Soloparentsleave5'] . "<br><br>";
                        echo $row['Soloparentsleave6'] . "<br><br>";
                        echo $row['Soloparentsleave7'] . "<br><br>";
                        echo $row['Soloparentsleave8'] . "<br><br>";
                        echo $row['Soloparentsleave9'] . "<br><br>";
                        echo $row['Soloparentsleave10'] . "<br><br>";
                        echo $row['Soloparentsleave11'] . "<br><br>";
                        echo $row['Soloparentsleave12'] . "<br><br>";
                        echo $row['Soloparentsleave13'] . "<br><br>";
                      echo "<b>Bereavement Leave</b><br><br>";
                        echo $row['Bereavementleave'] . "<br><br>";
                      echo "<b>Residence Rendered Uninhabitable by Fire</b><br><br>";
                        echo $row['Residenceunhabitablebyfire'] . "<br><br>";
                      }

                  ?>
                        </div>

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--MODAL-->
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Window</h4>
                </div>
                    <div class="modal-body">

                        <?php
                       $result = mysql_query("SELECT * FROM rulesonnotification",$connection);
                        while($row = mysql_fetch_array($result)){

                        echo "   <form name='input' method='post'> ";
                        echo"      <label><font face='tahoma'>Rules on Notification</font>&nbsp;&nbsp;</label><br><br> 
                                   <textarea cols='80' rows='10' name='RulesonNotification' id='RulesonNotification' class='form-control' required>" . $row['RulesonNotification'] . "</textarea><br><br>";

                        echo"      <label><font face='tahoma'>Paternity Leave</font>&nbsp;&nbsp;</label><br><br> ";
                        echo"      <textarea cols='80' rows='10' name='Paternityleave1' id='Paternityleave1' class='form-control' required>" . $row['Paternityleave1'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Paternityleave2' id='Paternityleave2' class='form-control' required>" . $row['Paternityleave2'] . "</textarea><br><br>";

                        echo"      <label><font face='tahoma'><center>- Paternity Leave Act of 1996</center></font>&nbsp;&nbsp;</label><br><br> ";
                        echo"      <textarea cols='80' rows='10' name='Paternityleave3' id='Paternityleave3' class='form-control' required>" . $row['Paternityleave3'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Paternityleave4' id='Paternityleave4' class='form-control' required>" . $row['Paternityleave4'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Paternityleave5' id='Paternityleave5' class='form-control' required>" . $row['Paternityleave5'] . "</textarea><br><br>";

                        echo"      <label><font face='tahoma'><center>Solo or Parents Leave</center></font>&nbsp;&nbsp;</label><br><br> ";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave1' id='Soloparentsleave1' class='form-control' required>" . $row['Soloparentsleave1'] . "</textarea><br><br>";

                        echo"      <label><font face='tahoma'>-Excerpt from Republic Act No. 8972 or The Solo Parents' Welfare Act of 2000 </font>&nbsp;&nbsp;</label><br><br> ";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave2' id='Soloparentsleave2' class='form-control' required>" . $row['Soloparentsleave2'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave3' id='Soloparentsleave3' class='form-control' required>" . $row['Soloparentsleave3'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave4' id='Soloparentsleave4' class='form-control' required>" . $row['Soloparentsleave4'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave5' id='Soloparentsleave5' class='form-control' required>" . $row['Soloparentsleave5'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave6' id='Soloparentsleave6' class='form-control' required>" . $row['Soloparentsleave6'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave7' id='Soloparentsleave7' class='form-control' required>" . $row['Soloparentsleave7'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave8' id='Soloparentsleave8' class='form-control' required>" . $row['Soloparentsleave8'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave9' id='Soloparentsleave9' class='form-control' required>" . $row['Soloparentsleave9'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave10' id='Soloparentsleave10' class='form-control' required>" . $row['Soloparentsleave10'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave11' id='Soloparentsleave11' class='form-control' required>" . $row['Soloparentsleave11'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave12' id='Soloparentsleave12' class='form-control' required>" . $row['Soloparentsleave12'] . "</textarea><br><br>";
                        echo"      <textarea cols='80' rows='10' name='Soloparentsleave13' id='Soloparentsleave13' class='form-control' required>" . $row['Soloparentsleave13'] . "</textarea><br><br>";

                        echo"      <label><font face='tahoma'>Bereavement Leave</font>&nbsp;&nbsp;</label><br> <br>";
                        echo"      <textarea cols='80' rows='10' name='Bereavementleave' id='Bereavementleave' class='form-control' required>" . $row['Bereavementleave'] . "</textarea><br><br>";
                        echo"      <label><font face='tahoma'>Residence Rendered Uninhabitable by Fire</font>&nbsp;&nbsp;</label><br> <br>";
                        echo"      <textarea cols='80' rows='10' name='Residenceunhabitablebyfire' id='Residenceunhabitablebyfire' class='form-control' required>" . $row['Residenceunhabitablebyfire'] . "</textarea><br><br>";

                        echo"      <Input Type='Hidden' name='ID' id='ID' class='form-control' value=". $row['ID'] . ">
                                   <br>"; 

                        echo "     <p align='left'>
                                   <input type='submit' value='Submit' class='btn btn-primary' name='submit'/>
                                   <input type='reset' value='Reset' class='btn btn-danger' name='reset'/>
                                   </p></form>";


    }
?>

                    </div>
                </div>
       
</body>
</html>

<?php

if(isset($_POST['submit']))
    {
        $ID = $_POST['ID'];
        $RulesonNotification = $_POST['RulesonNotification'];
        $Paternityleave1 = $_POST['Paternityleave1'];
        $Paternityleave2 = $_POST['Paternityleave2'];
        $Paternityleave3 = $_POST['Paternityleave3'];
        $Paternityleave4 = $_POST['Paternityleave4'];
        $Paternityleave5 = $_POST['Paternityleave5'];
        $Soloparentsleave1 = $_POST['Soloparentsleave1'];
        $Soloparentsleave2 = $_POST['Soloparentsleave2'];
        $Soloparentsleave3 = $_POST['Soloparentsleave3'];
        $Soloparentsleave4 = $_POST['Soloparentsleave4'];
        $Soloparentsleave5 = $_POST['Soloparentsleave5'];
        $Soloparentsleave6 = $_POST['Soloparentsleave6'];
        $Soloparentsleave7 = $_POST['Soloparentsleave7'];
        $Soloparentsleave8 = $_POST['Soloparentsleave8'];
        $Soloparentsleave9 = $_POST['Soloparentsleave9'];
        $Soloparentsleave10 = $_POST['Soloparentsleave10'];
        $Soloparentsleave11 = $_POST['Soloparentsleave11'];
        $Soloparentsleave12 = $_POST['Soloparentsleave12'];
        $Soloparentsleave13 = $_POST['Soloparentsleave13'];
        $Bereavementleave = $_POST['Bereavementleave'];
        $Residenceunhabitablebyfire = $_POST['Residenceunhabitablebyfire'];

      
        
        mysql_query("UPDATE rulesonnotification SET RulesonNotification='$RulesonNotification', Paternityleave1='$Paternityleave1', Paternityleave2='$Paternityleave2' , Paternityleave3= '$Paternityleave3' , Paternityleave4= '$Paternityleave4' , Paternityleave5= '$Paternityleave5' , Soloparentsleave1= '$Soloparentsleave1' , Soloparentsleave2= '$Soloparentsleave2' , Soloparentsleave3= '$Soloparentsleave3' , Soloparentsleave4= '$Soloparentsleave4' , Soloparentsleave5= '$Soloparentsleave5' , Soloparentsleave6= '$Soloparentsleave6' , Soloparentsleave7= '$Soloparentsleave7' , Soloparentsleave8= '$Soloparentsleave8' , Soloparentsleave9= '$Soloparentsleave9' , Soloparentsleave10= '$Soloparentsleave10' , Soloparentsleave11= '$Soloparentsleave11' , Soloparentsleave12= '$Soloparentsleave12' ,Soloparentsleave13= '$Soloparentsleave13' , Bereavementleave= '$Bereavementleave' , Residenceunhabitablebyfire= '$Residenceunhabitablebyfire' WHERE ID='$ID'", $connection);

        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location.href='RulesOnNotification.php';
        </SCRIPT>");
    }

    ?>