<?php

session_start();

unset($_SESSION["Uname"]);
unset($_SESSION["Name"]);
session_unset($_SESSION["Uname"]);
session_unset($_SESSION["Name"]);
session_unset();

header("location: ../../../index.php");

?>