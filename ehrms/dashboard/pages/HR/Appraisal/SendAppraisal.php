<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}


if(isset($_POST['submit2'])){
    $me =  $_SESSION["Id"];
    $subj = "Performance Appraisal Ticket";
    $Reciever = $_POST['receiver'];
    $mess = "You are chosen to be the rater for this specific Performance Appraisal. Please Accomplish Immediately.";
    $dat =  date('Y-m-d');
    $ratee = $_POST['ratee'];
    $testid = $_POST['test'];
    $query = "INSERT INTO dashboardtb (Subject, Sender, Reciever, Date, Status, Type, Message, Ratee, TestId) VALUES('$subj','$me','$Reciever','$dat','1','2','$mess','$ratee','$testid')";
    mysqli_query($con,$query);
}
  
  
 ?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./../HR.php">eBiZolution | HR </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a><ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./../logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
           </ul>
          

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                    echo "<li> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


             <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="./../PersonnelManagement.php"><i class="fa fa-male"></i> Personnel Management</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./../SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./../Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./../Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./../Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./../Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./../Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./../Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./../SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="./../DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </nav>


        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Send Appraisal</h1>
                    </div>
                    <form method = "post" enctype="multipart/form-data" name="Form" action='SendAppraisal.php'>

                        <script>
                        function Search(id)
                                {
                                     if (window.XMLHttpRequest) {
                                     // code for IE7+, Firefox, Chrome, Opera, Safari
                                     xmlhttp=new XMLHttpRequest();
                                    } else { // code for IE6, IE5
                                      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
                                                         
                                    var params = "id=" + id; 
                                    xmlhttp.open("GET","searchemp.php?" + params, true);
                                    xmlhttp.send();
                                

                                  xmlhttp.onreadystatechange=function() {
                                     if (xmlhttp.readyState==4 && xmlhttp.status==200)
                                        {
                                            document.getElementById('rsp_search').innerHTML=xmlhttp.responseText;
                                        }
                                        }
                              }  
                        </script>
  
                        <?php
          
          $result = mysqli_query($con,"SELECT * FROM masteremptb m Inner join masterpersonaltb  emp on emp.EmpId = m.EmpId inner join positionstb p on emp.PosId = p.PosId  Where AStat = '1' Order By Name") or die('Error: ' .  mysqli_error($con));
          echo "<div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            Simple Query Ticket Form
                        </div>
                        <!-- /.panel-heading -->
                        <table class='table table-striped table-bordered table-hover' border='1'>";

                            echo '<td colspan="1"><b>Employee </b></td><td colspan = "4"><select class="form-control" name="ratee" onChange="Search(this.options[this.selectedIndex].value)"><option value=""> </option>';
                                        while($row = mysqli_fetch_array($result)) {
                                     echo '<option value="' . $row["EmpId"] . '">' . $row["Name"] . '</option>';
                                        }     
                            echo "</option></td></tr>"; 
                            echo "<th colspan ='4'><div id='rsp_search'> </div></th>";

                            echo "<tr><td colspan='2' align='right'>
                                    <input type='Submit' name='submit2' value='Submit' class='btn btn-primary btn-outline'/>
                                    <input type='reset' value='Reset' onClick='confirm('Are you sure?')' class='btn btn-danger btn-outline'/></th>";


            echo "</form></table>
                            </div>
                            <!-- /.table-responsive -->
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>";
            ?>
                      
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>
