<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../../index.php");
}
  
 ?>



<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   <script>
    $(document).ready(function() {
        $('#yayadab').DataTable({
                responsive: true,
                "order": [ 1, 'asc' ]
        });
    });
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./../HR.php">eBiZolution | HR </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                        <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./../logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
           </ul>
           

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                    echo "<li> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


             <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="./../PersonnelManagement.php"><i class="fa fa-male"></i> Personnel Management</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./../SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./../Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./../Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./../Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./../Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./../Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./../Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./../Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./../SimpleQueryTicket.php"><i class="fa fa-envelope"></i> Simple Query Ticket</a>
                        </li>

                        <li>
                            <a href="./../DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Performance Appraisal</h1>
                        <center>
                    </div>

                <?php

 $EvalueeId = $_GET['Evaluee'];
 $myname = $_SESSION['Name'];

echo "<form method = 'POST' enctype='multipart/form-data' onSubmit='return checkForm(this);'>";

 $rateequery = mysqli_query($con,"SELECT * FROM masteremptb ma inner join masterpersonaltb as m on ma.EmpId = m.EmpId inner join positionstb as p on p.PosId = m.PosId where ma.EmpId = '$EvalueeId'");
           echo "<div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            Performance Appraisal Form                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>
                            <div class='dataTable_wrapper'>
                           

                                <table class='table table-striped table-bordered table-hover' width='100%'>";
                                  while($r = mysqli_fetch_array($rateequery)){
                                echo "<tr><td><b>Employee Name: </b></td><td colspan='3'>" . $r['Name'] . "</td></tr>";    
                                echo "<tr><td><b>Company : </b></td><td>" . $r['Company'] . " </td> <td><b>Department : </b> </td> <td>" . $r['Department'] . " </td> </tr>";
                                echo "<tr><td><b>Position : </b></td><td>" . $r['Description'] ."</td> <td><b>Date Hired : </b></td> <td>" . $r['DateHire'] . " </td> </tr>";
                                echo "<tr><th colspan='4'><center> <b>Evaluation Period: " . date("F Y", strtotime($r['LastAppraisal'])) . " - " . date('F Y') . " </b></th></tr>";   
                                $_SESSION['LA'] = $r['LastAppraisal'];
                                  }                  

                                echo "
                                <tr><th colspan='4'><center><h4><b><u>Performance Rating Guide</b></u></h4></th></tr>
                                <tr><td colspan='1'><center><b>Excellent - 5</b></td><td colspan='3'><center>Very Exceptional performance; rarely equaled.</td></tr>
                                <tr><td colspan='1'><center><b>Very Satisfactory - 4</b></td><td colspan='3'><center> Better performance than is normally expected, usually exceeds requirements of position.</td></tr>
                                <tr><td colspan='1'><center><b><b>Satisfactory - 3</b></td><td colspan='3'><center> Normally expected performance; consistently meets requirements of position.</td></tr>
                                <tr><td colspan='1'><center><b><b>Below Satisfactory - 2</b></td><td colspan='3'><center> Performance is less than normally expected.</td></tr>
                                <tr><td colspan='1'><center><b><b>Poor - 1</td><td colspan='3'><center> Fails to meet even the minimum standards.</td></tr></table>
                                ";

                                echo "<table class='table table-striped table-bordered table-hover'>";
                                echo "<thead><tr><th colspan='8'><center><b><h4><u> Appraisal Evaluation Results</u></b></h4></th></tr>";
                                echo "
                                        <th><center> Test</th>
                                        <th><center> Grade</th>
                                        <th><center> Remarks</th>
                                        </thead><tbody>
                                ";
                                $countall = 0;
                                $aveall = 0;
                                $testquery = mysqli_query($con,"SELECT *,DATEDIFF(Now(),LastAppraisal) as Month, AVG(Grade) as Ave, ma.LastAppraisal FROM appraisaldatatb a inner join masteremptb ma on a.Evaluee = ma.EmpId Where a.Evaluee = '$EvalueeId' Group by AppId");
                                while($row = mysqli_fetch_array($testquery)){
                                    $musid = $row['MusId'];
                                    
                           
                                            $testid = $row['TestId'];
                                            $testquery1 = mysqli_query($con,"SELECT testname FROM testtb Where testID = '$testid'");
                                            $ti = mysqli_fetch_array($testquery1);
                                            $AppIds = $row['AppId'];
                                            $testquery2 = mysqli_query($con,"SELECT * FROM appraisaldatatb a Where AppId = '$AppIds'");
                                            $rem = "";
                                            while($ma = mysqli_fetch_array($testquery2)){
                                             $rem = $rem . $ma['QuestionName']. " : " . $ma['Remarks'] . "<br>";
                                            }
                                                $aveall = $aveall + $row['Ave'];
                                                echo "<tr><td><input readonly type='hidden' id='test".$countall."' value='" . $ti['testname']. "'>" . $ti['testname']. "</td>";
                                                echo "<td><center><input readonly type='hidden' id='grade".$countall."' value='" . number_format($row['Ave'], 2, '.',','). "'>" . number_format($row['Ave'], 2, '.',','). "</td>";
                                                echo "<td><input type='hidden' class='remarks' readonly name='remarks".$countall."' id='remarks".$countall."' value='".$rem."'>" . $rem . "</td></tr>";
                                                $countall +=1;
                                        
                                }


                                $ave2 = $aveall / $countall;
                                 if($ave2 <= 3 & $ave2>= 2){
                                    $equv = "Below Satisfactory";
                                }
                                elseif($ave2 <= 4 && $ave2>=3){
                                    $equv = "Satisfactory";
                                }
                                elseif($ave2 >= 4 && $ave2< 5){
                                    $equv = "Very Satisfactory";
                                }
                                elseif($ave2 == 5){
                                    $equv = "Excellent";
                                }
                                elseif($ave2 > 2){
                                    $equv = "Poor";
                                }
                                echo "<tr><td><b> Total Average: </td><td><input readonly class='form-control' type='hidden' id='average' name='average' value='" . number_format($ave2, 2, '.',',') ."'><b>" . number_format($ave2, 2, '.',',') ."</td><td><b><u><center>$equv</u></b></td></tr>";
                                echo "<tr><td colspan='2'><b> Final Remarks: </td><td><textarea id = 'finalrem' class='form-control' cols='60' rows='5'></textarea> </td></tr>";
                                echo "</table>";
                                $style = 0;
                                while($style <= $countall)
                                {
                                    echo "

                                            <style>

                                                #remarks$style {
                                                resize: none;
                                                }

                                            </style>

                                    ";
                                    $style +=1;
                                }
                                echo "

                                            <style>

                                                #finalrem {
                                                resize: none;
                                                }

                                            </style>

                                    ";
                                echo "<table width='100%' border='0'>";
                                echo "<tr><td align='left'><a href='AppraiseFinal.php' role='button' type='button' class='btn btn-default btn-outline'>Back</a></td>";
                                echo "<td align='right'><input type='submit' name='submit' class='btn btn-primary btn-outline btn-md ' value='Finish Evaluation'></td></form>";
                            
                                   echo " 
                            </div>
                            <!-- /.table-responsive -->
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>";
            ?>        

<script type="application/javascript">
function checkForm(form)
{    
      if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}

    var inv = document.getElementsByClassName("remarks");
    var xx = document.getElementsByClassName("remarks").length;
    var i = 1;
    var o = 0;

        while (i <= xx){

                var questionname = document.getElementById("test" + o).value;
                var grade = document.getElementById("grade" + o).value;
                var ratee = <?php echo $EvalueeId; ?>;
                var remarks = inv[i - 1].value;
                var ave = document.getElementById("average").value;
                var finalrem = document.getElementById("finalrem").value;
                o+=1;

                var params = "ratee=" + ratee + "&testname=" + questionname + "&finalrem=" + finalrem + "&remarks=" + remarks + "&grade=" + grade + "&ave=" + ave;
                xmlhttp.open("POST","AddAppraisalCode2.php", false);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send(params); 
                i+=1;

            }

    xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    }}
      
        sendnoti();
        window.alert('Evaluation Successful!');
        window.location.href = 'AppraiseFinal.php';
        return true;
           
    
}


function sendnoti(){

              if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
    
         var ratee = <?php echo $EvalueeId; ?>; 

 
        var paramss = "Ratee=" + ratee;
        xmlhttp.open("POST","AddNotiCode2.php",true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send(paramss);   

            xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    }}

}     


</script>                
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


</body>
</html>

                  