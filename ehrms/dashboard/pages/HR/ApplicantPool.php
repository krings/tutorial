<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
require_once('connection.php');
 
//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}

 ?>
<!DOCTYPE html>
    <html lang="en">
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                "order": [ 1, 'desc' ]
        });
    });
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


              <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
               </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Applicant Pool</h1>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <b>Applicant Pool</b>
                        </div>
                        <div class="panel-body">
                    <!-- /.col-lg-12 -->


               

<?php
 $qry = "SELECT T.TicketId, T.Level, T.Status, T.Remarks, P.Name, C.Location, L.Description FROM ticketpersonaltb as P INNER JOIN tickettb as T ON T.TicketId = P.TicketId  AND T.Level <= 10
 INNER JOIN tempuploadstb as C on T.TicketId = C.TicketId INNER JOIN recviewtb as L on T.Level = L.LevelId Order By T.TicketId Desc";
 $result = mysql_query($qry) or die('Error: ' + mysql_error);
 

 echo "<div class='panel-body'>
                            <div class='dataTable_wrapper'>
                                <table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'>
                                    <thead>
                                        <tr>
                                           <td align='center' width='10%'><b>Ticket ID</b></td>
                                           <td align='center' width='10%'><b>Image</b></td>
                                           <td align='center'width='10%'><b>Name</b></td>
                                           <td align='center'width='10%'><b>Level</b></td>
                                           <td align='center' width='8%'><b>Status</b></td>
                                           <td align='center' width='25%'><b>Remarks</b></td>
                                           <td align='center'width='5%'><b>Option</b></td>
                                           <td align='center' width='12%'><b>View Profile</b></td>
                                        </tr>
                                    </thead>
                                    <tbody> "; 

                while($row = mysql_fetch_array($result)) {
                echo "<tr>";
                        echo "<td><center>" . $row['TicketId'] . "</center></td>";   
                        echo "<td><center><img src='" . $row['Location'] . "' width='80' height='80'></center></td>";
                        echo "<td><center>" . $row['Name'] . "</center></td>";
                        echo "<td><center>" . $row['Description'] . "</center></td>";
                        if ($row['Status'] == 1)
                        {
                            echo "<td><center>Active</center></td>";
                        }
                        else
                        {
                            echo "<td><center>Inactive</center></td>";
                        }
                        
                        echo "<td><center>" . $row['Remarks'] . "</center></td>";

                if ($row['Level'] == 9)
                {
                    echo "<td><center><a href='Rec9.php?TicketId=". $row['TicketId'] ."' ><input type='submit' value='Hire!' class='btn btn-outline btn-sm btn-primary' name='edit'/></a></center></td>";
        
                }
                elseif($row['Level'] == 10)
                {
                   echo "<td> </td>";
                }
                else
                {
                    echo "<td><center><a href='?TicketId=". $row['TicketId'] ."' ><input type='submit' class='btn btn-outline btn-sm btn-primary'  value='Re-Evaluate!' name='edit' /></a></center></td>";
                }
                echo "<td><center><a href='TicketProfile.php?TicketId=". $row['TicketId'] ."' ><input type='submit' class='btn btn-outline btn-sm btn-primary' value='View' name='edit' /></a></center></td>";
                echo "</tr>";

                }      
                echo "</tbody>
                      
                      </table></div>";

  ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
</html>