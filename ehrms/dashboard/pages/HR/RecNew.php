<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");
require_once('connection.php');

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

     <style>
    #reasons {
    resize: none;
    }


    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

               <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
            

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


             <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h2 class="page-header">EHRMS | Recruitment</h1>
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <b>Recruitment</b>
                        </div>
                        <div class="panel-body">
<?php

if(isset($_POST['submit2']))
{
    
    if($_POST['Emp'] == "Pass")
    {
        $con = mysqli_connect('localhost', 'root', '', 'ehrms');
        if (mysqli_connect_errno()) {
            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
        }

        $id = $_GET['TicketId'];
        $rem = $_POST['textarea'];
        $result = mysqli_query($con,"Select Level From tickettb where TicketId = '$id'");
        $row = mysqli_fetch_array($result);
        $level = $row['Level'];
        $lev = $level + 1;

        $as = mysql_query("SELECT Remarks FROM tickettb where TicketId = '$id'");
        $as2 = mysql_fetch_array($as);  
        $remarkold = $as2['Remarks'];

        $asd = mysql_query("SELECT Description FROM recviewtb where LevelId = '$level'");
        $asd2 = mysql_fetch_array($asd);    
        $levnames = $asd2['Description'];
        if($remarkold == "None" or $remarkold == "")
        {
            $rem2 = $levnames . ": " . $rem;
        }
        else
        {
            $rem2 = $remarkold . "\n" . $levnames . ": " . $rem;
        }

    
        $qryUp = "UPDATE tickettb SET Level = '$lev', Remarks = '$rem2' WHERE TicketId = '$id' ";
        mysqli_query($con,$qryUp) or die('Error: ' . mysqli_error($con));
        echo "<meta http-equiv=\"refresh\" content=\"0; URL=Monitoring.php\">";
    }
    else
        
    {
        $con = mysqli_connect('localhost', 'root', '', 'ehrms');
        if (mysqli_connect_errno()) {
            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
        }

        $id = $_GET['TicketId'];
        $result = mysqli_query($con,"Select Description From tickettb where TicketId = '$id'");
        $row = mysqli_fetch_array($result);
        $level = $row['Description'];
        $rem = $_POST['textarea'];
        $as = mysql_query("SELECT Remarks FROM tickettb where TicketId = '$id'");
        $as2 = mysql_fetch_array($as);  
        $remarkold = $row['Remark'];
        $asd = mysql_query("SELECT Description FROM recviewtb where LevelId = '$level'");
        $asd2 = mysql_fetch_array($asd);    
        $levnames = $row['Description'];
        $rem2 = $remarkold . "\n" . $levnames . ": " . $rem . " Failed The $level of The Recruitment";
    
        $qryUp = "UPDATE tickettb SET Status = '0', Remarks = '$remarks' WHERE TicketId = '$id' ";
        mysqli_query($con,$qryUp) or die('Error: ' . mysqli_error($con));   
        echo "<meta http-equiv=\"refresh\" content=\"0; URL=Monitoring.php\">";
    }
    
}

?>

<style>
#textarea {
    resize: none;
    }
</style>

<?php

$id = $_GET['TicketId'];

$qry = "SELECT * FROM tickettb as T INNER JOIN ticketpersonaltb as E WHERE T.TicketId = $id AND E.TicketId = $id";
$result = mysql_query($qry) or die('Error: ');
while($row = mysql_fetch_array($result))
{
    $level = $row['Level'];
    $name = $row['Name'];   
    $Position = $row['PosId'];
}

    $re = mysql_query("SELECT Description FROM recviewtb where LevelId = $level");
    $row = mysql_fetch_array($re);
    $levname = $row['Description'];

    $res = "SELECT Description From positionstb where PosId = '$Position'";
    $resultt = mysql_query($res);
    $rowt = mysql_fetch_array($resultt);
    $positions = $rowt['Description'];


    ?>

</head>
<body>
<center>

<form method="POST" enctype="multipart/form-data" >


    <br>
    <img src="../Uploads/Images/Logo.png" width="279" height="120" /> <br><br>   
                
                   <div class="row">
                        <div class="col-lg-12">
                         <div class="form-group">
                            <table border="0" width="100%">
                                <tr>
                                    <td width="7%"> <label>&nbsp;Name: </label> </td> 
                                    <td width="40%"> <input class="form-control" placeholder="Name"></td>
                                    <td width="3"></td>
                                    <td width="7%"> <label>&nbsp;Position: </label> </td> 
                                    <td width="43%"> <input class="form-control" placeholder="Position"></td>
                                <br>
                                </tr>
 
                                <tr>
                                <td><br></td>
                                </tr> 

                                <tr>
                                    <td width="7%"> <label>&nbsp;Level: </label> </td> 
                                    <td width="40%"> <input class="form-control" placeholder="Level"> </td>
                                    <td width="3"></td>
                                    <td width="7%"> <label>&nbsp;Remarks: </label> 
                                    <td width="43%"> <textarea id="reasons" name="reasons" class="form-control" rows="3" placeholder="Remarks" ></textarea> 
                                </tr>
                            </table>
                        </div>
                        </div>
                    </div>


    <table border="0" width="100%">
    <tr>
    <td height="80">&nbsp;</td>
    <td><div align="center"><img src="../Uploads/Images/pass.png" width="241" height="137"><br><input type="radio" id="Emp21" name="Emp" required value="Pass"></div></td>
    

    <td><div align="center"><img src="../Uploads/Images/fail.png" width="239" height="142"><br><input type="radio" id="Emp2" name="Emp" required value="Fail"></div></td>
    <td>&nbsp; </td>
    </tr>


    <th height="50" colspan="4">
        <p align="center">
            <?php
        echo "<a href='Recruitment2-8.php?TicketId=". $id ."' >  
        <button type='button' class='btn btn-primary btn-outline' data-toggle='modal' data-target='#myModal' data-title='Edit'><font size='3'>Submit</font></button></div>&nbsp;&nbsp;
         </a>";
        ?>


        </p>
    </th>
    </table>
    </center>
<br>
</td>
</tr>
</table>
<br>
<br>

</form>
</center>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>