<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}


if(isset($_POST['Username']))
{

    if($_POST['pw1'] == $_POST['pw2'])
    {
        $Username =  $_POST['Username'];
        $PW = $_POST['pw1'];
        $Level = $_POST['level'];
        $EmpId = $_POST['empid'];
    
        $query = "INSERT INTO accounttb (Username,Password,Level,EmpId,Status) VALUES('$Username','$PW','$Level','$EmpId','1')";
        if(mysqli_query($con,$query))
        {
            echo "<script> alert('Successfully Created'); </script>";
            header("location: ./Accounts.php");
        }


    }

}
  
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

          <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


              <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
               </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Add Account</h1>  </div>

                        <div class="panel panel-primary">
                        <div class="panel-heading">
                        <b>Add Account</b>
                        </div>
                        <div class="panel-body">

                        <form method = "post" enctype="multipart/form-data" name="Form" action='AddAccount.php'>
               <?php

          $result = mysqli_query($con,"SELECT *, m.EmpId as Acc, a.EmpId as Emp FROM masterpersonaltb as M Left Join accounttb as A on M.EmpId = A.AccId INNER JOIN positionstb as p on M.PosId = p.PosId") or die('Error: ' .  mysqli_error($con));
          echo "<div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-default'>
                        <!-- /.panel-heading -->

                    <table class='table table-striped table-bordered table-hover' id='dataTables-example'>
                                    <thead>
                                        <tr>
                                            <th colspan='2'><b><center>ACCOUNT CREATION</th>
                                        </tr>
                                    </thead>";

                            echo '<td><b>Employee Name / Position: </b></td><td><select class="form-control" name="empid" onChange="Validate("checkNull", this.options[this.selectedIndex].value, "rsp_" + this.id, "")">';
                                        while($row = mysqli_fetch_array($result)) {
                                            if($row['EmpId'] == "")
                                            {
                                              echo '<option value="' . $row["Acc"] . '">' . $row["Name"] . ' / ' . $row['Description'] .  '</option>';
                                            }
                       
                                        }     
                            echo "</option></td></tr>"; 

                            echo "<tr><td><b>Username:</b> </td><td><input type = 'text' class='form-control' name='Username'></td></te>";
                            echo "<tr><td><b>Password:</b> </td><td><input type = 'password' class='form-control' name='pw1'></td></te>";
                            echo "<tr><td><b>Confirm Password:</b> </td><td><input type = 'password' class='form-control' name='pw2'></td></te>";
                            echo "<tr><td><b>User Level:</b> </td><td><select class='form-control' name='level'>
                                    <option value='admin'>Admin</option>
                                    <option value='1'>Employee</option>
                                    <option value='2'>Accountant</option>
                                    <option value='3'>Human Resource</option>
                                    <option value='4'>Supervisor</option>
                            </td></tr>

                            </table>
                            
                            </div>
                            <!-- /.table-responsive -->
                            
                            <!-- /.table-responsive -->";   

            echo" <center>
            <table width = '41%'>
            <tr>
            <td width ='20%'>
            <button type='button' class='btn btn-primary btn-outline btn-lg btn-block'data-toggle='modal' data-target='#myModal' data-title='Edit' >Submit</button>
            </td>
            <td width='1%'> </td>
            <td width ='20%'>
            <button type='button' value='Reset' onClick='confirm('Are you sure?')' class='btn btn-outline btn-danger btn-lg btn-block' data-toggle='modal' data-target='#myModal' data-title='Edit' >Reset</button>
            </center>"; 
                            


            echo "
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>";
            ?>
   


                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>