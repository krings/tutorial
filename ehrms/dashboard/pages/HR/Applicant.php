<?php

session_start();
require_once('connection.php');
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

<style>
#WorkReason3 {
    resize: none;
    }
#WorkReason2 {
    resize: none;
    }
#WorkReason1 {
    resize: none;
    }
#ColDetails{
    resize: none;
    }
#FGradDetails {
    resize: none;
    }
#GradDetails {
    resize: none;
    }


</style>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR </a>
            </div>
            <!-- /.navbar-header -->


             <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

              <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
               </nav>

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Applicant Form</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

<?php

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}


if (isset($_FILES['picture']['tmp_name']))
        {
                   $r = mysqli_query($con,"SELECT COUNT(TicketId) as C FROM tickettb");
                    while($ro = mysqli_fetch_array($r))
                    {
                        $ids = $ro['C'];    
                    }
                $lo = '../Uploads/Pictures/';
                $loc = AddSlashes($lo. $_FILES['picture']['name']);     
                $file = $_FILES['picture']['tmp_name'];
                $filename = $_FILES['picture']['name'];
                
                if(!move_uploaded_file($file, $loc))
                {
                    echo "<script> alert('Error!'); </script>";
                }
                else
                {
                    $and = "INSERT INTO tempuploadstb (TicketId, Location, Name) VALUES('$ids', '$loc', '$filename')";
                    mysqli_query($con,$and) or die('Error: ' . mysqli_error($con));
                    echo "<meta http-equiv=\"refresh\" content=\"0; URL=Monitoring.php\">";
                }
                
        }
        
?>
   

<script type="application/javascript">

window.onunload = function() {
    return "Warning: Leaving this page will result in any unsaved data being lost. Are you sure you wish to continue?";
};

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#PictureMismo')
        .attr('src', e.target.result)
        .width(162)
        .height(144);
    };
    reader.readAsDataURL(input.files[0]);
  }
  
}


function isNumberKey(evt)
          {
             var charCode = (evt.which) ? evt.which : event.keyCode
             if (charCode > 32 && (charCode < 48 || charCode > 57))
                {return false;}
             return true;
          }
    
function isLetterKey(evt)
      {
      evt = (evt) ? evt : event;
      var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
        if (charCode > 32 && (charCode < 65 || charCode > 90) &&
                (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
      }
      
function isEmailKey(evt)
      {
      evt = (evt) ? evt : event;
      var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
        if (charCode > 31 && (charCode < 45 || charCode > 47) &&
                (charCode < 64 || charCode > 122) && (charCode < 97 || charCode >122)) {
            return false;
        }
        return true;
      }

function checkForm(form)
{    

        if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}


    if (this . picture . value == "") 
    {
        alert("Choose Image First!");
        this . picture . focus();
        return false;
    }
    
            var counts = 0;
            var pl = 1;
            
        var name = this . Name . value; 
        var comp = this . Company . options[Company.selectedIndex] . value;
        var pos = this . Pos . options[Pos.selectedIndex] . value;
        var add = this . Add . value;   
        var cnum = this . Cnum . value; 
        var eadd = this . Eadd . value;
        var dob = this . Dob . value;   
        var mstat = this . Mstat . value;   
        var tin = this . Tin . value;   
        var sss = this . SSS . value;
        var pagibig = this . Pagibig . value;   
        var philhealth = this . Phealth . value;    
        var hs = this . FGrad . value;  
        var hsd = this . FGradDate . value; 
        var hsr = this . FGradDetails . value;
        var col = this . Col . value;   
        var cold = this . ColDate . value;  
        var colr = this . ColDetails . value;   
        var grad = this . Grad . value;
        var gradd = this . GradDate . value;    
        var gradr = this . GradDetails . value;

        var work1 = this . Work1 . value;
        var work2 = this . WorkAdd1 . value;
        var work3 = this . WorkDate1 . value;
        var work4 = this . WorkEndDate1 . value;
        var work5 = this . WorkPos1 . value;
        var work6 = this . WorkReason1 . value;

        var work11 = this . Work2 . value;
        var work22 = this . WorkAdd2 . value;
        var work33 = this . WorkDate2 . value;
        var work44 = this . WorkEndDate2 . value;
        var work55 = this . WorkPos2 . value;
        var work66 = this . WorkReason2 . value;

        var work111 = this . Work3 . value;
        var work222 = this . WorkAdd3 . value;
        var work333 = this . WorkDate3 . value;
        var work444 = this . WorkEndDate3 . value;
        var work555 = this . WorkPos3 . value;
        var work666 = this . WorkReason3 . value;


        var params = "name=" + name + "&company=" + comp + "&pos=" + pos + "&add=" + add + "&cnum=" + cnum + "&eadd=" + eadd + "&dob=" + dob + "&mstat=" + mstat + "&tin=" + tin + "&sss=" + sss + "&pagibig=" + pagibig + "&philhealth=" + philhealth + "&FGrad=" + hs + "&FGradDate=" + hsd + "&FGradDetails=" + hsr + "&col=" + col + "&coldate=" + cold + "&coldetails=" + colr + "&grad=" + grad + "&graddate=" + gradd + "&graddetails=" + gradr + "&Emps1=" + work1 + "&WorkAdd1=" + work2 + "&WorkDate1=" + work3 + "&WorkEndDate1=" + work4 + "&WorkPos1=" + work5 + "WorkReason1=" + work6 + "&Emps2=" + work11 + "&WorkAdd2=" + work22 + "&WorkDate2=" + work33 + "&WorkEndDate2=" + work44 + "&WorkPos2=" + work55 + "&WorkReason2=" + work66 + "&Emps3=" + work111 + "&WorkAdd3=" + work222 + "&WorkDate3=" + work333 + "&WorkEndDate3=" + work444 + "&WorkPos3=" + work555 + "&WorkReason3=" + work666;
       
        xmlhttp.open("POST","AddApplicantCode.php", false);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send(params);           

        
    xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        document.getElementById('Msg').innerHTML=xmlhttp.responseText;
    }}

    
}


function Validate(method, value, target, table)
        {
             if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
                                 
            var params = "method=" + method + "&value=" + value + "&Table=" + table; 
            xmlhttp.open("GET","validate.php?" + params, true);
            xmlhttp.send();
        

          xmlhttp.onreadystatechange=function() {
             if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    document.getElementById(target).innerHTML=xmlhttp.responseText;
                }
                }
      }  
      

function not()
{
    if(document.getElementById('NotAttained').checked)
    {
        document.getElementById('Grad1').checked = true;
        document.getElementById('Grad2').checked = true;
        document.getElementById('Grad3').checked = true;
        
        document.getElementById('Grad').disabled = true;
        document.getElementById('GradDate').disabled = true;
        document.getElementById('GradDetails').disabled = true;
        
    }
    else
    {
        document.getElementById('Grad1').checked = false;
        document.getElementById('Grad2').checked = false;
        document.getElementById('Grad3').checked = false;
        
        document.getElementById('Grad').disabled = false;
        document.getElementById('GradDate').disabled = false;
        document.getElementById('GradDetails').disabled = false;
    }
    
    
}

function not2()
{
    if(document.getElementById('NotAttained2').checked)
    {
        document.getElementById('Grad11').checked = true;
        document.getElementById('Grad22').checked = true;
        document.getElementById('Grad33').checked = true;
        
        document.getElementById('FGrad').disabled = true;
        document.getElementById('FGradDate').disabled = true;
        document.getElementById('FGradDetails').disabled = true;
        
    }
    else
    {
        document.getElementById('Grad11').checked = false;
        document.getElementById('Grad22').checked = false;
        document.getElementById('Grad33').checked = false;
        
        document.getElementById('FGrad').disabled = false;
        document.getElementById('FGradDate').disabled = false;
        document.getElementById('FGradDetails').disabled = false;
    }

}

function not3()
{
    if(document.getElementById('NotAttained3').checked)
    {
        document.getElementById('Work1').disabled = true;
        document.getElementById('WorkAdd1').disabled = true;
        document.getElementById('WorkDate1').disabled = true;
        document.getElementById('WorkEndDate1').disabled = true;
        document.getElementById('WorkPos1').disabled = true;

        document.getElementById('Work2').disabled = true;
        document.getElementById('WorkAdd2').disabled = true;
        document.getElementById('WorkDate2').disabled = true;
        document.getElementById('WorkEndDate2').disabled = true;
        document.getElementById('WorkPos2').disabled = true;

        document.getElementById('Work3').disabled = true;
        document.getElementById('WorkAdd3').disabled = true;
        document.getElementById('WorkDate3').disabled = true;
        document.getElementById('WorkEndDate3').disabled = true;
        document.getElementById('WorkPos3').disabled = true;
        document.getElementById('WorkReason1').disabled = true;
        document.getElementById('WorkReason2').disabled = true;
        document.getElementById('WorkReason3').disabled = true;

        document.getElementById('Work11').checked = true;
        document.getElementById('Work21').checked = true;
        document.getElementById('Work31').checked = true;
        document.getElementById('Work41').checked = true;
        document.getElementById('Work51').checked = true;
        document.getElementById('Work61').checked = true;

        document.getElementById('Work71').checked = true;
        document.getElementById('Work81').checked = true;
        document.getElementById('Work91').checked = true;
        document.getElementById('Work11').checked = true;
        document.getElementById('Work12').checked = true;
        document.getElementById('Work13').checked = true;


        document.getElementById('W1').checked = true;
        document.getElementById('W2').checked = true;
        document.getElementById('W3').checked = true;
        document.getElementById('W4').checked = true;
        document.getElementById('W5').checked = true;
        document.getElementById('W6').checked = true;


    }
    else
    {
    
        document.getElementById('Work1').disabled = false;
        document.getElementById('WorkAdd1').disabled = false;
        document.getElementById('WorkDate1').disabled = false;
        document.getElementById('WorkEndDate1').disabled = false;
        document.getElementById('WorkPos1').disabled = false;

        document.getElementById('Work2').disabled = false;
        document.getElementById('WorkAdd2').disabled = false;
        document.getElementById('WorkDate2').disabled = false;
        document.getElementById('WorkEndDate2').disabled = false;
        document.getElementById('WorkPos2').disabled = false;

        document.getElementById('Work3').disabled = false;
        document.getElementById('WorkAdd3').disabled = false;
        document.getElementById('WorkDate3').disabled = false;
        document.getElementById('WorkEndDate3').disabled = false;
        document.getElementById('WorkPos3').disabled = false;

        document.getElementById('WorkReason3').disabled = false;
        document.getElementById('WorkReason2').disabled = false;
        document.getElementById('WorkReason1').disabled = false;

        document.getElementById('Work11').checked = false;
        document.getElementById('Work21').checked = false;
        document.getElementById('Work31').checked = false;
        document.getElementById('Work41').checked = false;
        document.getElementById('Work51').checked = false;
        document.getElementById('Work61').checked = false;
        document.getElementById('Work71').checked = false;
        document.getElementById('Work81').checked = false;
        document.getElementById('Work91').checked = false;
        document.getElementById('Work11').checked = false;
        document.getElementById('Work12').checked = false;
        document.getElementById('Work13').checked = false;

        document.getElementById('W1').checked = false;
        document.getElementById('W2').checked = false;
        document.getElementById('W3').checked = false;
        document.getElementById('W4').checked = false;
        document.getElementById('W5').checked = false;
        document.getElementById('W6').checked = false;
    }

    
    
}

function not4()
{
    if(document.getElementById('NotAttained4').checked)
    {


        document.getElementById('Work2').disabled = true;
        document.getElementById('WorkAdd2').disabled = true;
        document.getElementById('WorkDate2').disabled = true;
        document.getElementById('WorkEndDate2').disabled = true;
        document.getElementById('WorkPos2').disabled = true;
        document.getElementById('WorkReason2').disabled = true;

        document.getElementById('Work11').checked = true;
        document.getElementById('Work21').checked = true;
        document.getElementById('Work31').checked = true;
        document.getElementById('Work41').checked = true;
        document.getElementById('Work51').checked = true;
        document.getElementById('Work61').checked = true;


    }
    else
    {
    

        document.getElementById('Work2').disabled = false;
        document.getElementById('WorkAdd2').disabled = false;
        document.getElementById('WorkDate2').disabled = false;
        document.getElementById('WorkEndDate2').disabled = false;
        document.getElementById('WorkPos2').disabled = false;
        document.getElementById('WorkReason2').disabled = false;

        document.getElementById('Work11').checked = false;
        document.getElementById('Work21').checked = false;
        document.getElementById('Work31').checked = false;
        document.getElementById('Work41').checked = false;
        document.getElementById('Work51').checked = false;
        document.getElementById('Work61').checked = false;

    }
    
    
}

function not5()
{
    if(document.getElementById('NotAttained5').checked)
    {
    
        document.getElementById('Work3').disabled = true;
        document.getElementById('WorkAdd3').disabled = true;
        document.getElementById('WorkDate3').disabled = true;
        document.getElementById('WorkEndDate3').disabled = true;
        document.getElementById('WorkPos3').disabled = true;
        document.getElementById('WorkReason3').disabled = true;

        document.getElementById('Work71').checked = true;
        document.getElementById('Work81').checked = true;
        document.getElementById('Work91').checked = true;
        document.getElementById('Work11').checked = true;
        document.getElementById('Work12').checked = true;
        document.getElementById('Work13').checked = true;

    }
    else
    {
    
        document.getElementById('Work3').disabled = false;
        document.getElementById('WorkAdd3').disabled = false;
        document.getElementById('WorkDate3').disabled = false;
        document.getElementById('WorkEndDate3').disabled = false;
        document.getElementById('WorkPos3').disabled = false;
        document.getElementById('WorkReason3').disabled = false;

        document.getElementById('Work71').checked = false;
        document.getElementById('Work81').checked = false;
        document.getElementById('Work91').checked = false;
        document.getElementById('Work11').checked = false;
        document.getElementById('Work12').checked = false;
        document.getElementById('Work13').checked = false;

    }

    
    
}

function getPos(id)
{

            if (window.XMLHttpRequest) {
             // code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}
                                 
            var paramsss = "Company=" + id; 
            xmlhttp.open("POST","getPos.php?" + paramsss, true);
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send(paramsss);
        

          xmlhttp.onreadystatechange=function() {
             if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    document.getElementById('rsp_pos').innerHTML=xmlhttp.responseText;
                }
                }

}
</script>
</head>

<body>

</br>
</br>



                <div class="panel panel-primary">
                        <div class="panel-heading">
                        <b>Applicant Form</b>
                        </div>
                        <div class="panel-body">

<form method = "post" enctype="multipart/form-data" name="Form" action='Applicant.php' onSubmit="return checkForm(this);">
   
<div class="panel-body">
                            
    <div class="row">
                        
    <div class="col-lg-6">
            <div class="form-group">
        <table width="100%">

        <tr>
        <td width="20"><div align="left"><label>Picture:</label></div></td>
        <td width="205" height="91"><center><img name="" id="PictureMismo" src="../Uploads/Pictures\UnknownPerson.jpg" width="120" height="120" alt="">
            <br>
            <br>
            <input class="form-control" type="file" name="picture" id="picture" onChange="readURL(this)">
            <div id="rsp_Image"> 
            <br>
        </td>
        </tr>
            </table>

            </div>
            </div>


<div class="col-lg-6">
     <div class="form-group">
        <table width="100%">
        <tr>
        <br>
        <br>
        <br>
        <br>
        <td width="20%"> <label> Name: </label> </td> 
        <td width="218%"> <div align="center"><input class="form-control" type="text" id="Name" placeholder="Full Name" size="40"  onkeypress="return isLetterKey(event)" required></div></td>
    </tr>
    </table>
    </div>
 </div>
 </div>


<div class="row">
 <div class="col-lg-6">
     <div class="form-group">
        <table width="100%">
        <tr>
        <td width="20%"> <label> Company: </label> </td> 
        <td width="80%"> <div align="center">
    <select class="form-control" name="Company" size="1" id="Company" value="" onChange="getPos(this.options[this.selectedIndex].value)">
    <?php
    $as = mysqli_query($con,"SELECT Company FROM positionstb GROUP BY Company");    
    echo '  <option value="None">----------------------------</option>';
    while ($ass = mysqli_fetch_array($as))
    {
          echo "<option value='".$ass['Company']."'>".$ass['Company']."</option>";
    } 

    ?>
    </select>
    </div></td>

    </tr>
    </table>
    </div>
 </div>


<div class="col-lg-6">
     <div class="form-group">
        <table width="100%">
        <tr> 
        <td width="20%"> <label> Position: </label> </td> 
        <td width="80%"> <div align="center">
        <div id = 'rsp_pos'>
        </div>
    </div>

        </div>
        </td>

    </tr>
    </table>
    </div>
 </div>
 </div>
</div>
 </div>
 
   <div align="center"><h2><b>Personal Information</b></h2></div> 
   
<div class="row">
<div class="col-lg-12">
     <div class="form-group">
        <table width="98%">
        <tr>
        <td>       
        <table border='0' width="100%">
        <tr>
        <td width="15%"> <label>&nbsp;&nbsp;Address: </label><br></td>
        <td><input class="form-control" name="Add" placeholder="Address" type="text" id="Add" size="30" required><br></td>
    </tr>
    </table>
    </div>
 </div>

 </div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label>&nbsp;&nbsp;Date of Birth: </label> </td> 
            <td width="70%"> <div align="center">
                <input class="form-control" type="date" name="textfield" required = "required" id="Dob" onChange="Validate('checkDate', this.value, 'rsp_' + this.id, '')">
                </div></td>
        </tr>
        </table>
        </div>
        </div>

    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label>Cellphone #: </label> </td> 
        <td width="11%"><input class="form-control" placeholder="+63" type="text" readonly>
        <td width='1'></td>
        <td width="58%"><input class="form-control" name="Cnum" placeholder="Cellphone #" type="text" id="Cnum" onKeyPress="return isNumberKey(event)" maxlength="10" title="Cellphone number must be 10 digits only." size="30">
    </div></td>
        </tr>
        </table>
        </div>
        </div>
        </div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label>&nbsp;&nbsp;E-Mail Address: </label> </td> 
            <td width="70%"> <div align="center">
                <input class="form-control" name="Eadd" placeholder="E-Mail Address" type="text" id="Eadd" size="30" onkeypress="return isEmailKey(event)" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required></div></td>
        </tr>
        </table>
        </div>
        </div>

    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label> Marital Status: </label> </td> 
        <td width="70%"> <div align="center">
    <select class="form-control" name="Mstat" size="1" id="Mstat" value="">
    <option value="None">----------------------------</option>
    <option value="Married">Married</option>
    <option value="Single">Single</option>
    </select>
    </div></td>
        </tr>
        </table>
        </div>
        </div>
        </div>


<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label>&nbsp;&nbsp;SSS Number: </label> </td> 
            <td width="70%"> <div align="center">
            <input class="form-control" name="SSS" placeholder="SSS Number" type="text" id="SSS" size="30" onKeyPress="return isNumberKey(event)" pattern=[0-9]{11,11} maxlength="11" required></div></td>
        </tr>
        </table>
        </div>
        </div>

    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label> TIN Number: </label> </td> 
        <td width="70%"> <div align="center">
    <input class="form-control" name="Tin" placeholder="TIN Number" type="text" id="Tin" size="30" onKeyPress="return isNumberKey(event)" pattern=[0-9]{11,11} maxlength="11" required>
    </div></td>
        </tr>
        </table>
        </div>
        </div>
        </div>


<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label>&nbsp;&nbsp;PhilHealth Number: </label> </td> 
            <td width="70%"> <div align="center">
            <input class="form-control" name="Phealth" placeholder="PhilHealth Number" type="text" id="Phealth" size="30" onKeyPress="return isNumberKey(event)" pattern=[0-9]{11,11} maxlength="11" required>
    </div></td>
        </table>
        </div>
        </div>

    <div class="col-lg-6">
        <div class="form-group">
        <table width="100%">
        <tr>
        <td width="30%"> <label> Pag-Ibig Number: </label> </td> 
        <td width="70%"> <div align="center">
    <input class="form-control" name="Pagibig" placeholder="Pag-Ibig Number" type="text" id="Pagibig" size="30" onKeyPress="return isNumberKey(event)" pattern=[0-9]{11,11} maxlength="11" required>
    </div> </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        </div>
        </div>
        </div>
 
<table width='100%' border='0'>
    <tr>
        <td colspan='5'>
            <div align="center"><h2><b>Academic Information</b></h2></div>
        </td>
    </tr>
    <tr>
        <td width='40%' align='center'>
        <b><u>Name of University, College(s), Further Education</u></b>
        </td>
        
        <td width='1%'>
        </td>
        
        <td width='28%' align='center'>
        <b><u>Date Graduated</u></b>
        </td>

        <td width='1%'>
        </td>
        
        <td width='30%' align='left'>
        <b><u>Attained Qualifications(Course, Honors, Etc.)</u></b>
        </td>
    </tr>
</table>
<br>
 

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
        <table width="98%">
        <tr>

            <td width="1%">
            </td> 
        
            <td width="11%">
                <div align="center">
                    <label> College: </label>
                </div>
            </td>
             <td width="1%">
            </td> 

            <td width="27%">
                <div align="center">
                <input class="form-control" onKeyPress="return isLetterKey(event)" name="Col" type="text" id="Col" size="30" required>
                </div>
            </td>    

            <td width="1%">
            </td> 

            <td width="27%">   
                <input class="form-control" type="date" name="ColDate" required = "required" id="ColDate" onChange="Validate('checkDateGrad', this.value, 'rsp_' + this.id, '')">
                <div id="rsp_ColDate"></div>
            </td>
        
            <td width="1%">
            </td> 
        
            <td width="27%">
                <div align="right">
                    <textarea class="form-control" name="ColDetails" cols="20" rows="3" id="ColDetails" required></textarea>
                    <div id="rsp_ColDetails"></div>
                </div>
            </td>

            <td width="1%">
            </td> 
        
        </tr>
        </table>

        </div>
        </div>
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
        <table width="98%">
        <tr>

            <td width="1%">
            </td> 
        
            <td width="11%">
                <div align="center">
                    <label> Graduate School (Masters):</label> <br><input type="checkbox" id="NotAttained" onClick="not()">
    Not Attained</div></td>
                </div>
            </td>

             <td width="1%">
            </td> 
            
            <td width="27%">
                <div align="center">
                    <input class="form-control" onKeyPress="return isLetterKey(event)" name="Grad" type="text" id="Grad" size="30" required>
                <div id="rsp_Grad">
                    <input type="checkbox" class="CheckValid" id="Grad1" disabled = "True">
                </div>
                </div>
            </td>    

            <td width="1%">
            </td> 

            <td width="27%">  
            <div align="center"> 
               <input class="form-control" type="date" name="Dob4" required = "required" id="GradDate" onChange="Validate('checkDateGrad', this.value, 'rsp_' + this.id, '')">
                <div align="center" id="rsp_GradDate"><input type="checkbox" class="CheckValid" id="Grad2" disabled = "True"></div>
            </div>
            </td>

        
            <td width="1%">
            </td> 
        
            <td width="27%">
                <div align="center">
                <textarea class="form-control" name="GradDetails" cols="40" rows="3" id="GradDetails" required></textarea>
                <div id="rsp_GradDetails">
                <input type="checkbox" class="CheckValid" id="Grad3" disabled = "True"></div>
                

                </div>
                </div>
            </td>

            <td width="1%">
            </td> 
        
        </tr>
        </table>

        </div>
        </div>
</div>




<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
        <table width="98%">
        <tr>

            <td width="1%">
            </td> 
        
            <td width="11%">
                <div align="center">
                    <label> Graduate School (Further Education):</label> <br><input type="checkbox" id="NotAttained2" onClick="not2()">
    Not Attained</div></td>
                </div>
            </td>

             <td width="1%">
            </td> 
            
            <td width="27%">
                <div align="center">
                    <input class="form-control" onKeyPress="return isLetterKey(event)" name="Grad" type="text" id="FGrad" size="30" required>
                    <div id="rsp_Grad">
                    <input type="checkbox" class="CheckValid" id="Grad11" disabled = "True"></div>
                    </div></td>

                </div>
                </div>
            </td>    

            <td width="1%">
            </td> 

            <td width="27%">  
            <div align="center"> 
               <input class="form-control" type="date" name="Dob4" required = "required" id="FGradDate" onChange="Validate('checkDateGrad', this.value, 'rsp_' + this.id, '')">
                <div align="center" id="rsp_GradDate"><input type="checkbox" class="CheckValid" id="Grad22" disabled = "True"></div>
            
            </td>

        
            <td width="1%">
            </td> 
        
            <td width="27%">
                <div align="center">
                <textarea class="form-control" name="FGradDetails" cols="40" rows="3" id="FGradDetails" required></textarea>
                <div id="rsp_GradDetails">
                <input type="checkbox" class="CheckValid" id="Grad33" disabled = "True"></div>
    

                </div>
                

                </div>
                </div>
            </td>

            <td width="1%">
            </td> 
        
        </tr>
        </table>

        </div>
        </div>
</div>



    <tr><th colspan="4"></th></tr>
    <tr><th colspan="4"></th></tr>
    <tr><th colspan="4"><div align="center"><h2><b>Employment Record</b></h2></div></th> </tr>
    <tr><th colspan="4"></th></tr>
    <tr><th colspan="4"></th></tr>   

        <center>
        <table width="98%"> 
        
    <thead>
    <tr>
    <th width = "9"> <div align="center">Options</div> </th>
    <th width = "1%"></th>   
    <th width = "14"><div align="center">Employer</div></th>
    <th width = "1%"></th>   
    <th width = "14"><div align="center">Address</div></th>
    <th width = "1%"></th>   
    <th width = "14"><div align="center">Start Date</div></th>
    <th width = "1%"></th>   
    <th width = "14"><div align="center">End Date</div></th>
    <th width = "1%"></th>   
    <th width = "14"><div align="center">Position</div></th>
    <th width = "1%"></th>   
    <th width = "14"><div align="center">Reason of Resignation</div></th>
    <th width = "2%"></th>   
    </tr>
    </thead>

    <tbody>
    <tr>
    <td width="9%"><center><font size="2">Unavailable?</font><br><input type="checkbox" id="NotAttained3" onClick="not3()"></td>

    <td width = "1%"></td>
    <td width = "14%"><center><input type="checkbox" class="CheckValid" id="W1" disabled = "True"><input class='form-control' onKeyPress="return isLetterKey(event)" placeholder='Employer' name='Work1' class='Work' type='text' id='Work1' size='24' required></td> 

    <td width = "1%"></td>
    <td width = "14%"><center><input type="checkbox" class="CheckValid" id="W2" disabled = "True"><input class='form-control' placeholder='Address' name='WorkAdd1' class='Work' type='text' id='WorkAdd1' size='24' required></td>

    <td width = "1%"></td>
    <td width = "14%"><center><input type="checkbox" class="CheckValid" id="W3" disabled = "True"><input class='form-control' type='date' class='Work' id='WorkDate1' required></td>
    <td width = "1%"></td>

    <td width = "14%"><center><input type="checkbox" class="CheckValid" id="W4" disabled = "True"><input class='form-control' type='date' class='Work' id='WorkEndDate1' required></td>
    <td width = "1%"></td>

    <td width = "14%"><center><input type="checkbox" class="CheckValid" id="W5" disabled = "True"><input class='form-control' onKeyPress="return isLetterKey(event)" placeholder='Position' name='WorkPos1' class='Work' type='text' id='WorkPos1' size='24' required></td>
    <td width = "1%"></td>

    <td width = "14%"><center><input type="checkbox" class="CheckValid" id="W6" disabled = "True"><textarea class='form-control' cols='23' placeholder='Reason of resignation' class='Work' rows='3' id='WorkReason1' required></textarea></td>

    <td width = "2%"></td>
    </tr>

    <tr>
    <td><center><font size="2">Unavailable?</font><br><input type="checkbox" id="NotAttained4" onClick="not4()"></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work11" disabled = "True"><input class='form-control' onKeyPress="return isLetterKey(event)" placeholder='Employer' name='Work2' class='Work' type='text' id='Work2' size='24' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work21" disabled = "True"><input class='form-control' placeholder='Address' name='WorkAdd2' class='Work' type='text' id='WorkAdd2' size='24' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work31" disabled = "True"><input class='form-control' type='date' class='Work' id='WorkDate2' required></td>
    <td width = "1%"></td>

    <td><center><input type="checkbox" class="CheckValid" id="Work41" disabled = "True"><input class='form-control' type='date' class='Work' id='WorkEndDate2' required></td>
    <td width = "1%"></td>

    <td><center><input type="checkbox" class="CheckValid" id="Work51" disabled = "True"><input class='form-control' onKeyPress="return isLetterKey(event)" placeholder='Position' name='WorkPos2' class='Work' type='text' id='WorkPos2' size='24' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work61" disabled = "True"><textarea class='form-control' cols='23' placeholder='Reason of resignation' class='Work' rows='3' id='WorkReason2' required></textarea></td>

    <td width = "2%"></td>
    </tr>
        <tr>
    <td><center><font size="2">Unavailable?</font><br><input type="checkbox" id="NotAttained5"onClick="not5()"></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work71" disabled = "True"><input class='form-control' onKeyPress="return isLetterKey(event)" placeholder='Employer' name='Work3' class='Work' type='text' id='Work3' size='24' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work81" disabled = "True"><input class='form-control' placeholder='Address' name='WorkAdd3' class='Work' type='text' id='WorkAdd3' size='24' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work91" disabled = "True"><input class='form-control' type='date' class='Work' id='WorkDate3' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work11" disabled = "True"><input class='form-control' type='date' class='Work' id='WorkEndDate3' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work12" disabled = "True"><input class='form-control' onKeyPress="return isLetterKey(event)" placeholder='Position' name='WorkPos3' class='Work' type='text' id='WorkPos3' size='24' required></td>

    <td width = "1%"></td>
    <td><center><input type="checkbox" class="CheckValid" id="Work13" disabled = "True"><textarea class='form-control' cols='23' placeholder='Reason of resignation' class='Work' rows='3' id='WorkReason3' required></textarea></td>

    <td width = "2%"></td>
    </tr>
     </tbody>          
    </table>  
    <div id="rsp_Work"></div>     
    </center>

</td>
</tr>
</table>

<br>
<div class="row" align='center'>

<?php
echo "<table width='95%' border='0'>";
echo "<tr><td><p align='left'><a href='Monitoring.php' role='button' type='button' class='btn btn-default btn-outline'>Back</a></p></td>";
echo "<td><p align='right'>
<input type='Submit' name='submit' value='Submit' class='btn btn-primary btn-outline'/>
<input type='reset' value='Reset' class='btn btn-danger btn-outline'/></p></td></tr></table>";

?> 
</div>
</form>
</center>

  


                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>
