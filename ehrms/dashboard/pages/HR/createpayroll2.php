<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ./Accountant.php");
}
  
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR</a>
            </div>
            <!-- /.navbar-header -->

           <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
                       <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by Status desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by Status desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

            <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->

        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Payroll</h1>                   
                    </div>
                    <!-- /.col-lg-12 -->
                <div class='col-lg-12'>
                    <div class='panel panel-primary'>
                        <div class='panel-heading'>
                            Payroll
                        </div>
                        <!-- /.panel-heading -->
                        <div class='panel-body'>



<?php
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())   {   echo "Failed to connect to MySQL: " . mysqli_connect_error();   } 

$id = $_GET['PId'];
echo "<form action='createpayroll.php?PId=". $id ."' method='post'>";
$rr = mysqli_query($con,"SELECT * FROM payrolltb where PId = '$id'");
while($row=mysqli_fetch_array($rr)){
    $EmpId = $row['EmpId'];
    $frum = $row['PStart'];
    $too = $row['PEnd'];
}

$qry = "SELECT *,m.Name as Na,m.Company as C FROM masteremptb ma 
inner join masterpersonaltb m on ma.EmpId = m.EmpId
inner join masteruploadtb u on u.EmpId = m.EmpId
inner join positionstb pa on pa.PosId = m.PosId Where m.EmpId = '$EmpId'";


$resultd = mysqli_query($con,$qry) or die('Error: '. mysqli_error($con));
 while($rows=mysqli_fetch_array($resultd))
 {
    $basicpay = $rows['BasicPay'];
    $dep = $rows['Dependency'];
    $mstat = $rows['Mstatus'];  
        echo "<table class='table'><center><thead>";
         echo "<tr rowspan='2'>";

                    echo "<tr rowspan='2'>";
                    echo "<td width='178' rowspan='7' align='center'>
                    <img src='". $rows['Location'] . "' height='100' width='120'></img></td>";

                    echo "<td align='left'><b>Employee Number: </td>";
                    echo "<td>" . $rows['EmpId'] . "</td>";

                    echo "<td align='left'><b>Employee Name: </b> </td>";
                    echo "<td>" . $rows['Na'] . "</td></tr>";

                    echo "<tr><td align='left'><b>Designation:</b> </td>";
                    echo "<td>" . $rows['Description'] . "</td>
                    <td align='left'><b>Company:</b> </td>";
                    echo "<td>" . $rows['C'] . "</td>

                    </tr>";
                    echo "<tr><th colspan='4'><center> Payroll Period: " . date("M j, Y", strtotime($frum)) . " - " . date("M j, Y", strtotime($too))  . " </th></tr>";
                    echo "</table><thead>";
                //end
}

$hh = mysqli_query($con,"SELECT * FROM payrolldatatb WHERE EmpId = '$EmpId' AND PStart >= '$frum' AND PEnd <= '$too'") or die("Error " . mysqli_error($con));
while($how = mysqli_fetch_array($hh))
{
                    echo "<table class='table' border='0' width='100%'><center><thead>";
                    echo "<tr><td colspan='4' width='100%'><b>Basic Compensation</b></td></tr>";
                    echo "<tr align= 'left'>";
                    echo "<td width='15%'>Gross Income:</td>";
                    echo "<td><input type = 'text' readonly name='hrsworkpay' class='form-control' id='hrsworkpay' value='". number_format($how['Gross'], 2, '.',',') ."'></td>";

                    echo "<td>Gross OT Pay:</td>";
                    echo "<td><input type = 'text' readonly  class='form-control' name='hrsotpay' id='hrsotpay' value='". number_format($how['OT'], 2, '.',',') ."'></td></tr>";

                    echo "</tr>";

                    echo "<table class='table' width='100%' border='0'><center><thead>";
                    
                    echo "<tr><td colspan='2' width='100%'><b>Undertime Deduction</b></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td colspan='1'>Gross Undertime Deduction:</td>";
                    echo "<td><input type = 'text' readonly class='form-control' name='hrsut' id='hrsut' value='". number_format($how['UT'], 2, '.',',') ."'></td></tr>";

                    echo "</tr></table></thead>";
                    $tax = $how['tax'];
                    $regdeduc = $how['regdeduct'];
                    $TotalGrossPay = $how['TotalGross'];
                    $TotalDeductions = $how['TotalDeduct'];
                    $TotalNet = $how['Net'];
}



        //Other Income
         echo "<table class='table'><center><thead>";
         echo "<tr><th>Other Income</th></tr>";
         echo "
                <th>Income Id</th>
                <th>Income Name</th>
                <th>Income Amount</th>
                </thead><tbody>
         ";
        $inamount = 0;
        $in = mysqli_query($con,"SELECT * FROM incometb where EmpId = '$EmpId' AND IStart = '$frum' AND IEnd ='$too' AND Status = '0'");
        while($ii = mysqli_fetch_array($in)){  
             $inamount = $inamount + $ii['Amount'];
                    echo "<tr>";
                    echo "<td><input type = 'text' readonly name='oiid' id='oiid' class='form-control OtherIncome' value='".$ii['InId']."'></td>";
                    echo "<td><input type = 'text' readonly name='oiname' id='oiname' class='form-control OtherIName' value='" . $ii['IncomeName'] . "'></td>";
                    echo "<td><input type = 'text' readonly name='iamount' id='iamount' class='form-control OtherIAmount' value='". number_format($ii['Amount'], 2, '.',',') ."'></td></td><tr>";
       }
          echo "<tr><td><b>Total Amount </td><td> &nbsp;</td><td><input type = 'text' readonly name='TIamount' id='TIamount' class='form-control' value='". number_format($inamount, 2, '.',',') ."'></td></td><tr>";
          echo "</tr></tbody></table>";

        //Other Deductions
         echo "<table class='table'><center><thead>";
         echo "<tr><th>Other Deductions</th></tr>";
         echo "
                <th>Deduction Id</th>
                <th>Deduction Name</th>
                <th>Deduction Amount</th>
                </thead><tbody>
         ";
        $odamount = 0;
        $od = mysqli_query($con,"SELECT * FROM deductionstb where EmpId = '$EmpId'  AND Start = '$frum' AND End ='$too' AND Status = '0'");
        while($dd = mysqli_fetch_array($od)){  
             $odamount = $odamount + $dd['DeducAmount'];
                    echo "<tr><td><input type = 'text' readonly name='oiid' id='oiid' class='form-control OtherDeductions' value='".$dd['DeductionId']."'></td>";
                    echo "<td><input type = 'text' readonly name='oiname' id='oiname' class='form-control OtherDName' value='" . $dd['DeducName'] . "'></td>";
                    echo "<td><input type = 'text' readonly name='iamount' id='iamount' class='form-control OtherDAmount' value='". number_format($dd['DeducAmount'], 2, '.',',') ."'></td></td><tr>";
       }
          echo "<tr><td><b>Total Amount </td><td> &nbsp;</td><td><input type = 'text' readonly name='ODamount' id='ODamount' class='form-control' value='". number_format($odamount, 2, '.',',') ."'></td></td><tr>";
          echo "</tr></tbody></table>";
                
          //Regular Deductions
            $totalregdeduc = 0;
            echo "<table class='table' border='0' width='100%' bordercolor='red'><thead>";
            echo "<tr><td colspan='2'><b>Regular Deductions</b></td></tr>";
            $reg = mysqli_query($con,"SELECT * FROM ssstb Where '$basicpay' >= MinRange AND '$basicpay' <= MaxRange");
            while($sss = mysqli_fetch_array($reg)){
            $totalregdeduc = $totalregdeduc + ($sss['EE'] / 2);
            echo "<tr>";
            echo "<td width='25%' align='left'>SSS: </td>";
            echo "<td><input type = 'text' class='form-control' readonly name='sssee' id='sssee' value='" . ($sss['EE'] / 2) . "'></td></tr>";
            }
           

            $reg2 = mysqli_query($con,"SELECT * FROM pagibigtb Where '$basicpay' >= RangeMin AND '$basicpay' <= RangeMax");
            while($pagibig = mysqli_fetch_array($reg2)){
            $pg = 50;    
            $totalregdeduc = $totalregdeduc + $pg;
            echo "<tr rowspan='2'>";
            echo "<td align='left'>Pagibig: </td>";
            echo "<td><input type = 'text' class='form-control' readonly name='pagibig' id='pagibig' value='" . $pg . "'></td></tr>";
            }

            $reg3 = mysqli_query($con,"SELECT * FROM philhealthtb Where '$basicpay' >= RangeMin AND '$basicpay' <= RangeMax");
            while($phil = mysqli_fetch_array($reg3)){
            $totalregdeduc = $totalregdeduc + ($phil['EE'] / 2);
            echo "<tr rowspan='2'>";
            echo "<td align='left'>Philhealth: </td>";
            echo "<td><input type = 'text' class='form-control' readonly name='phil' id='phil' value='" . ($phil['EE'] / 2) . "'></td></tr>";
            }

            // $salary = $basicpay - $totalregdeduc;

            // $reg4 = mysqli_query($con,"SELECT * FROM taxtb where '$salary' >= taxablemin and '$salary' <= taxablemax and status = '$mstat' and dependencies='$dep'");
            // while($tax = mysqli_fetch_array($reg4)){
            // if($basicpay <= 11000){
            //     $taxam = 0;
            // }
            // else
            // {
            //     $taxam = (($salary - $tax['exemp']) * $tax['perce']) + $tax['basetax'];
            // }
            // 
            // }
            // $totalregdeduc = $totalregdeduc + $taxam;

            echo "<tr rowspan='2'>";
            echo "<td align='left'>Tax: </td>";

            echo "<td><input type = 'number' class='form-control' readonly value='$tax' name='tax' id='tax'></td></tr>";
            
            echo "<tr rowspan='2'>";
            echo "<td align='left'><b>Total Regular Deduction:  </td>";
            echo "<td><input type = 'text' name='grossdeduc' id='grossdeduc' readonly class='form-control' value='" .  number_format($totalregdeduc, 2, '.',',') . "'></td><tr>";

         echo "</table></thead>";
                //end

                echo "<table class='table' width='100%' border='0'><center><thead>";
                    
                    echo "<tr><th>Net and Gross</th></tr>";
                    
                    echo "<tr align= 'left'>";
                    echo "<td width='25%'>Total Gross Pay:</td>";
                    echo "<td><input type = 'text' class='form-control' readonly name='grosspay' value='" .  number_format($TotalGrossPay, 2, '.',',') . "'></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td>Total Deductions:</td>";
                    echo "<td><input type = 'text' class='form-control' readonly name='totaldeduct' id='totaldeduct' value='" .  number_format($TotalDeductions, 2, '.',',') . "'></td></tr>";

                    echo "<tr align= 'left'>";
                    echo "<td><b>Total Net Pay:</td>";
                    echo "<td><input type = 'text' class='form-control' readonly name='netpay' id='netpay' value='" .  number_format($TotalNet, 2, '.',',') . "'></td></tr>";


    ?>              
                    </table></form>

                        </div>
                    </div>
                </div>
            </div>
        <!-- /.container-fluid -->
        </div>
    <!-- /#page-wrapper -->
    </div>
<!-- /#wrapper -->

</body>
</html>