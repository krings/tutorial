<?php
  session_start();
  $connection = mysql_connect('localhost', 'root');
  // Selecting Database
  $con = mysqli_connect("localhost","root","","ehrms");
  $db = mysql_select_db("ContentManagement", $connection);
  $error = "";

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
 ?>


<!DOCTYPE html>
<html lang="en">

<head>

     <style>

#MP1{
    resize: none;
    }
#MP2{
    resize: none;
    }
#MP3{
    resize: none;
    }
#MP4{
    resize: none;
    }
#MA {
    resize: none;
    }
#TA{
    resize: none;
    }
#MP5{
    resize: none;
    }

</style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--MODAL-->
    <script src="lib/js/jquery-1.11.2.min.js"></script>
    <script src="lib/js/bootstrap.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      $("#myModal").on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);  // Button that triggered the modal
            var titleData = button.data('title'); // Extract value from data-* attributes
            $(this).find('.modal-title').text(titleData + ' Form');
        });
    });
    </script>

</head>

<body>

    <div id="wrapper">

       <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./../HR.php">eBiZolution | HR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b>User</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./../logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
          

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./../ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                    echo "<li> <a href='./../ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./../ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


            <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="./../HR.php"><i class="fa fa-home"></i> Back to Main Menu</a>
                        </li>
                        <li>
                            <a href="./OrganizationDescription.php"> Organization Description</a>
                        </li>
                        <li>
                            <a href="./ServiceProvided.php"> Service Provided</a>
                        </li>
                        <li>
                            <a href="./CompanyPhilosophy.php"> Company Philosophy</a>
                        </li>
                        <li>
                            <a href="./NatureOfEmployment.php"> Nature of Employment</a>
                        </li>
                        <li>
                            <a href="./Recruitment.php"> Recruitment</a>
                        </li>
                        <li>
                            <a href="./Attendance.php"> Attendance</a>
                        </li>
                        <li>
                            <a href="./RecordingAndReporting.php"> Recording & Reporting</a>
                        </li>
                        <li>
                            <a href="./Holidays.php"> Holidays</a>
                        </li>
                        <li>
                            <a href="./Overtime.php"> Overtime</a>
                        </li>
                        <li>
                            <a href="./LeavingForOfficialBusiness.php"> Leaving for Official Business</a>
                        </li>
                        <li>
                            <a href="./OfficialLeaves.php"> Offical Leaves & AWOL</a>
                        </li>
                        <li>
                            <a href="./AbsenceWithPay.php"> Absence with Pay</a>
                        </li>
                        <li>
                            <a href="./RulesOnNotification.php"> Rules on Notification</a>
                        </li>
                        <li>
                            <a href="./PersonnelFiles.php"> Personnel Files</a>
                        </li>
                        <li>
                            <a href="./SalaryAdministration.php"> Salary Administration</a>
                        </li>
                        <li>
                            <a href="./EmployeeBenefits.php"> Employee Benefits</a>
                        </li>
                        <li>
                            <a href="./WorkingCondition.php"> Working Condition</a>
                        </li>
                        <li>
                            <a href="./Sources.php"> Sources</a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Employee Benefits</h1>
                    </div>
                        
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                            
                            <?php

                      $result = mysql_query("SELECT * FROM EmployeeBenefits",$connection);
                                                  
                      while($row = mysql_fetch_array($result)){ 

                          echo $row['MP1'] . "<br><br>";
                          echo $row['MP2'] . "<br><br>";
                          echo $row['MP3'] . "<br><br>";
                          echo $row['MP4'] . "<br><br>";
                          echo $row['MA'] . "<br><br>";
                          echo $row['TA'] . "<br><br>";
                          echo $row['MP5'] . "<br><br>";

  }

?>
                        </div>

                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <button type="button" class="btn btn-outline btn-primary" data-toggle="modal" data-target="#myModal" data-title="Edit">Edit Content</button><br><br>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!--MODAL-->
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Window</h4>
                </div>
                    <div class="modal-body">

                        <?php
                       $result = mysql_query("SELECT * FROM EmployeeBenefits",$connection);
                        while($row = mysql_fetch_array($result))
                          {

                            echo " <form name='input' method='post'> ";

                              echo "<b>Equal Emploment Opportunity</b><br><br>";
                                echo "<textarea cols='78' rows='4' name='MP1' id='MP1'class='form-control' required>" . $row['MP1'] . "</textarea><br><br>";
                                echo "<textarea cols='78' rows='3' name='MP2' id='MP2'class='form-control' required>" . $row['MP2'] . "</textarea><br><br>";
                                echo "<textarea cols='78' rows='6' name='MP3' id='MP3'class='form-control' required>" . $row['MP3'] . "</textarea><br><br>";
                                echo "<textarea cols='78' rows='4' name='MP4' id='MP4'class='form-control' required>" . $row['MP4'] . "</textarea><br><br>";
                                echo "<textarea cols='78' rows='3' name='MA' id='MA'class='form-control' required>" . $row['MA'] . "</textarea><br><br>";
                                echo "<textarea cols='78' rows='3' name='TA' id='TA'class='form-control' required>" . $row['TA'] . "</textarea><br><br>";
                                echo "<textarea cols='78' rows='5' name='MP5' id='MP5'class='form-control' required>" . $row['MP5'] . "</textarea><br><br>";
                                
                              echo "<input type='Hidden' name='ID' id='ID' class='form-control' value=". $row['ID'] . "><br>"; 

                            echo " <div class = 'modal-footer'>
                                   <input type='submit' value='Submit' class='btn btn-outline btn-primary' name='submit'/>
                                   <input type='reset' value='Reset' class='btn btn-outline btn-danger' name='reset'/>
                                   </div></form>";

    }
?>


                    </div>
                </div>
       
</body>
</html>

<?php

if(isset($_POST['submit']))
    {
        $ID = $_POST['ID'];
        $MP1 = $_POST['MP1'];
        $MP2 = $_POST['MP2'];
        $MP3 = $_POST['MP3'];
        $MP4 = $_POST['MP4'];
        $MA = $_POST['MA'];
        $TA = $_POST['TA'];
        $MP5 = $_POST['MP5'];
        
        mysql_query("UPDATE EmployeeBenefits SET MP1='$MP1', MP2='$MP2', MP3='$MP3', MP4='$MP4', MA='$MA',
                                                  TA='$TA', MP5='$MP5' WHERE ID='$ID'", $connection);


   echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location.href='EmployeeBenefits.php';
        </SCRIPT>");
      
      
    }

    ?>