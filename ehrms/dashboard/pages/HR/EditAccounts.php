<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
           

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>

             <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </nav>

        <!-- Page Content -->

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS |  Edit Accounts </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="panel panel-primary">
                        <div class="panel-heading">
                        <b> Edit Accounts </b>
                        </div>
                        <div class="panel-body">

<?php  
$host='localhost';
$user='root';
$pass="";

mysql_connect($host,$user,$pass) or die (mysql_error());
mysql_select_db('ehrms') or die (mysql_error());

$EmpId = $_GET['EmpId'];
//echo $SkillsID . "<br>";

if(isset($_POST['submit']))
    {
        $Password = $_POST['Password'];
        $cPassword = $_POST['cPassword'];
        $Level = $_POST['Level'];

        if ($Password != $cPassword)
        {
        echo "<script> alert('Your password not match!'); </script>";
        }

        else
        {

        mysqli_query($con, "UPDATE accounttb SET Password='$Password', Level='$Level' WHERE EmpId='$EmpId'");

        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('Edit complete.')
        window.location.href='Accounts.php';
        </SCRIPT>");
        }
    }

 $result = mysqli_query($con,"SELECT * FROM accounttb as T INNER JOIN masterpersonaltb as E WHERE T.EmpId = '$EmpId' AND E.EmpId = '$EmpId'") or die('Error: ' .  mysqli_error($con));

echo "<form action='' method='POST'>";
echo "<table border='0' width='100%'>";

while($row = mysqli_fetch_array($result))
{
echo "<tr>";
echo "<td width='15%'>Name:</td>";
echo "<td><input type='text' name='Name' id='Name' class='form-control' required readonly value='" . $row['Name'] . "'></td>";
echo "</tr>";
echo "<tr><td height='15'></td></tr>";
echo "<tr>";
echo "<td width='15%'>Username:</td>";
echo "<td><input type='text' name='Username' id='Username' class='form-control' required readonly value='" . $row['Username'] . "'></td>";
echo "</tr>";
echo "<tr><td height='15'></td></tr>";
echo "<tr>";
echo "<td width='15%'>Password:</td>";
echo "<td><input type='Password' name='Password' id='Password' class='form-control' value=''></td>";
echo "</tr>";
echo "<tr><td height='15'></td></tr>";
echo "<tr>";
echo "<td width='15%'>Confirm Password:</td>";
echo "<td><input type='Password' name='cPassword' id='cPassword' class='form-control' value=''></td>";
echo "</tr>";
echo "<tr><td height='15'></td></tr>";
echo "<tr>";
echo "<td width='15%'>User Level:</td>";

$lvl = $row['Level'];

}   

if ($lvl == "admin") {
    $lvl1 = "admin";
} elseif ($lvl == 1) {
    $lvl1 = "Employee";
} elseif ($lvl == 2) {
    $lvl1 = "Accountant";
} elseif ($lvl == 3) {
    $lvl1 = "Human Resource";
} elseif ($lvl == 4) {
    $lvl1 = "Supervisor";
} else {
    $lvl1 = "NA";
}

echo "<td><select class='form-control' name='Level' id='Level' required>
                        <option>" .  $lvl1 . "</option>
                        <option value='admin'>Admin</option>
                        <option value='1'>Employee</option>
                        <option value='2'>Accountant</option>
                        <option value='3'>Human Resource</option>
                        <option value='4'>Supervisor</option></td>";
echo "</tr>";

echo "<tr>";
echo "<td height='15'></td>";
echo "</tr>";
echo "<tr><td><p align='left'><a href='Accounts.php' role='button' type='button' class='btn btn-default btn-outline'>Back</a></p></td>";
echo "<td><p align='right'>
<input type='Submit' name='submit' value='Submit' class='btn btn-primary btn-outline'/>
<input type='reset' value='Reset' onClick='confirm('Are you sure?')' class='btn btn-danger btn-outline'/></p></td></tr>";
echo "</table>";

$result1 = mysqli_query($con,"SELECT * FROM accounttb as T INNER JOIN masterpersonaltb as E WHERE T.EmpId = '$EmpId' AND E.EmpId = '$EmpId'") or die('Error: ' .  mysqli_error($con));

while($row1 = mysqli_fetch_array($result1))
{
    if($row1['Status'] == 1)
    {
        echo "<button name='deact' id='button' class='btn btn-danger btn-outline'>Deactivate Account</button>";
    }
    else
    {
        echo "<button name='act' id='button' class='btn btn-primary btn-outline'>Activate Account</button>";
    }
}
echo "</form>";

?>
</div>
</div>
</div>
</div>


</body>
</html>

<?php

if(isset($_POST['deact']))

    {
        echo "<SCRIPT LANGUAGE='JAVASCRIPT'>
              if (window.confirm('Are you sure you want to deactivate this account? ') == false)
              {
                window.location.href='EditAccounts.php?EmpId=" . $EmpId . "';
              }
              else
              {
                window.location.href='terminate_user.php?EmpId=" . $EmpId . "';
              }
              
              </SCRIPT>";
    }


if(isset($_POST['act']))

    {
        echo "<SCRIPT LANGUAGE='JAVASCRIPT'>
              if (window.confirm('Are you sure you want to activate this account? ') == false)
              {
                window.location.href='EditAccounts.php?EmpId=" . $EmpId . "';
              }
              else
              {
                window.location.href='activate_user.php?EmpId=" . $EmpId . "';
              }
              
              </SCRIPT>";
    }

?>