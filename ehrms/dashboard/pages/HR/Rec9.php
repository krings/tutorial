<?php

session_start();
$con = mysqli_connect("localhost","root","","ehrms");

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//syntax for session checking for logout (different location for digital handbook, ../ means up one folder or back one folder)
if ($_SESSION["Uname"] == "" or $_SESSION["Name"] == "" or $_SESSION["Id"] == "")
{
 header("location: ../../../index.php");
}
  
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="./HR.php">eBiZolution | HR</a>
            </div>

            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>&nbsp;<b><?php echo $_SESSION['Name']; ?></b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         
                        
                        <!-- link to unset/destroy session. logout script -->
                        <li><a href="./logunset.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>
         

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php 
                            $me =  $_SESSION["Id"];
                 $re = mysqli_query($con,"SELECT COUNT(Id) as Num FROM dashboardtb where Reciever = '$me' AND Status='1' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                 $num = mysqli_fetch_array($re);
                 $ru = mysqli_query($con,"SELECT *,Status as S FROM dashboardtb where Reciever = '$me' Order by `Status` desc") or die('Error: ' .  mysqli_error($con));
                        ?>
                        <i class="fa fa-envelope fa-fw"></i>&nbsp;<b>Message (<?php echo $num['Num']; ?>)</b> &nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php
                        while($r = mysqli_fetch_array($ru)){

                            $sen = mysqli_query($con,"Select Name FROM masterpersonaltb Where EmpId = '" . $r['Sender'] . "' ");
                            $s = mysqli_fetch_array($sen);

                            if ($r['S'] == 1){

                                if($r['Type'] == 1){
                                    echo "<li> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li > <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }elseif ($r['S'] == 0){
                                if($r['Type'] == 1){
                                    echo "<li style ='background-color: 'white';'> <a href='./ViewMessage.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 2){
                                     echo "<li style ='background-color: 'white';'> <a href='./ViewMessage2.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . "</a></li>";
                                }
                                elseif ($r['Type'] == 3){
                                    echo "<li> <a href='./ViewMessage3.php?MsgId=". $r['Id'] ."'><i class='fa fa fa-circle-o fa-fw'></i> " . $r['Subject'] . " | " . $s['Name'] . " </a></li>";
                                }
                            }
                            
                            
                        }
                        
                        ?>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->

            </ul>


             <!-- NAVIGATION BARS -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                                           <li>
                            <a href="./PersonnelManagement.php"><i class="fa fa-male"></i> Employee Management</a>
                             </li>

                        <li>
                            <a href="#"><i class="fa fa-user"></i> My Profile<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./PersonalInformation.php">Personal Information</a>
                                </li>
                                 
                                <li>
                                    <a href="./SalaryDetails.php">Salary Details</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-clock-o"></i> Applications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Overtime.php">Overtime</a>
                                </li>
                                <li>
                                    <a href="./Leave.php">Leave</a>
                                </li>
                                <li>
                                    <a href="./Loan.php">Loan</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a href="./Positions.php"><i class="fa fa-users"></i> Positions </a>
                        </li>

                        <li>
                            <a href="./Recuitment.php"><i class="fa fa-plus"></i> Recruitment<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./ApplicantPool.php">Applicant Pool</a>
                                </li>
                                <li>
                                    <a href="./Monitoring.php">Monitoring</a>
                                </li>
                             </ul>
                         </li>
                        <li>
                            <a href="./Accounts.php"><i class="fa fa-dashboard fa-fw"></i> Accounts</a>
                        </li>    
                        <li>
                            <a href="./Appraisal/Test1.php"><i class="fa fa-edit fa-fw"></i> Performance Appraisal<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Appraisal/Test1.php">Test Management</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/SendAppraisal.php">Send Appraisal</a>
                                </li>
                                <li>
                                    <a href="./Appraisal/AppraiseFinal.php">Appraise</a>
                                </li>
                             </ul>
                        </li>    
                        <li>
                            <a href="./Reports.php"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="./Verify.php"> Verify</a>
                                </li>
                             </ul>  
                         </li>    
                        <li>
                            <a href="./SimpleQueryTicket.php"><i class="fa fa-envelope"></i> E-Mail</a>
                        </li>

                        <li>
                            <a href="./DigitalHandbook/DigitalHandbook.php"><i class="fa fa-book"></i> Digital Handbook</a>
                        </li>


                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div>
                        <h1 class="page-header">EHRMS | Recruitment</h1>
                    </div>
<!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Recruitment
                        </div>
                        <div class="panel-body">
<?php
require_once('connection.php');

if(isset($_POST['submit2']))
{
	
	if($_POST['Emp'] == "Pass")
	{
		$con = mysqli_connect('localhost', 'root', '', 'ehrms');
		if (mysqli_connect_errno()) {
			echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
	}

		$ids = $_SESSION['TicketNo'];
        $datehired = date('Y-m-d');
        $i =  "INSERT INTO masteremptb (Status,DateHire,LastAppraisal,AStat) VALUES('1','$datehired','$datehired','0')";
        mysqli_query($con,$i);
        $skio = mysqli_query($con,"SELECT COUNT(EmpId) as B FROM masteremptb where EmpId != '0'");
        while($resu = mysqli_fetch_array($skio))
        {
            $tid = $resu['B'];
        }


        $a1 = mysqli_query($con,"SELECT * FROM ticketpersonaltb WHERE TicketId = '$ids'");
        while($a2 = mysqli_fetch_array($a1))
        {
            $Name = $a2['Name']; //check
            $Address = $a2['Address']; //check
            $company = $a2['Company'];
            $celnum = $a2['CellNum']; //check
            $Mstatus = $a2['Mstatus']; //check
            $Email = $a2['Email']; //check
            $bday = $a2['Dob']; //check
            $company = $a2['Company']; //check
            $Pagibig = $a2['Pagibig'];
            $SSS = $a2['SSS']; //check
            $Tin = $a2['Tin']; //check
            $Phealth = $a2['Philhealth']; //check
            $pos = $a2['PosId'];
        }
        $q1 = "INSERT INTO masterpersonaltb (EmpId, Name, Address, CellNum, Mstatus, Email, Dob, Company, Pagibig, SSS, Tin, Philhealth, PosId) VALUES('$tid','$Name','$Address','$celnum','$Mstatus','$Email','$bday','$company','$Pagibig','$SSS','$Tin','$Phealth','$pos')";
        mysqli_query($con,$q1);		

        $mus = mysqli_query($con,"SELECT COUNT(EmpId) as Id FROM masterpersonaltb Where Company = '$company'");
        while($mus1 = mysqli_fetch_array($mus))
        {
              $musid = $mus1['Id'];
              $musidfinal = $musid;  
        }

         $mus = mysqli_query($con,"SELECT COUNT(EmpId) as Id FROM masterpersonaltb Where Company = '$company'");
        while($mus1 = mysqli_fetch_array($mus))
        {
              $musid = $mus1['Id'];
              $musidfinal = $musid;  
        }

        if($company === "eBiZolution")
        {
            if($musidfinal <= 9)
            {
                if($musidfinal == 1)
                {
                $m = "EBIZ002";
                }
                else{
                $musidfinal = $musidfinal + 1;
                $m = "EBIZ00" . $musidfinal;
                }
            }
            else if ($musidfinal >= 10 && $musidfinal <= 99)
            {
                $musidfinal = $musidfinal + 1;
                $m = "EBIZ0" . $musidfinal;
            }
            else
            {
                $musidfinal = $musidfinal + 1;
                $m = "EBIZ" . $musidfinal;
            }
        }
        else if($company == "Kreativo Koncepto")
        {
               if($musidfinal <= 9)
            {
                $m = "KKI00" . $musidfinal;
            }
            else if ($musidfinal >= 10 && $musidfinal <= 99)
            {
                $m = "KKI0" . $musidfinal;
            }
            else
            {
                $m = "KKI" . $musidfinal;
            }
        }
        else if($company == "Optomization")
        {
                 if($musidfinal <= 9)
            {
                $m = "OPTO00" . $musidfinal;
            }
            else if ($musidfinal >= 10 && $musidfinal <= 99)
            {
                $m = "OPTO0" . $musidfinal;
            }
            else
            {
                $m = "OPTO" . $musidfinal;
            }
        }

        $musupdate = "UPDATE masteremptb SET MusId = '$m' WHERE EmpId = '$tid'";
        mysqli_query($con,$musupdate);

        $b1 = mysqli_query($con,"SELECT * FROM ticketworktb WHERE TicketId = '$ids'");
        while($b2 = mysqli_fetch_array($b1))
        {
            $on = $b2['Employer'];
            $on1 = $b2['Address'];
            $on2 = $b2['DateStart'];
            $on3 = $b2['DateEnd'];
            $on4 = $b2['Position'];
            $on5 = $b2['Reason'];
            $q2 = "INSERT INTO masterworktb (EmpId, Employer, Address, DateStart, DateEnd, Position, Reason) VALUES('$tid','$on','$on1','$on2','$on3','$on4','$on5') ";
            mysqli_query($con,$q2);
       }

       $c1 = mysqli_query($con,"SELECT * FROM tempuploadstb WHERE TicketId = '$ids'");
        while($c2 = mysqli_fetch_array($c1))
        {
            $ob = $c2['Location'];
            $ob1 = $c2['Name'];
       }
        $q3 = "INSERT INTO masteruploadtb (EmpId, Location, Name) VALUES('$tid','$ob','$ob1')";
        mysqli_query($con,$q3);

        $d1 = mysqli_query($con,"SELECT * FROM ticketeductb WHERE TicketId = '$ids'");
        while($d2 = mysqli_fetch_array($d1))
        {
            $bon = $d2['School'];
            $bon1 = $d2['Date'];
            $bon2 = $d2['Qualification'];
            $q4 = "INSERT INTO mastereductb  (EmpId, School, Date, Course) VALUES('$tid','$bon','$bon1','$bon2') ";
            mysqli_query($con,$q4);
       }
        $q5 = "UPDATE tickettb SET Level = '10'  WHERE TicketId = '$ids'";
        mysqli_query($con,$q5);
        echo "<meta http-equiv=\"refresh\" content=\"0; URL=Monitoring.php\">";
	}

	else
		
	{
		$con = mysqli_connect('localhost', 'root', '', 'ehrms');
		if (mysqli_connect_errno()) {
			echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
		}

		$ids = $_SESSION['TicketNo'];
        $result = mysqli_query($con,"Select Description From tickettb where TicketId = '$id'");
        $a1 = mysqli_fetch_array($result);
        $level = $a1['Description'];
        $rem = $_POST['textarea'];
        $as = mysql_query("SELECT Remarks FROM tickettb where TicketId = '$id'");
        $as2 = mysql_fetch_array($as);  
        $remarkold = $a1['Remark'];
        $asd = mysql_query("SELECT Description FROM recviewtb where LevelId = '$level'");
        $asd2 = mysql_fetch_array($asd);    
        $levnames = $a1['Description'];
        $rem2 = $remarkold . "\n" . $levnames . ": " . $rem . " Failed The $level of The Recruitment";
    
        $qryUp = "UPDATE tickettb SET Status = '0', Remarks = '$remarks' WHERE TicketId = '$id' ";
        mysqli_query($con,$qryUp) or die('Error: ' . mysqli_error($con));   
        echo "<meta http-equiv=\"refresh\" content=\"0; URL=Monitoring.php\">";
	}
	
}

?>

<style>
#textarea {
    resize: none;
    }
</style>

<?php

$id = $_GET['TicketId'];
$_SESSION['TicketNo'] = $id;

$qry = "SELECT * FROM tickettb as T INNER JOIN ticketpersonaltb as E WHERE T.TicketId = $id AND E.TicketId = $id";
$result = mysql_query($qry) or die('Error: ');
while($a1 = mysql_fetch_array($result))
{
	$level = $a1['Level'];
	$name = $a1['Name'];	
	$Position = $a1['PosId'];
}

$re = mysql_query("SELECT Description FROM recviewtb where LevelId = $level");
$a1 = mysql_fetch_array($re);
$levname = $a1['Description'];

    $res = "SELECT Description From positionstb where PosId = '$Position'";
    $resultt = mysql_query($res);
	$a1t = mysql_fetch_array($resultt);
	$positions = $a1t['Description'];


?>

<script type="application/javascript">
	

function C(){

		if(document.getElementById("C1").checked)
	{
		if(document.getElementById("C2").checked)
	{
		if(document.getElementById("C3").checked)
	{
		if(document.getElementById("C4").checked)
	{
		if(document.getElementById("C5").checked)
	{
		if(document.getElementById("C6").checked)
	{
		if(document.getElementById("C7").checked)
	{document.getElementById('Emp21').disabled = false;document.getElementById('Emp2').disabled = false;  }else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}
	}else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}}else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}}else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}}else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}}else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}}else {document.getElementById('Emp21').disabled = true;document.getElementById('Emp2').disabled = true;}
}

</script>

</head>
<body>
<center>
<form method="POST" enctype="multipart/form-data" >
<table width='100%' border="0">
	<th height="100" colspan="4" scope="col"><center>
   	<br>
   	<img src="../Uploads/Images/Logo.png" width="279" height="120" />
    <br>
    <br>
    <tr>
    <th colspan="2" scope="col" width="60%"><h4>Name: <label> <?php echo $name; ?></label></h4></th>
    <th colspan="2" scope="col" width="60%"><h4>Position: <label><?php echo $positions; ?> </label></h4></th>
  	</tr>

  	<tr>
    <th colspan="2" scope="col" width="60%"><h4>Level: <label id="LevelName"> <?php echo $levname; ?>
      ( <?php echo $level; ?> )</h4></th>
	<th colspan="2" scope="col" width="60%"><h4>Remarks: <textarea name="textarea" cols="20" rows="4" id="textarea" class="form-control"></textarea></th></th>
	  </tr>

	  <tr>
        <td></td>
	    <td height="69" colspan="3"><h4><b><font color="#196197">Check List:</font></b></h4> </td>
	    </tr>
	    <tr>
        <td></td>
	  	<td colspan="3"><b><input type="checkbox" class="CheckList" id="C1" onfocus="C()"> Social Security System Number (SSS No.)</td>
	  </tr>
      <tr>
        <td height='10'>
        </td>
      </tr>
	  <tr>
        <td></td>
	  	<td colspan="1"><b><input type="checkbox" class="CheckList" id="C2" onfocus="C()"> Diploma</td>
	  	<td colspan="2"><b><input type="checkbox" class="CheckList" id="C3" onfocus="C()"> Medical Certificate</td>
	  </tr>
      <tr>
        <td height='10'>
        </td>
      </tr>
	  <tr>
        <td></td>
	  	<td colspan="1"><b><input type="checkbox" class="CheckList" id="C4" onfocus="C()"> NBI Clearance</td>
	  	<td colspan="2"><b><input type="checkbox" class="CheckList" id="C5" onfocus="C()"> Certificate of Previous Employment</td>
	  </tr>
      <tr>
        <td height='10'>
        </td>
      </tr>
	  <tr>
        <td></td>
	  	<td colspan="1"><b><input type="checkbox" class="CheckList" id="C6" onfocus="C()"> Tin Number</td>
	  	<td colspan="2"><b><input type="checkbox" class="CheckList" id="C7" onfocus="C()">Transcript of Records</td>
	  </tr>

	  <tr><th colspan = "4" height = "50" > </th></tr>
	  
	  <tr>
	    <td width="8%" height="80">&nbsp;</td>
	    <td width="37%"><div align="center"><img src="../Uploads/Images/hire.jpg" width="148" height="153"><br><input type="radio" id="Emp21" class='form-control' name="Emp" required value="Pass" disabled = "false"></div></td>
	    <td width="47%"><div align="center"><img src="../Uploads/Images/fail.png" width="181" height="153"><br><input type="radio" id="Emp2" class='form-control' name="Emp" required value="Fail" disabled = "false"></div></td>
	    <td width="8%">&nbsp; </td>
	  </tr>

	    <tr>   
	    	<td colspan="2">
                <a href='./Monitoring.php' role='button' type='button' class='btn btn-default btn-md btn-outline'>Back</a>
            </td>
            <td colspan='2' align='right'>
                <a href="Rec9.php"><input type="Submit" name="submit2" class='btn btn-primary btn-md btn-outline' value="Submit"></a>
	    	</td>
	    </tr>

</form>

    </table>
    </center>

</div>
</div>
                        
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>
</html>