<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>eBiZolution | E-HRMS Portal</title>

    <!-- Bootstrap Core CSS -->
    <link href="lib/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="lib/css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<!--MODAL-->
<script src="lib/js/jquery-1.11.2.min.js"></script>
<script src="lib/js/bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $("#myModal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);  // Button that triggered the modal
        var titleData = button.data('title'); // Extract value from data-* attributes
        $(this).find('.modal-title').text(titleData + ' Form');
    });
});
</script>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <link href="dashboard/dist/css/sb-admin-2.css" rel="stylesheet">

</head>

<body>

    <div class="intro-header">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>eBiZolution</h1>
                        <h3>E - HRMS PORTAL</h3>
                        <hr class="intro-divider">
                        <div class="row">
                            <div class="col-md-4">
                            </div>

                            <div class="col-md-4">
                            <?php                             
                            if( isset($_SESSION['ERRMSG_ARR']) &&
                            is_array($_SESSION['ERRMSG_ARR']) && 
                            count($_SESSION['ERRMSG_ARR']) >0 ) {
                            echo '<td class="err"> <center>';
                            foreach($_SESSION['ERRMSG_ARR'] as $msg) {                             
                            echo '<font color="#FFFFFF">',$msg,'';   }
                            echo '</font></td>'; unset($_SESSION['ERRMSG_ARR']);}                            
                             ?>

                            <a href="#" class="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#myModal" data-title="Sign In"><i class="fa fa-sign-in fa-fw"></i> <span class="network-name">Sign in</span></a>
                            </div>

                            <div class="col-md-4">
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container -->
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright text-muted small">Copyright &copy; Your Company 2014. All Rights Reserved</p>
                </div>
            </div>
    </div>
    <!-- /.intro-header -->
                
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Window</h4>
                </div>
                <div class="modal-body">
                    <form action="login.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                       <label class="control-label">Username:</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                              <input class="form-control" type="text" name="username" id="username" required placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password:</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                              <input class="form-control" type="password" name="password" id="password" required placeholder="Password">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="submit" id="submit" class="btn btn-outline btn-primary">Log In</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>