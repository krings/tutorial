-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 05, 2015 at 04:07 PM
-- Server version: 5.1.53
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ehrms`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounttb`
--

CREATE TABLE IF NOT EXISTS `accounttb` (
  `AccId` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `Username` text NOT NULL,
  `Password` text NOT NULL,
  `Level` text NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`AccId`),
  UNIQUE KEY `EmpId` (`EmpId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `accounttb`
--

INSERT INTO `accounttb` (`AccId`, `EmpId`, `Username`, `Password`, `Level`, `Status`) VALUES
(0, 0, 'SUPERADMIN', 'admin', '3', 1),
(1, 1, 'nmarquez', 'nmarquez', 'admin', 1),
(2, 2, 'rgobrin', 'rgobrin', 'admin', 1),
(3, 3, 'kdelmendo', 'kdelmendo', '1', 1),
(4, 4, 'rvillanueva', 'rvillanueva', '2', 1),
(5, 5, 'jbenecio', 'jbenecio', '2', 1),
(6, 6, 'aalegre', 'aalegre', '4', 1),
(7, 10, 'mdevera', 'mdevera', '4', 1),
(8, 7, 'evelasco', 'evelasco', '1', 1),
(9, 8, 'dmarquez', 'dmarquez', '1', 1),
(10, 9, 'agarcia', 'agarcia', '1', 1),
(11, 11, 'rurbano', 'rurbano', '1', 1),
(12, 12, 'jpanahon', 'jpanahon', '1', 1),
(13, 13, 'msantos', 'msantos', '1', 1),
(14, 14, 'atadeo', 'atadeo', '1', 1),
(15, 16, 'bakmad', 'bakmad', '1', 1),
(16, 17, 'jpipino', 'jpipino', 'admin', 1),
(17, 18, 'cbalgos', 'cbalgos', 'admin', 1),
(18, 15, 'rdeguzman', '1', '3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `appraisaldatatb`
--

CREATE TABLE IF NOT EXISTS `appraisaldatatb` (
  `AppraisalId` int(11) NOT NULL AUTO_INCREMENT,
  `Evaluee` int(11) NOT NULL,
  `Evaluator` int(11) NOT NULL,
  `TestId` int(11) NOT NULL,
  `QuestionName` text NOT NULL,
  `Question` text NOT NULL,
  `Grade` int(11) NOT NULL,
  `Remarks` text,
  `ADate` date NOT NULL,
  `AppId` text NOT NULL,
  PRIMARY KEY (`AppraisalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `appraisaldatatb`
--

INSERT INTO `appraisaldatatb` (`AppraisalId`, `Evaluee`, `Evaluator`, `TestId`, `QuestionName`, `Question`, `Grade`, `Remarks`, `ADate`, `AppId`) VALUES
(1, 14, 6, 23, 'Productivity', 'Meets or exceeds productivity standards. Completes work in a timely manner.\nWorks accurately and achieves established objectives.\n', 4, 'Excellent!', '2015-08-28', '1462015-08-2811'),
(2, 14, 6, 23, 'Adaptability and Dependability', 'Adapts to changes in the work environment. Manages competing demands. Performs well under pressure. Follow instructions; responds to management direction and meets attendance and punctuality guidelines.\n', 3, '', '2015-08-28', '1462015-08-2811'),
(3, 14, 6, 23, 'Initiative and Judgement', 'Takes independent action based on sound judgement. Seeks increased responsibility. Demonstrates effective and timely decision making\nskills and volunteers readily. ', 4, '', '2015-08-28', '1462015-08-2811'),
(4, 14, 6, 23, 'Communications', 'Selects and uses appropriate communications\nmethods. Effectively expresses ideas verbally. Effectively expresses ideas in writing. Communicates effectively with external units. Communicates effectively with colleagues. Communicates effectively with supervisor.', 4, '', '2015-08-28', '1462015-08-2811'),
(5, 14, 6, 23, 'Cooperation and Teamwork', 'Maintains effective working relationships. Exhibits openness to othersâ€™ views. Works cooperatively in a group situation. Contributes to building positive team spirit and offers assistance and support to co-workers.\n', 2, '', '2015-08-28', '1462015-08-2811'),
(6, 14, 6, 23, 'Continuous Improvement', 'Provides guidance and training to units. Identifies areas for improvement and develops solutions. Leads or participates in teams to improve processes.\n', 4, '', '2015-08-28', '1462015-08-2811'),
(7, 14, 6, 23, 'Analysis, Assessment and Problem Solving', 'Collects appropriate information. Develops recommendations based on thorough\nanalysis and supports/explains reasoning for recommendations.\n', 3, '', '2015-08-28', '1462015-08-2811'),
(8, 14, 6, 23, 'Customer Service', 'Displays courtesy and sensitivity. Meets commitments. Responds promptly to customer needs and requests. Solicits customer feedback to improve services.\n', 4, '', '2015-08-28', '1462015-08-2811'),
(9, 14, 6, 23, 'Professional Development', 'Displays competence in required job skills. Learns and applies new skills. Keeps abreast of current developments.', 3, '', '2015-08-28', '1462015-08-2811'),
(10, 14, 6, 23, 'Stewardship', 'Demonstrates accountability and sound judgement in\nmanaging company resources in an open and effective\nmanner. Demonstrates appropriate understanding of\nconfidentiality and company values. Demonstrates adherence to policies, procedures, and safety guidelines.', 4, 'Satisfactory!', '2015-08-28', '1462015-08-2811'),
(11, 14, 6, 23, 'Cross-Cultural Competence', 'Promotes cooperation, fairness and equity. Demonstrates respect for people and their differences. Works to understand perspectives and world views of\nothers; demonstrates empathy. Provides for and/or contributes to a culture of inclusion. Integrates diversity into business practices. ', 3, '', '2015-08-28', '1462015-08-2811'),
(12, 14, 6, 22, 'Quality of Work', 'Deals with accuracy, completeness and neatness of work, including safety consciousness.', 4, '', '2015-08-28', '1062015-08-2815'),
(13, 14, 6, 22, 'Job Knowledge', 'The extent the employee knows and understands the techniques, methods, details and nature of his assigned job and related duties.', 4, '', '2015-08-28', '1062015-08-2815'),
(14, 14, 6, 22, 'Timeliness of Job Assignment', 'Effectiveness in planning and utilizing time to meet commitments, handle unexpected situations or deadlines and accomplish special work assignments well within schedule.', 3, '', '2015-08-28', '1062015-08-2815'),
(15, 14, 6, 22, 'Adherence to Company Regulations', 'Shows regard for company regulations and procedures; extent to which an employee conscientiously abides by SOPâ€™s and personnel policies, behave and conducts himself in accordance with prescribed office procedures.', 2, '', '2015-08-28', '1062015-08-2815'),
(16, 14, 6, 22, 'Cooperation', 'Effectiveness in establishing rapport and contributing to the general atmosphere of friendship and goodwill in the group; also consider employeeâ€™s willingness to follow assignments or instructions.', 4, '', '2015-08-28', '1062015-08-2815'),
(17, 14, 6, 22, 'Initiative', 'Demonstrates the ability to act independently with desirable self-confidence; sets what needs to be done and does it without being told, and seeking opportunities to improve self and job.', 4, 'Excellent!', '2015-08-28', '1062015-08-2815'),
(18, 14, 6, 22, 'Work Attitude', 'Concerns the employeeâ€™s attitude toward his work and work environment.  It involves such things as willingness to respond to extra effort to meet unusual conditions, to conform to established policies and procedures.', 0, '', '2015-08-28', '1062015-08-2815'),
(19, 14, 6, 22, 'Dependability', 'Sense of  duty; willingness to perform and complete a job at any cost to self; extent an employee can be depended upon to be available for work when circumstances require; the degree to which the employee is reliable and trustworthy.', 3, '', '2015-08-28', '1062015-08-2815'),
(20, 14, 6, 22, 'Punctuality and Attendance', 'The extent the employee reports to work on time, observes work hours and break periods, takes every opportunity to make-up for time lost and takes leaves for valid reasons.', 3, '', '2015-08-28', '1062015-08-2815'),
(21, 14, 6, 22, 'Communication and Interpersonal Skills', 'Degree of oral and written communication and other personal interaction skills required to be able relate to, coordinate with, persuade, influence, sell to and motivate people within and outside the company.', 3, '', '2015-08-28', '1062015-08-2815'),
(22, 14, 6, 22, 'Analytical Ability and Creativity', 'Degree of mental process required to arrive at varied courses of action and decisions, ability to exercise good judgment, ingenuity and creativity under a given situation.', 4, '', '2015-08-28', '1062015-08-2815'),
(23, 14, 6, 22, 'Planning and Control', 'Ability to program, schedule and integrate various work activities to anticipate needs, determine priorities and develop contingent plans to achieve various results.', 3, 'Good!', '2015-08-28', '1062015-08-2815'),
(24, 14, 6, 22, 'Relationship with Others', 'Consider employeeâ€™s abilities to maintain a positive and harmonious attitude in the work environment. How well does the employee relate to the supervisors, co-workers and the broader University community. ', 0, '', '2015-08-28', '1062015-08-2815'),
(25, 14, 6, 22, 'Commitment to Safety', 'To what extent has the employee adhered to the recommended safe work practices,\nparticipated in safety training programs; and contributes to the recognition and control of hazard in his/her work area.', 4, '', '2015-08-28', '1062015-08-2815'),
(26, 14, 6, 22, 'Supervisor Ability', 'In the evaluation of this factor, consider the employeeâ€™s ability to organize, plan, train, delegate and control the work of subordinates in an effective manner. ', 3, '', '2015-08-28', '1062015-08-2815'),
(27, 14, 7, 25, 'Team Performance Evaluation', 'How skilled at their jobs are the members of your team?', 4, '', '2015-08-28', '1472015-08-2810'),
(28, 14, 7, 25, 'Team Performance Evaluation', 'How professionally do the members of your team behave?', 4, '', '2015-08-28', '1472015-08-2810'),
(29, 14, 7, 25, 'Team Performance Evaluation', 'How honest with each other are the members of your team?', 4, '', '2015-08-28', '1472015-08-2810'),
(30, 14, 7, 25, 'Team Performance Evaluation', 'How well do members of your team share responsibility for tasks?', 3, '', '2015-08-28', '1472015-08-2810'),
(31, 14, 7, 25, 'Team Performance Evaluation', 'How well does your supervisor work with clients?', 4, '', '2015-08-28', '1472015-08-2810'),
(32, 14, 7, 25, 'Team Performance Evaluation', 'How efficiently are team meetings conducted?', 4, '', '2015-08-28', '1472015-08-2810'),
(33, 14, 7, 25, 'Team Performance Evaluation', 'How well do the members of your team communicate with each other?', 4, '', '2015-08-28', '1472015-08-2810'),
(34, 14, 7, 25, 'Team Performance Evaluation', 'How hardworking is your supervisor?', 3, '', '2015-08-28', '1472015-08-2810'),
(35, 14, 7, 25, 'Team Performance Evaluation', 'How often does your team meet its deadlines?', 4, '', '2015-08-28', '1472015-08-2810'),
(36, 14, 7, 25, 'Team Performance Evalutaion', 'How well does your supervisor handle criticism?', 4, '', '2015-08-28', '1472015-08-2810'),
(37, 14, 7, 25, 'Team Performance Evaluation', 'How fairly are responsibilities shared among your team members?', 4, '', '2015-08-28', '1472015-08-2810'),
(38, 14, 7, 25, 'Team Performance Evaluation', 'How politely do members of your team treat each other?', 4, '', '2015-08-28', '1472015-08-2810'),
(39, 14, 7, 25, 'Team Performance Evaluation', 'How quickly does your team adjust to changing priorities?', 4, '', '2015-08-28', '1472015-08-2810'),
(40, 14, 7, 25, 'Team Performance Evaluation', 'How quickly does your team act on its decisions?', 4, '', '2015-08-28', '1472015-08-2810'),
(41, 14, 7, 25, 'Team Performance Evaluation', 'Would you say that your team has too many meetings, too few meetings, or about the right number?', 4, '', '2015-08-28', '1472015-08-2810'),
(42, 14, 7, 25, 'Team Performance Evaluation', 'Should your team be larger than it is, smaller than it is, or is the size about right?', 4, '', '2015-08-28', '1472015-08-2810'),
(43, 14, 7, 25, 'Team Performance Evaluation', 'How likely is it that you would recommend this company as a place to work to a friend or colleague?', 4, '', '2015-08-28', '1472015-08-2810'),
(44, 12, 6, 28, 'Supervisor Performance Evaluation', 'How approachable is your supervisor?', 4, '', '2015-08-29', '1262015-08-2934'),
(45, 12, 6, 28, 'Supervisor Performance Evaluation', 'How available to employees is your supervisor?', 3, '', '2015-08-29', '1262015-08-2934'),
(46, 12, 6, 28, 'Supervisor Performance Evaluation', 'How often does your supervisor give you feedback about your work?', 4, '', '2015-08-29', '1262015-08-2934'),
(47, 12, 6, 28, 'Supervisor Performance Evaluation', 'How improved is your performance after getting feedback from your supervisor about your work?', 3, '', '2015-08-29', '1262015-08-2934'),
(48, 12, 6, 28, 'Supervisor Performance Evaluation', 'How effective is the training you receive from your supervisor?', 4, '', '2015-08-29', '1262015-08-2934'),
(49, 12, 6, 28, 'Supervisor Performance Evaluation', ' How consistently does your supervisor reward employees for good work?', 3, '', '2015-08-29', '1262015-08-2934'),
(50, 12, 6, 28, 'Supervisor Performance Evaluation', 'How consistently does your supervisor punish employees for bad work?', 3, '', '2015-08-29', '1262015-08-2934'),
(51, 12, 6, 28, 'Supervisor Performance Evaluation', 'How reasonable are the decisions made by your supervisor?', 4, '', '2015-08-29', '1262015-08-2934'),
(52, 12, 6, 28, 'Supervisor Performance Evaluation', 'Does your supervisor take too much time to make decisions, too little time, or about the right amount of time?', 4, '', '2015-08-29', '1262015-08-2934'),
(53, 12, 6, 28, 'Supervisor Performance Evaluation', ' How often does your supervisor listen to employees` opinions when making decisions?', 4, '', '2015-08-29', '1262015-08-2934'),
(54, 12, 6, 28, 'Supervisor Performance Evaluation', 'How easy is it for employees to disagree with the decisions made by your supervisor?', 4, '', '2015-08-29', '1262015-08-2934'),
(55, 12, 6, 28, 'Supervisor Performance Evaluation', 'How well does your supervisor respond to employees` mistakes?', 4, '', '2015-08-29', '1262015-08-2934'),
(56, 12, 6, 28, 'Supervisor Performance Evaluation', 'How reliable is your supervisor?', 4, '', '2015-08-29', '1262015-08-2934'),
(57, 12, 6, 28, 'Supervisor Performance Evaluation', 'How effectively does your supervisor use company resources?', 3, '', '2015-08-29', '1262015-08-2934'),
(58, 12, 6, 28, 'Supervisor Performance Evaluation', 'How effectively does your supervisor use company resources?', 4, '', '2015-08-29', '1262015-08-2934'),
(59, 12, 6, 28, 'Supervisor Performance Evaluation', 'Overall, are you satisfied with your supervisor, neither satisfied nor dissatisfied with him/her, or dissatisfied with him/her?', 3, '', '2015-08-29', '1262015-08-2934');

-- --------------------------------------------------------

--
-- Table structure for table `appraisaltb`
--

CREATE TABLE IF NOT EXISTS `appraisaltb` (
  `QuestionId` int(11) NOT NULL AUTO_INCREMENT,
  `TestId` int(11) NOT NULL,
  `jobcompetence` text NOT NULL,
  `definition` text NOT NULL,
  PRIMARY KEY (`QuestionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=184 ;

--
-- Dumping data for table `appraisaltb`
--

INSERT INTO `appraisaltb` (`QuestionId`, `TestId`, `jobcompetence`, `definition`) VALUES
(1, 1, 'Quality Management ', 'Quality of work produced or accomplished compared with accepted standards of performance.'),
(8, 1, 'Promptness', 'Basic Urge to do things ahead or within the allotted time.	'),
(9, 1, 'Job knowledge', 'Knowledge and understanding of the basic skills, standards, procedures and technical expertise of the job. '),
(10, 1, 'Dependability', 'Reliability in assuming and carrying out the commitments and obligation of his/her position. '),
(11, 1, 'Analysis/Judgment', 'Effectiveness in thinking through a problem in recognizing, securing and evaluating relevant facts and in reaching a conclusions. '),
(12, 2, 'Organization / Control', 'Ability to plan and organize work effectively. '),
(14, 2, 'Innovation', 'Ability to asses situation, develop alternative solutions. '),
(16, 3, 'Leadership', 'Ability to effective supervise, motivate, guide and support subordinates. '),
(17, 3, 'Communicating', 'Effectiveness in presenting thoughts and ideas and in keeping associates and superiors adequately informed. '),
(18, 3, 'Efficiency', 'Accuracy, Completeness, neatness and orderliness of the job performed.'),
(19, 4, 'Customer Focus', 'Ability to provide excellent customer service.	'),
(20, 4, 'Quantity of Work / Productivity', 'Volume of Acceptable work acceptable compared with what may reasonably be expected. Ability to reach desired target/quota. Ability to maximize given time to produce accurate work.	'),
(21, 1, 'Client Management', 'Ability to address / provide client needs / requirements. '),
(95, 22, 'Quality of Work', 'Deals with accuracy, completeness and neatness of work, including safety consciousness.'),
(96, 22, 'Job Knowledge', 'The extent the employee knows and understands the techniques, methods, details and nature of his assigned job and related duties.'),
(97, 22, 'Timeliness of Job Assignment', 'Effectiveness in planning and utilizing time to meet commitments, handle unexpected situations or deadlines and accomplish special work assignments well within schedule.'),
(98, 22, 'Adherence to Company Regulations', 'Shows regard for company regulations and procedures; extent to which an employee conscientiously abides by SOPâ€™s and personnel policies, behave and conducts himself in accordance with prescribed office procedures.'),
(99, 22, 'Cooperation', 'Effectiveness in establishing rapport and contributing to the general atmosphere of friendship and goodwill in the group; also consider employeeâ€™s willingness to follow assignments or instructions.'),
(100, 22, 'Initiative', 'Demonstrates the ability to act independently with desirable self-confidence; sets what needs to be done and does it without being told, and seeking opportunities to improve self and job.'),
(101, 22, 'Work Attitude', 'Concerns the employeeâ€™s attitude toward his work and work environment.  It involves such things as willingness to respond to extra effort to meet unusual conditions, to conform to established policies and procedures.'),
(102, 22, 'Dependability', 'Sense of  duty; willingness to perform and complete a job at any cost to self; extent an employee can be depended upon to be available for work when circumstances require; the degree to which the employee is reliable and trustworthy.'),
(103, 22, 'Punctuality and Attendance', 'The extent the employee reports to work on time, observes work hours and break periods, takes every opportunity to make-up for time lost and takes leaves for valid reasons.'),
(104, 22, 'Communication and Interpersonal Skills', 'Degree of oral and written communication and other personal interaction skills required to be able relate to, coordinate with, persuade, influence, sell to and motivate people within and outside the company.'),
(105, 22, 'Analytical Ability and Creativity', 'Degree of mental process required to arrive at varied courses of action and decisions, ability to exercise good judgment, ingenuity and creativity under a given situation.'),
(106, 22, 'Planning and Control', 'Ability to program, schedule and integrate various work activities to anticipate needs, determine priorities and develop contingent plans to achieve various results.'),
(107, 22, 'Relationship with Others', 'Consider employeeâ€™s abilities to maintain a positive and harmonious attitude in the work environment. How well does the employee relate to the supervisors, co-workers and the broader University community. '),
(108, 22, 'Commitment to Safety', 'To what extent has the employee adhered to the recommended safe work practices,\r\nparticipated in safety training programs; and contributes to the recognition and control of hazard in his/her work area.'),
(109, 22, 'Supervisor Ability', 'In the evaluation of this factor, consider the employeeâ€™s ability to organize, plan, train, delegate and control the work of subordinates in an effective manner. '),
(110, 23, 'Productivity', 'Meets or exceeds productivity standards. Completes work in a timely manner.\r\nWorks accurately and achieves established objectives.\r\n'),
(111, 23, 'Adaptability and Dependability', 'Adapts to changes in the work environment. Manages competing demands. Performs well under pressure. Follow instructions; responds to management direction and meets attendance and punctuality guidelines.\r\n'),
(112, 23, 'Initiative and Judgement', 'Takes independent action based on sound judgement. Seeks increased responsibility. Demonstrates effective and timely decision making\r\nskills and volunteers readily. '),
(113, 23, 'Communications', 'Selects and uses appropriate communications\r\nmethods. Effectively expresses ideas verbally. Effectively expresses ideas in writing. Communicates effectively with external units. Communicates effectively with colleagues. Communicates effectively with supervisor.'),
(114, 23, 'Cooperation and Teamwork', 'Maintains effective working relationships. Exhibits openness to othersâ€™ views. Works cooperatively in a group situation. Contributes to building positive team spirit and offers assistance and support to co-workers.\r\n'),
(115, 23, 'Continuous Improvement', 'Provides guidance and training to units. Identifies areas for improvement and develops solutions. Leads or participates in teams to improve processes.\r\n'),
(116, 23, 'Analysis, Assessment and Problem Solving', 'Collects appropriate information. Develops recommendations based on thorough\r\nanalysis and supports/explains reasoning for recommendations.\r\n'),
(117, 23, 'Customer Service', 'Displays courtesy and sensitivity. Meets commitments. Responds promptly to customer needs and requests. Solicits customer feedback to improve services.\r\n'),
(118, 23, 'Professional Development', 'Displays competence in required job skills. Learns and applies new skills. Keeps abreast of current developments.'),
(119, 23, 'Stewardship', 'Demonstrates accountability and sound judgement in\r\nmanaging company resources in an open and effective\r\nmanner. Demonstrates appropriate understanding of\r\nconfidentiality and company values. Demonstrates adherence to policies, procedures, and safety guidelines.'),
(120, 23, 'Cross-Cultural Competence', 'Promotes cooperation, fairness and equity. Demonstrates respect for people and their differences. Works to understand perspectives and world views of\r\nothers; demonstrates empathy. Provides for and/or contributes to a culture of inclusion. Integrates diversity into business practices. '),
(121, 24, 'Employee Satisfaction Evaluation', 'How meaningful is your work?'),
(122, 24, 'Employee Satisfaction Evaluation', 'How challenging is your job?'),
(123, 24, 'Employee Satisfaction Evaluation', 'In a typical week, how often do you feel stressed at work?'),
(124, 24, 'Employee Satisfaction Evaluation', 'How well are you paid for the work you do?'),
(125, 24, 'Employee Satisfaction Evaluation', 'How much do your opinions about work matter to your co-workers?'),
(126, 24, 'Employee Satisfaction Evaluation', 'How realistic are the expectations of your supervisor?'),
(127, 24, 'Employee Satisfaction Evaluation', 'How often do the tasks assigned to you by your supervisor help you grow professionally?'),
(128, 24, 'Employee Satisfaction Evaluation', 'How many opportunities do you have to get promoted where you work?'),
(129, 24, 'Employee Satisfaction Evaluation', 'Are you supervised too much at work, supervised too little, or supervised about the right amount?'),
(130, 24, 'Employee Satisfaction Evaluation', 'Are you satisfied with your employee benefits, neither satisfied nor dissatisfied with them, or dissatisfied with them?'),
(131, 24, 'Employee Satisfaction Evaluation', 'Are you satisfied with your job, neither satisfied nor dissatisfied with it, or dissatisfied with it?'),
(132, 24, 'Employee Satisfaction Evaluation', 'Do you like your employer, neither like nor dislike them, or dislike them?'),
(133, 24, 'Employee Satisfaction Evaluation', 'How likely are you to look for another job outside the company?'),
(134, 25, 'Team Performance Evaluation', 'How skilled at their jobs are the members of your team?'),
(135, 25, 'Team Performance Evaluation', 'How professionally do the members of your team behave?'),
(136, 25, 'Team Performance Evaluation', 'How honest with each other are the members of your team?'),
(137, 25, 'Team Performance Evaluation', 'How well do members of your team share responsibility for tasks?'),
(138, 25, 'Team Performance Evaluation', 'How well does your supervisor work with clients?'),
(139, 25, 'Team Performance Evaluation', 'How efficiently are team meetings conducted?'),
(140, 25, 'Team Performance Evaluation', 'How well do the members of your team communicate with each other?'),
(141, 25, 'Team Performance Evaluation', 'How hardworking is your supervisor?'),
(142, 25, 'Team Performance Evaluation', 'How often does your team meet its deadlines?'),
(143, 25, 'Team Performance Evalutaion', 'How well does your supervisor handle criticism?'),
(144, 25, 'Team Performance Evaluation', 'How fairly are responsibilities shared among your team members?'),
(145, 25, 'Team Performance Evaluation', 'How politely do members of your team treat each other?'),
(146, 25, 'Team Performance Evaluation', 'How quickly does your team adjust to changing priorities?'),
(147, 25, 'Team Performance Evaluation', 'How quickly does your team act on its decisions?'),
(148, 25, 'Team Performance Evaluation', 'Would you say that your team has too many meetings, too few meetings, or about the right number?'),
(149, 25, 'Team Performance Evaluation', 'Should your team be larger than it is, smaller than it is, or is the size about right?'),
(150, 25, 'Team Performance Evaluation', 'How likely is it that you would recommend this company as a place to work to a friend or colleague?'),
(151, 26, 'Management Performance Evaluation', 'How comfortable do you feel voicing your concerns to your supervisor?'),
(152, 26, 'Management Performance Evaluation', 'Would you like to interact with your supervisor more, less, or about the same amount as you currently interact with him/her?'),
(153, 26, 'Management Performance Evaluation', 'When decisions or goals change, how often does your supervisor explain to you why this has happened?'),
(154, 26, 'Management Performance Evaluation', 'How well does management handle political issues that affect the company?'),
(155, 26, 'Management Performance Evaluation', 'How effective is management at public relations?'),
(156, 26, 'Management Performance Evaluation', 'Does your supervisor spend too much money to meet short-term goals, too little money, or about the right amount of money?'),
(157, 26, 'Management Performance Evaluation', 'How effectively does your supervisor use company resources?'),
(158, 26, 'Management Performance Evaluation', 'How well does your supervisor handle employee problems?'),
(159, 26, 'Management Performance Evaluation', 'How committed is your supervisor to making the company a more comfortable place to work?'),
(160, 26, 'Management Performance Evaluation', 'How clearly does your supervisor explain the company`s business plans?'),
(161, 26, 'Management Performance Evaluation', 'How well do your supervisor`s priorities match up with the goals of your company?'),
(162, 27, 'Employee Benefits Evaluation', 'How comfortable is your employer`s work environment?'),
(163, 27, 'Employee Benefits Evaluation', 'Are you satisfied with your options for getting to and from work, neither satisfied nor dissatisfied with them, or dissatisfied with them?'),
(164, 27, 'Employee Benefits Evaluation', 'Is your employer`s health insurance plan better, worse, or about the same as those of other employers?'),
(165, 27, 'Employee Benefits Evaluation', 'Are you satisfied with your employer`s catered meals, neither satisfied nor dissatisfied with them, or dissatisfied with them?'),
(166, 27, 'Employee Benefits Evaluation', 'How fair is your employer`s sick day policy?'),
(167, 27, 'Employee Benefits Evaluation', 'Are you satisfied with your employee benefits, neither satisfied nor dissatisfied with them, or dissatisfied with them?'),
(168, 28, 'Supervisor Performance Evaluation', 'How approachable is your supervisor?'),
(169, 28, 'Supervisor Performance Evaluation', 'How available to employees is your supervisor?'),
(170, 28, 'Supervisor Performance Evaluation', 'How often does your supervisor give you feedback about your work?'),
(171, 28, 'Supervisor Performance Evaluation', 'How improved is your performance after getting feedback from your supervisor about your work?'),
(172, 28, 'Supervisor Performance Evaluation', 'How effective is the training you receive from your supervisor?'),
(173, 28, 'Supervisor Performance Evaluation', ' How consistently does your supervisor reward employees for good work?'),
(174, 28, 'Supervisor Performance Evaluation', 'How consistently does your supervisor punish employees for bad work?'),
(175, 28, 'Supervisor Performance Evaluation', 'How reasonable are the decisions made by your supervisor?'),
(176, 28, 'Supervisor Performance Evaluation', 'Does your supervisor take too much time to make decisions, too little time, or about the right amount of time?'),
(177, 28, 'Supervisor Performance Evaluation', ' How often does your supervisor listen to employees` opinions when making decisions?'),
(178, 28, 'Supervisor Performance Evaluation', 'How easy is it for employees to disagree with the decisions made by your supervisor?'),
(179, 28, 'Supervisor Performance Evaluation', 'How well does your supervisor respond to employees` mistakes?'),
(180, 28, 'Supervisor Performance Evaluation', 'How reliable is your supervisor?'),
(181, 28, 'Supervisor Performance Evaluation', 'How effectively does your supervisor use company resources?'),
(182, 28, 'Supervisor Performance Evaluation', 'How effectively does your supervisor use company resources?'),
(183, 28, 'Supervisor Performance Evaluation', 'Overall, are you satisfied with your supervisor, neither satisfied nor dissatisfied with him/her, or dissatisfied with him/her?');

-- --------------------------------------------------------

--
-- Table structure for table `dashboardtb`
--

CREATE TABLE IF NOT EXISTS `dashboardtb` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Message` text NOT NULL,
  `Subject` text NOT NULL,
  `Sender` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Status` int(11) NOT NULL,
  `Reciever` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `TestId` int(11) DEFAULT NULL,
  `Ratee` int(11) DEFAULT NULL,
  `AppStatus` int(11) NOT NULL DEFAULT '0',
  `AppId` text,
  `AppRef` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `dashboardtb`
--

INSERT INTO `dashboardtb` (`Id`, `Message`, `Subject`, `Sender`, `Date`, `Status`, `Reciever`, `Type`, `TestId`, `Ratee`, `AppStatus`, `AppId`, `AppRef`) VALUES
(1, 'Try', 'Try', 5, '2015-08-27', 0, 5, 1, NULL, NULL, 0, NULL, NULL),
(2, 'Name: Jesusa Benecio \r\nCompany: eBiZolution \r\nDepartment: Admin and Finance \r\nPosition:  Accountant  \r\n\r\nIs ready to be Appraised! Please processes Immediately!', 'Performance Appraisal Update', 0, '2015-08-28', 1, 0, 1, NULL, NULL, 0, NULL, NULL),
(3, 'Name: Melvin De Vera \r\nCompany: eBiZolution \r\nDepartment: IT \r\nPosition:  Technical Associate  \r\n\r\nIs ready to be Appraised! Please processes Immediately!', 'Performance Appraisal Update', 0, '2015-08-28', 1, 0, 1, NULL, NULL, 0, NULL, NULL),
(4, 'Name: Mark Dominic Daval-Santos \r\nCompany: eBiZolution \r\nDepartment: Sales \r\nPosition:  Technical Pre-Sales  \r\n\r\nIs ready to be Appraised! Please processes Immediately!', 'Performance Appraisal Update', 0, '2015-08-28', 1, 0, 1, NULL, NULL, 0, NULL, NULL),
(5, 'Name: Al-Jhay Kevin Tadeo \r\nCompany: eBiZolution \r\nDepartment: IT \r\nPosition:  Technical Associate  \r\n\r\nIs ready to be Appraised! Please processes Immediately!', 'Performance Appraisal Update', 0, '2015-08-28', 1, 0, 1, NULL, NULL, 0, NULL, NULL),
(10, 'You are chosen to be the rater for this specific Performance Appraisal. Please Accomplish Immediately.', 'Performance Appraisal Ticket', 6, '2015-08-28', 0, 7, 2, 25, 14, 1, '1472015-08-2810', NULL),
(11, 'You are chosen to be the rater for this specific Performance Appraisal. Please Accomplish Immediately.', 'Performance Appraisal Ticket', 6, '2015-08-28', 0, 6, 2, 23, 14, 1, '1462015-08-2811', NULL),
(12, 'You are chosen to be the rater for this specific Performance Appraisal. Please Accomplish Immediately.', 'Performance Appraisal Ticket', 6, '2015-08-28', 0, 10, 2, 22, 14, 0, NULL, NULL),
(13, 'I have already finished the Evaluation for Al-Jhay Kevin Tadeo.', 'Performance Appraisal Ticket', 6, '2015-08-28', 1, 0, 3, 23, 14, 0, NULL, '1462015-08-2811'),
(14, 'I have already finished the Evaluation for Al-Jhay Kevin Tadeo.', 'Performance Appraisal Ticket', 6, '2015-08-28', 0, 6, 3, 23, 14, 0, NULL, '1462015-08-2811'),
(15, 'You are chosen to be the rater for this specific Performance Appraisal. Please Accomplish Immediately.', 'Performance Appraisal Ticket', 6, '2015-08-28', 0, 6, 2, 22, 10, 1, '1062015-08-2815', NULL),
(16, 'I have already finished the Evaluation for Melvin De Vera.', 'Performance Appraisal Ticket', 6, '2015-08-28', 1, 0, 3, 22, 10, 0, NULL, '1062015-08-2815'),
(17, 'I have already finished the Evaluation for Melvin De Vera.', 'Performance Appraisal Ticket', 6, '2015-08-28', 0, 6, 3, 22, 10, 0, NULL, '1062015-08-2815'),
(18, 'I have already finished the Evaluation for Al-Jhay Kevin Tadeo.', 'Performance Appraisal Ticket', 7, '2015-08-28', 1, 0, 3, 25, 14, 0, NULL, '1472015-08-2810'),
(19, 'I have already finished the Evaluation for Al-Jhay Kevin Tadeo.', 'Performance Appraisal Ticket', 7, '2015-08-28', 1, 15, 3, 25, 14, 0, NULL, '1472015-08-2810'),
(29, 'I have already finished your Evaluation.', 'Performance Appraisal', 15, '2015-08-28', 0, 14, 4, NULL, NULL, 0, NULL, NULL),
(30, 'I would to request a employee for the position said above.', 'Assistant Account', 15, '2015-08-28', 0, 15, 1, NULL, NULL, 0, NULL, NULL),
(31, 'I have already posted recruitment ads. thank you...', 'Assistant Accountant', 15, '2015-08-28', 1, 15, 1, NULL, NULL, 0, NULL, NULL),
(32, 'Name: Joselito Panahon \r\nCompany: eBiZolution \r\nDepartment: IT \r\nPosition:  Technical Associate  \r\n\r\nIs ready to be Appraised! Please processes Immediately!', 'Performance Appraisal Update', 0, '2015-08-29', 1, 0, 1, NULL, NULL, 0, NULL, NULL),
(33, 'Name: Joselito Panahon \r\nCompany: eBiZolution \r\nDepartment: IT \r\nPosition:  Technical Associate  \r\n\r\nIs ready to be Appraised! Please processes Immediately!', 'Performance Appraisal Update', 0, '2015-08-29', 0, 15, 1, NULL, NULL, 0, NULL, NULL),
(34, 'You are chosen to be the rater for this specific Performance Appraisal. Please Accomplish Immediately.', 'Performance Appraisal Ticket', 15, '2015-08-29', 0, 6, 2, 29, 12, 1, '1262015-08-2934', NULL),
(35, 'I have already finished the Evaluation for Joselito Panahon.', 'Performance Appraisal Ticket', 6, '2015-08-29', 1, 0, 3, 28, 12, 0, NULL, '1262015-08-2934'),
(36, 'I have already finished the Evaluation for Joselito Panahon.', 'Performance Appraisal Ticket', 6, '2015-08-29', 0, 15, 3, 28, 12, 0, NULL, '1262015-08-2934'),
(37, 'IT Associate', 'Job Request', 6, '2015-09-03', 0, 15, 1, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deductionstb`
--

CREATE TABLE IF NOT EXISTS `deductionstb` (
  `DeductionId` int(11) NOT NULL AUTO_INCREMENT,
  `DeducName` text NOT NULL,
  `DeducAmount` double NOT NULL,
  `DeductionType` int(11) NOT NULL,
  `Start` date NOT NULL,
  `End` date NOT NULL,
  `EmpId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`DeductionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `deductionstb`
--

INSERT INTO `deductionstb` (`DeductionId`, `DeducName`, `DeducAmount`, `DeductionType`, `Start`, `End`, `EmpId`, `Status`) VALUES
(1, 'Other Advances', 500, 1, '2015-02-01', '2015-02-01', 5, 1),
(2, 'Mobile', 135.7, 1, '2015-02-01', '2015-02-01', 5, 1),
(3, 'HDMF Loan', 500, 2, '2015-02-01', '2015-02-15', 5, 1),
(4, 'HDMF Loan', 150, 1, '2015-01-01', '2015-01-15', 9, 0),
(5, 'SSS Loan', 323.03, 1, '2015-01-01', '2015-01-15', 9, 0),
(6, 'WTC', 50, 1, '2015-01-01', '2015-01-15', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `finalappraisaltb`
--

CREATE TABLE IF NOT EXISTS `finalappraisaltb` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TestName` text NOT NULL,
  `Grade` float NOT NULL,
  `Ave` float NOT NULL,
  `Remarks` text NOT NULL,
  `FinalRemark` text NOT NULL,
  `Evaluee` int(11) NOT NULL,
  `Date` date NOT NULL,
  `LastAppraisal` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `finalappraisaltb`
--

INSERT INTO `finalappraisaltb` (`Id`, `TestName`, `Grade`, `Ave`, `Remarks`, `FinalRemark`, `Evaluee`, `Date`, `LastAppraisal`) VALUES
(1, 'Qualitative Factors Evaluation', 2.93, 3.42, 'Quality of Work : <br>Job Knowledge : <br>Timeliness of Job Assignment : <br>Adherence to Company Regulations : <br>Cooperation : <br>Initiative : Excellent!<br>Work Attitude : <br>Dependability : <br>Punctuality and Attendance : <br>Communication and Interpersonal Skills : <br>Analytical Ability and Creativity : <br>Planning and Control : Good!<br>Relationship with Others : <br>Commitment to Safety : <br>Supervisor Ability : <br>', 'Normally expected performance; consistently meets requirements of position.', 14, '2015-08-28', '2015-02-01'),
(2, 'Job Responsibilities Evaluation', 3.45, 3.42, 'Productivity : Excellent!<br>Adaptability and Dependability : <br>Initiative and Judgement : <br>Communications : <br>Cooperation and Teamwork : <br>Continuous Improvement : <br>Analysis, Assessment and Problem Solving : <br>Customer Service : <br>Professional Development : <br>Stewardship : Satisfactory!<br>Cross-Cultural Competence : <br>', 'Normally expected performance; consistently meets requirements of position.', 14, '2015-08-28', '2015-02-01'),
(3, 'Team Performance Evaluation', 3.88, 3.42, 'Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evalutaion : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>Team Performance Evaluation : <br>', 'Normally expected performance; consistently meets requirements of position.', 14, '2015-08-28', '2015-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `holidaytb`
--

CREATE TABLE IF NOT EXISTS `holidaytb` (
  `HId` int(11) NOT NULL AUTO_INCREMENT,
  `HolidayName` text NOT NULL,
  `Date` date NOT NULL,
  `Type` int(11) NOT NULL,
  PRIMARY KEY (`HId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `holidaytb`
--

INSERT INTO `holidaytb` (`HId`, `HolidayName`, `Date`, `Type`) VALUES
(1, 'New Year''s Day', '2015-01-01', 1),
(2, 'National Heroes Day', '2015-08-31', 1),
(3, 'Eidul Adha', '2015-08-23', 1),
(4, 'All Saints Day', '2015-11-01', 2),
(5, 'Bonifacio Day', '2015-11-30', 1),
(6, 'Christmas Eve', '2015-12-24', 2),
(7, 'Christmas Day', '2015-12-25', 1),
(8, 'Rizal Day', '2015-12-30', 1),
(9, 'New Year''s Eve', '2015-12-31', 2),
(10, 'New Year''s Day 2', '2015-01-02', 1),
(11, 'Chinese New Year', '2015-01-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `incometb`
--

CREATE TABLE IF NOT EXISTS `incometb` (
  `InId` int(11) NOT NULL AUTO_INCREMENT,
  `IStart` date NOT NULL,
  `IEnd` date NOT NULL,
  `Amount` float NOT NULL,
  `IncomeName` text NOT NULL,
  `EmpId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`InId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `incometb`
--

INSERT INTO `incometb` (`InId`, `IStart`, `IEnd`, `Amount`, `IncomeName`, `EmpId`, `Status`) VALUES
(1, '2015-02-01', '2015-02-01', 200, 'Allowance', 5, 1),
(2, '2015-02-01', '2015-02-01', 9.88, 'Night Differential', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mastereductb`
--

CREATE TABLE IF NOT EXISTS `mastereductb` (
  `EducId` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `School` text NOT NULL,
  `Date` date NOT NULL,
  `Course` text NOT NULL,
  PRIMARY KEY (`EducId`),
  KEY `TicketId` (`EmpId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `mastereductb`
--

INSERT INTO `mastereductb` (`EducId`, `EmpId`, `School`, `Date`, `Course`) VALUES
(1, 1, 'Pamantasan ng Lungsod ng Maynila', '2014-11-29', 'a'),
(2, 2, 'Pamantasan ng Lungsod ng Maynila', '2011-11-30', 'asd'),
(3, 3, 'Pamantasan ng Lungsod ng Maynila', '2007-10-29', 'sd'),
(4, 4, 'Adamson University', '2005-10-29', 'asd'),
(5, 5, 'Pamantasan ng Lungsod ng Maynila', '2007-07-28', 'asd'),
(6, 6, 'Adamson University', '2014-10-29', 'sad'),
(7, 7, 'Pamantasan ng Lungsod ng Maynila', '2006-10-29', 'asd'),
(8, 8, 'Pamantasan ng Lungsod ng Maynila', '2007-10-29', 'as'),
(9, 9, 'Adamson University', '2007-11-29', 'ada'),
(10, 10, 'Pamantasan ng Lungsod ng Maynila', '2005-11-29', 'ad'),
(11, 11, 'Pamantasan ng Lungsod ng Maynila', '2011-11-30', 'asd'),
(12, 12, 'Pamantasan ng Lungsod ng Maynila', '2013-10-28', 'sds'),
(13, 13, 'Adamson University', '2014-03-04', 'dgdg'),
(14, 14, 'Adamson University', '2011-02-03', 'dgd'),
(15, 15, 'Pamantasan ng Lungsod ng Maynila', '2015-08-03', 'dsdf'),
(16, 16, 'Adamson University', '2015-08-20', 'gddf'),
(17, 17, 'Pamantasan ng Lungsod ng Maynila', '2015-08-14', 'sfffs'),
(18, 18, 'AdU', '2015-07-31', 'ddsf'),
(19, 19, 'Pamantasan ng Lungsod ng Maynila', '2015-08-04', 'gdfd'),
(20, 20, 'Adamson University', '2015-07-30', 'cdcdc'),
(21, 21, 'Pamantasan ng Lungsod ng Maynila', '2015-08-19', 'dffds'),
(22, 22, 'Pamantasan ng Lungsod ng Maynila', '2015-12-31', 'njhh'),
(23, 23, 'Adamson University', '2015-11-30', 'nnm,'),
(24, 24, 'Adamson University', '2014-12-30', 'ndsdm'),
(25, 25, 'Adamson University', '2015-07-27', 'dnddm'),
(26, 26, 'Pamantasan ng Lungsod ng Maynila', '2011-12-26', 'mdmd'),
(27, 27, 'Adamson University', '2012-11-29', 'ndsmdk'),
(28, 28, 'Adamson University', '2014-08-31', 'sdad'),
(29, 29, 'Adamson University', '2013-12-29', 'dns'),
(30, 30, 'Pamantasan ng Lungsod ng Maynila', '2011-08-29', 'sffs'),
(31, 31, 'Adamson University', '2009-11-26', 'sfggg'),
(32, 32, 'Adamson University', '2011-11-28', 'ndkdm'),
(33, 33, 'Adamson University', '2011-12-30', 'mdnd'),
(34, 34, 'Pamantasan ng Lungsod ng Maynila', '2007-11-29', 'dsdd'),
(35, 35, 'De La Salle College Of Saint Benilide', '2015-08-24', 'SAP Certificate Holder');

-- --------------------------------------------------------

--
-- Table structure for table `masteremptb`
--

CREATE TABLE IF NOT EXISTS `masteremptb` (
  `EmpId` int(11) NOT NULL AUTO_INCREMENT,
  `Status` int(11) NOT NULL,
  `MusId` text NOT NULL,
  `DateHire` date NOT NULL,
  `LastAppraisal` date NOT NULL,
  `AStat` int(11) NOT NULL,
  `SysStat` int(11) NOT NULL,
  PRIMARY KEY (`EmpId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `masteremptb`
--

INSERT INTO `masteremptb` (`EmpId`, `Status`, `MusId`, `DateHire`, `LastAppraisal`, `AStat`, `SysStat`) VALUES
(0, 1, 'ADMIN', '0000-00-00', '0000-00-00', 3, 0),
(1, 1, 'EBIZ002', '0000-00-00', '0000-00-00', 0, 0),
(2, 1, 'EBIZ003', '0000-00-00', '0000-00-00', 0, 0),
(3, 1, 'EBIZ004', '0000-00-00', '0000-00-00', 0, 0),
(4, 1, 'EBIZ005', '0000-00-00', '0000-00-00', 0, 0),
(5, 1, 'EBIZ006', '2014-08-10', '2015-02-01', 1, 1),
(6, 1, 'EBIZ007', '0000-00-00', '0000-00-00', 0, 0),
(7, 1, 'EBIZ008', '0000-00-00', '0000-00-00', 0, 0),
(8, 1, 'EBIZ009', '2015-08-27', '2015-08-27', 0, 0),
(9, 1, 'EBIZ010', '2015-08-27', '2015-08-27', 0, 0),
(10, 1, 'EBIZ011', '2014-08-15', '2015-02-01', 1, 1),
(11, 1, 'EBIZ012', '2015-08-27', '2015-08-27', 0, 0),
(12, 1, 'EBIZ013', '2014-08-27', '2015-02-27', 1, 1),
(13, 1, 'EBIZ014', '2014-08-23', '2015-02-01', 1, 1),
(14, 1, 'EBIZ015', '2014-08-20', '2015-08-28', 0, 0),
(15, 1, 'EBIZ016', '2015-08-27', '2015-08-27', 0, 0),
(16, 1, 'EBIZ017', '2015-08-27', '2015-08-27', 0, 0),
(17, 1, 'EBIZ018', '2015-08-27', '2015-08-27', 0, 0),
(18, 1, 'EBIZ019', '2015-08-27', '2015-08-27', 0, 0),
(19, 1, 'EBIZ020', '2015-08-27', '2015-08-27', 0, 0),
(20, 1, 'KKI001', '2015-08-27', '2015-08-27', 0, 0),
(21, 1, 'KKI002', '2015-08-27', '2015-08-27', 0, 0),
(22, 1, 'KKI003', '2015-08-27', '2015-08-27', 0, 0),
(23, 1, 'KKI004', '2015-08-27', '2015-08-27', 0, 0),
(24, 1, 'KKI005', '2015-08-27', '2015-08-27', 0, 0),
(25, 1, 'KKI006', '2015-08-27', '2015-08-27', 0, 0),
(26, 1, 'KKI007', '2015-08-27', '2015-08-27', 0, 0),
(27, 1, 'KKI008', '2015-08-27', '2015-08-27', 0, 0),
(28, 1, 'KKI009', '2015-08-27', '2015-08-27', 0, 0),
(29, 1, 'KKI010', '2015-08-27', '2015-08-27', 0, 0),
(30, 1, 'OPTO001', '2015-08-27', '2015-08-27', 0, 0),
(31, 1, 'OPTO002', '2015-08-27', '2015-08-27', 0, 0),
(32, 1, 'OPTO003', '2015-08-27', '2015-08-27', 0, 0),
(33, 1, 'OPTO004', '2015-08-27', '2015-08-27', 0, 0),
(34, 1, 'OPTO005', '2015-08-27', '2015-08-27', 0, 0),
(35, 1, 'EBIZ021', '2015-08-28', '2015-08-28', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `masterpersonaltb`
--

CREATE TABLE IF NOT EXISTS `masterpersonaltb` (
  `InfoId` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Address` text NOT NULL,
  `CellNum` bigint(11) NOT NULL,
  `Mstatus` text NOT NULL,
  `Email` text NOT NULL,
  `Dob` date NOT NULL,
  `Company` text NOT NULL,
  `Pagibig` varchar(20) NOT NULL,
  `SSS` varchar(20) NOT NULL,
  `Tin` varchar(20) NOT NULL,
  `Philhealth` varchar(20) NOT NULL,
  `PosId` int(11) NOT NULL,
  `Dependency` int(11) NOT NULL,
  PRIMARY KEY (`InfoId`),
  KEY `TicketId` (`EmpId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `masterpersonaltb`
--

INSERT INTO `masterpersonaltb` (`InfoId`, `EmpId`, `Name`, `Address`, `CellNum`, `Mstatus`, `Email`, `Dob`, `Company`, `Pagibig`, `SSS`, `Tin`, `Philhealth`, `PosId`, `Dependency`) VALUES
(1, 0, 'ADMIN', '00', 0, '00', '00', '2015-08-02', '', '00', '00', '00', '00', 9, 0),
(2, 1, 'Nathaniel Marquez', '900 San Marcelino', 905702796, 'Single', 'nmarquez@ebizolution.com', '2013-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 10, 0),
(3, 2, 'Rodolfo Gobrin', '900 San Marcelino', 905702796, 'Married', 'rgobrin@ebizolution.com', '2013-11-30', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 9, 0),
(4, 3, 'Karen Mae Delmendo', 'Makati City', 905702796, 'Single', 'kdelmendo@ebizolution.com', '2013-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 6, 0),
(5, 4, 'Remedios Villanueva', '900 San Marcelino', 905702796, 'Married', 'rvillanueva@ebizolution.com', '2007-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 2, 0),
(6, 5, 'Jesusa Benecio', '900 San Marcelino', 905702796, 'Married', 'jbenecio@ebizolution.com', '2012-09-30', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 2, 0),
(7, 6, 'Arnold Alegre', '900 San Marcelino', 905702796, 'Married', 'aalegre@ebizolution.com', '2001-08-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 7, 0),
(8, 7, 'Efraem Velasco', 'Makati City', 905702796, 'Single', 'evelasco@ebizolution.com', '2003-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(9, 8, 'Diana Jane Marquez', '108 TopLand Ave. Golden Gate Subd. Talon 3 Las Pinas City', 9328823252, 'Single', 'dmarquez@ebizolution.com', '1981-11-18', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 13, 0),
(10, 9, 'Adrian Garcia', '900 San Marcelino', 905702796, 'Single', 'agarcia@ebizolution.com', '2005-09-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(11, 10, 'Melvin De Vera', '900 San Marcelino', 905702796, 'Single', 'mdevera@ebizolution.com', '2009-11-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(12, 11, 'Regel Urbano', '900 San Marcelino', 905702796, 'Single', 'rurbano@ebizolution.com', '2006-11-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 6, 0),
(13, 12, 'Joselito Panahon', '2425 Cuenca St. A. Arnaiz Ave. Pasay City', 9228449631, 'Single', 'jpanahon@ebizolution.com', '1983-07-05', 'eBiZolution', '121034612400', '3387444276', '261483881', '00000000000', 3, 0),
(14, 13, 'Mark Dominic Daval-Santos', 'Manila City', 905702796, 'Single', 'msantos@ebizolution.com', '2016-03-05', 'eBiZolution', '00000000000', '00000000000', '00000000000', '00000000000', 6, 0),
(15, 14, 'Al-Jhay Kevin Tadeo', 'Makati City', 0, 'Married', 'atadeo@ebizolution.com', '2013-02-25', 'eBiZolution', '11111111111', '90909090909', '00000000000', '00000000000', 3, 0),
(16, 15, 'Rochelle De Guzman', 'Makati City', 0, 'Single', 'rdeguzman@ebizolution.com', '2015-02-03', 'eBiZolution', '00000000000', '33144994850', '00000000000', '11111111111', 12, 0),
(17, 16, 'Babay Akmad', 'Makati City', 905702796, 'Single', 'bakmad@ebizolution.com', '2015-07-30', 'eBiZolution', '00000000000', '12341212312', '23432525345', '00000000000', 3, 0),
(18, 17, 'JohnMar Pipino', 'Makati City', 905702796, 'Single', 'jpipino@ebizolution.com', '2015-08-27', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(19, 18, 'Christian Jay Balgos', 'Makati City', 905702796, 'Single', 'cbalgos@ebizolution.com', '2015-07-31', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(20, 19, 'Julius Cesar Arabe', 'Makati City', 905702796, 'Single', 'jarabe@ebizolution.com', '2015-08-07', 'eBiZolution', '00000000000', '90909090909', '00000000000', '00000000000', 3, 0),
(21, 20, 'Maria Christina Marquez', 'Makati City', 905702796, 'Single', 'cmarquez@kki.com', '2015-08-19', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 14, 0),
(22, 21, 'Rhoda Zaguirre', 'Makati City', 905702796, 'Single', 'rzaguirre@kki.com', '2015-08-06', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(23, 22, 'Maria Evangeline Baranquiel', 'Makati City', 905702796, 'Single', 'ebaranquiel@kki.com', '2015-08-04', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(24, 23, 'Maria Erica Simon', 'Makati City', 905702796, 'Single', 'esimon@kki.com', '2015-07-30', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(25, 24, 'Jonathan Zaguirre', 'Makati City', 905702796, 'Single', 'jzaguirre@kki.com', '2015-12-30', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(26, 25, 'Dennis Simon', 'Makati City', 905702796, 'Single', 'dsimon@kki.com', '2015-08-07', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(27, 26, 'Jymbee Zaguirre', 'Makati City', 905702796, 'Single', 'jyzaguirre@kki.com', '2012-12-30', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(28, 27, 'Jeff Ventura', 'Cavite City', 905702796, 'Single', 'jventura@kki.com', '2008-11-24', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(29, 28, 'Jackielyn Cano', 'Cavite City', 905702796, 'Married', 'jcano@kki.com', '2013-11-22', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(30, 29, 'Angel Nevarez', 'Makati City', 905702796, 'Single', 'anevarez@kki.com', '2015-09-09', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, 0),
(31, 30, 'Ponciano Tugonon Jr. ', 'Makati City', 0, 'Single', 'ptugonon@kki.com', '2012-11-28', 'Optomization', '00000000000', '33144994850', '00000000000', '00000000000', 5, 0),
(32, 31, 'Ricky Pardillo', 'Makati City', 905702796, 'Married', 'rpardillo@kki.com', '2012-11-28', 'Optomization', '00000000000', '90909090909', '00000000000', '00000000000', 5, 0),
(33, 32, 'Mark Ronald Epino', 'Makati City', 905702796, 'Married', 'mepino@kki.com', '2010-12-02', 'Optomization', '00000000000', '33144994850', '00000000000', '00000000000', 5, 0),
(34, 33, 'Niko Franz Gepiga', 'Makati City', 905702796, 'Married', 'ngepiga@kki.com', '1995-11-30', 'Optomization', '00000000000', '33144994850', '00000000000', '00000000000', 5, 0),
(35, 34, 'Venice Jeremaiah Munar', 'Cavite City', 905702796, 'Single', 'vmunar@kki.com', '1973-07-29', 'Optomization', '00000000000', '33144994850', '00000000000', '11111111111', 5, 0),
(36, 35, 'Carl Domenique Millan Gabuelo', '21 Street Sto Nino Pque City', 9267021992, 'Single', 'blckwolf@gmail.com', '1992-07-19', 'eBiZolution', '00000000000', '00000000000', '00000000000', '00000000000', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `masteruploadtb`
--

CREATE TABLE IF NOT EXISTS `masteruploadtb` (
  `UploadId` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `Location` text NOT NULL,
  `Name` text NOT NULL,
  PRIMARY KEY (`UploadId`),
  KEY `TicketId` (`EmpId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `masteruploadtb`
--

INSERT INTO `masteruploadtb` (`UploadId`, `EmpId`, `Location`, `Name`) VALUES
(1, 1, '../Uploads/Pictures/0a7b1b9.jpg', '0a7b1b9.jpg'),
(2, 2, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(3, 3, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(4, 4, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(5, 5, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(6, 6, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(7, 7, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(8, 8, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(9, 9, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(10, 10, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(11, 11, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(13, 12, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(14, 13, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(15, 14, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(16, 15, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(17, 16, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(18, 17, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(19, 18, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(20, 19, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(21, 20, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(22, 21, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(23, 22, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(24, 23, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(25, 24, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(26, 25, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(27, 26, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(28, 27, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(29, 28, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(30, 29, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(31, 30, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(32, 31, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(33, 32, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(34, 33, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(35, 34, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(36, 35, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png');

-- --------------------------------------------------------

--
-- Table structure for table `masterworktb`
--

CREATE TABLE IF NOT EXISTS `masterworktb` (
  `WorkId` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `Employer` text NOT NULL,
  `Address` text NOT NULL,
  `DateStart` date NOT NULL,
  `DateEnd` date NOT NULL,
  `Position` text NOT NULL,
  `Reason` text NOT NULL,
  PRIMARY KEY (`WorkId`),
  KEY `TicketId` (`EmpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `masterworktb`
--


-- --------------------------------------------------------

--
-- Table structure for table `mustardtb`
--

CREATE TABLE IF NOT EXISTS `mustardtb` (
  `InfoId` int(11) NOT NULL AUTO_INCREMENT,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `MusId` text NOT NULL,
  `In1` time NOT NULL,
  `Out1` time NOT NULL,
  `In2` time DEFAULT NULL,
  `Out2` time DEFAULT NULL,
  `In3` time DEFAULT NULL,
  `Out3` time DEFAULT NULL,
  `In4` time DEFAULT NULL,
  `Out4` time DEFAULT NULL,
  `In5` time DEFAULT NULL,
  `Out5` time DEFAULT NULL,
  `Date` date NOT NULL,
  `HrsWork` float NOT NULL,
  `HrsOT` float NOT NULL,
  `HrsUT` float NOT NULL,
  PRIMARY KEY (`InfoId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=152 ;

--
-- Dumping data for table `mustardtb`
--

INSERT INTO `mustardtb` (`InfoId`, `StartDate`, `EndDate`, `MusId`, `In1`, `Out1`, `In2`, `Out2`, `In3`, `Out3`, `In4`, `Out4`, `In5`, `Out5`, `Date`, `HrsWork`, `HrsOT`, `HrsUT`) VALUES
(1, '2015-02-01', '2015-02-15', 'EBIZ002', '12:31:00', '03:11:00', '06:03:00', '06:46:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 1.87, 1.77, 7.13),
(2, '2015-02-01', '2015-02-15', 'EBIZ002', '03:32:00', '08:28:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 1.72, 3.47, 7.28),
(3, '2015-02-01', '2015-02-15', 'EBIZ002', '07:54:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 0, 0, 9),
(4, '2015-02-01', '2015-02-15', 'EBIZ002', '09:53:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 0, 0, 9),
(5, '2015-02-01', '2015-02-15', 'EBIZ002', '01:13:00', '01:55:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 0.95, 0, 8.05),
(6, '2015-02-01', '2015-02-15', 'EBIZ002', '11:38:00', '04:39:00', '05:42:00', '06:09:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 4.57, 1.15, 4.43),
(7, '2015-02-01', '2015-02-15', 'EBIZ002', '10:32:00', '11:51:00', '05:38:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 0, 0, 9),
(8, '2015-02-01', '2015-02-15', 'EBIZ002', '01:38:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 0, 0, 9),
(9, '2015-02-01', '2015-02-15', 'EBIZ002', '01:36:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-14', 0, 0, 0),
(10, '2015-02-01', '2015-02-15', 'EBIZ003', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '0000-00-00', 0, 0, 0),
(11, '2015-02-01', '2015-02-15', 'EBIZ004', '09:28:00', '05:25:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 7.78, 0.42, 1.22),
(12, '2015-02-01', '2015-02-15', 'EBIZ004', '09:52:00', '10:39:00', '05:15:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 0, 0, 9),
(13, '2015-02-01', '2015-02-15', 'EBIZ004', '01:02:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 4.22, 0.05, 4.78),
(14, '2015-02-01', '2015-02-15', 'EBIZ004', '08:56:00', '12:12:00', '12:51:00', '05:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 7.67, 0.1, 1.33),
(15, '2015-02-01', '2015-02-15', 'EBIZ005', '08:28:00', '09:05:00', '02:12:00', '02:32:00', '03:43:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 0.02, 0, 8.98),
(16, '2015-02-01', '2015-02-15', 'EBIZ005', '08:15:00', '12:06:00', '05:25:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 0, 0, 9),
(17, '2015-02-01', '2015-02-15', 'EBIZ005', '08:09:00', '10:09:00', '11:55:00', '03:03:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 3.28, 0, 5.72),
(18, '2015-02-01', '2015-02-15', 'EBIZ005', '08:17:00', '05:07:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 8.97, 0.12, 0.03),
(19, '2015-02-01', '2015-02-15', 'EBIZ005', '08:15:00', '11:22:00', '12:04:00', '12:53:00', '05:09:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 0, 0, 9),
(20, '2015-02-01', '2015-02-15', 'EBIZ005', '08:17:00', '01:28:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 5.43, 0, 3.57),
(21, '2015-02-01', '2015-02-15', 'EBIZ005', '08:12:00', '11:57:00', '05:02:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 0, 0, 9),
(22, '2015-02-01', '2015-02-15', 'EBIZ005', '08:16:00', '09:35:00', '12:00:00', '05:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 6.57, 0.1, 2.43),
(23, '2015-02-01', '2015-02-15', 'EBIZ005', '08:25:00', '11:57:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 0, 0, 9),
(24, '2015-02-01', '2015-02-15', 'EBIZ005', '08:11:00', '12:00:00', '05:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0, 0, 9),
(25, '2015-02-01', '2015-02-15', 'EBIZ006', '12:33:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-01', 8, 0, 0),
(26, '2015-02-01', '2015-02-15', 'EBIZ006', '09:28:00', '05:24:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 8, 2.17, 0),
(27, '2015-02-01', '2015-02-15', 'EBIZ006', '11:05:00', '07:10:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 8, 6.25, 0),
(28, '2015-02-01', '2015-02-15', 'EBIZ006', '09:47:00', '11:17:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 8, 4.92, 0),
(29, '2015-02-01', '2015-02-15', 'EBIZ006', '09:45:00', '10:41:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 4, 0, 4),
(30, '2015-02-01', '2015-02-15', 'EBIZ006', '09:47:00', '07:14:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 4, 0, 4),
(31, '2015-02-01', '2015-02-15', 'EBIZ006', '12:54:00', '11:16:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 0, 0, 8),
(32, '2015-02-01', '2015-02-15', 'EBIZ006', '10:35:00', '05:12:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 8, 0, 0),
(33, '2015-02-01', '2015-02-15', 'EBIZ006', '10:16:00', '05:23:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 8, 0, 1.83),
(34, '2015-02-01', '2015-02-15', 'EBIZ006', '10:00:00', '05:20:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 8, 0, 0),
(35, '2015-02-01', '2015-02-15', 'EBIZ006', '10:28:00', '05:28:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0, 0, 0),
(36, '2015-02-01', '2015-02-15', 'EBIZ007', '12:33:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-01', 0, 0, 0),
(37, '2015-02-01', '2015-02-15', 'EBIZ007', '07:53:00', '05:13:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 9, 0.33, 0),
(38, '2015-02-01', '2015-02-15', 'EBIZ007', '07:53:00', '07:10:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 9, 2.28, 0),
(39, '2015-02-01', '2015-02-15', 'EBIZ007', '08:04:00', '11:16:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 9, 6.27, 0),
(40, '2015-02-01', '2015-02-15', 'EBIZ007', '08:46:00', '12:44:00', '10:41:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 0, 0, 9),
(41, '2015-02-01', '2015-02-15', 'EBIZ007', '10:51:00', '12:23:00', '05:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 0, 0, 9),
(42, '2015-02-01', '2015-02-15', 'EBIZ007', '10:33:00', '04:56:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 6.63, 0, 2.37),
(43, '2015-02-01', '2015-02-15', 'EBIZ007', '05:12:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 0, 0, 9),
(44, '2015-02-01', '2015-02-15', 'EBIZ007', '07:51:00', '10:10:00', '03:14:00', '05:19:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 3.93, 0.47, 5.07),
(45, '2015-02-01', '2015-02-15', 'EBIZ007', '09:50:00', '11:54:00', '12:25:00', '05:26:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 6.9, 0.43, 2.1),
(46, '2015-02-01', '2015-02-15', 'EBIZ008', '08:23:00', '10:29:00', '01:31:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 0, 0, 9),
(47, '2015-02-01', '2015-02-15', 'EBIZ008', '08:55:00', '09:24:00', '12:18:00', '05:07:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 5.43, 0.12, 3.57),
(48, '2015-02-01', '2015-02-15', 'EBIZ008', '01:09:00', '01:32:00', '01:53:00', '03:10:00', '05:27:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 0, 0, 9),
(49, '2015-02-01', '2015-02-15', 'EBIZ008', '08:40:00', '09:36:00', '10:11:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 0.6, 0, 8.4),
(50, '2015-02-01', '2015-02-15', 'EBIZ008', '08:09:00', '11:20:00', '03:03:00', '05:05:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 5.28, 0.08, 3.72),
(51, '2015-02-01', '2015-02-15', 'EBIZ008', '08:11:00', '11:55:00', '05:01:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 0, 0, 9),
(52, '2015-02-01', '2015-02-15', 'EBIZ008', '08:49:00', '12:12:00', '02:21:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 1.48, 0, 7.52),
(53, '2015-02-01', '2015-02-15', 'EBIZ009', '08:41:00', '09:30:00', '11:20:00', '12:03:00', '01:31:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 0.32, 0, 8.68),
(54, '2015-02-01', '2015-02-15', 'EBIZ009', '08:16:00', '09:22:00', '11:39:00', '03:13:00', '04:03:00', '05:02:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 5.87, 0.03, 3.13),
(55, '2015-02-01', '2015-02-15', 'EBIZ009', '09:22:00', '10:28:00', '11:52:00', '02:31:00', '03:41:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 2.83, 0, 6.17),
(56, '2015-02-01', '2015-02-15', 'EBIZ009', '09:06:00', '09:45:00', '05:07:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 0, 0, 9),
(57, '2015-02-01', '2015-02-15', 'EBIZ009', '08:54:00', '03:12:00', '03:52:00', '05:05:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 7.68, 0.08, 1.32),
(58, '2015-02-01', '2015-02-15', 'EBIZ009', '09:58:00', '11:04:00', '12:05:00', '02:14:00', '03:41:00', '05:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 4.82, 0, 4.18),
(59, '2015-02-01', '2015-02-15', 'EBIZ009', '08:18:00', '11:08:00', '01:09:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 1.07, 0, 7.93),
(60, '2015-02-01', '2015-02-15', 'EBIZ009', '08:23:00', '08:56:00', '12:45:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 5.05, 0.05, 3.95),
(61, '2015-02-01', '2015-02-15', 'EBIZ009', '11:19:00', '03:17:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 4.22, 0, 4.78),
(62, '2015-02-01', '2015-02-15', 'EBIZ009', '08:41:00', '08:59:00', '02:29:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0, 0, 9),
(63, '2015-01-01', '2015-01-15', 'EBIZ010', '08:06:00', '09:04:00', '11:59:00', '12:23:00', '12:35:00', '02:00:00', '04:01:00', '05:06:00', '00:00:00', '00:00:00', '2015-01-01', 0, 0, 8),
(64, '2015-01-01', '2015-01-15', 'EBIZ010', '08:24:00', '08:33:00', '08:39:00', '09:24:00', '09:50:00', '12:51:00', '02:31:00', '04:54:00', '05:02:00', '00:00:00', '2015-01-02', 0, 0, 8),
(65, '2015-01-01', '2015-01-15', 'EBIZ010', '08:14:00', '08:21:00', '09:26:00', '01:16:00', '03:43:00', '05:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-05', 8, 0, 0),
(66, '2015-01-01', '2015-01-15', 'EBIZ010', '08:10:00', '12:11:00', '01:27:00', '05:05:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-06', 8, 0, 0),
(67, '2015-01-01', '2015-01-15', 'EBIZ010', '08:27:00', '04:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-07', 8, 0, 0),
(68, '2015-01-01', '2015-01-15', 'EBIZ010', '08:32:00', '11:08:00', '01:12:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-08', 8, 0, 0),
(69, '2015-01-01', '2015-01-15', 'EBIZ010', '08:08:00', '03:19:00', '04:12:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-09', 8, 0, 0),
(70, '2015-01-01', '2015-01-15', 'EBIZ010', '08:14:00', '08:24:00', '09:31:00', '01:15:00', '03:27:00', '04:16:00', '05:02:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-12', 8, 0, 0),
(71, '2015-01-01', '2015-01-15', 'EBIZ010', '08:12:00', '08:24:00', '01:53:00', '04:13:00', '05:02:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-13', 8, 0, 0),
(72, '2015-02-01', '2015-02-15', 'EBIZ011', '07:22:00', '08:13:00', '11:39:00', '01:50:00', '05:01:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 0, 0, 9),
(73, '2015-02-01', '2015-02-15', 'EBIZ011', '07:19:00', '10:28:00', '05:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 0, 0, 9),
(74, '2015-02-01', '2015-02-15', 'EBIZ011', '07:13:00', '12:02:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 4.03, 0.78, 4.97),
(75, '2015-02-01', '2015-02-15', 'EBIZ011', '07:18:00', '10:55:00', '12:04:00', '01:24:00', '05:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 0.65, 0.7, 8.35),
(76, '2015-02-01', '2015-02-15', 'EBIZ011', '07:18:00', '12:01:00', '02:58:00', '04:59:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 6.03, 0.7, 2.97),
(77, '2015-02-01', '2015-02-15', 'EBIZ011', '07:11:00', '05:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 9, 0.82, 0),
(78, '2015-02-01', '2015-02-15', 'EBIZ011', '07:16:00', '11:37:00', '11:55:00', '05:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 8.7, 0.73, 0.3),
(79, '2015-02-01', '2015-02-15', 'EBIZ011', '07:15:00', '08:59:00', '10:54:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0, 0, 9),
(80, '2015-02-01', '2015-02-15', 'EBIZ012', '08:12:00', '05:07:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 8.92, 0.12, 0),
(81, '2015-02-01', '2015-02-15', 'EBIZ012', '08:00:00', '09:08:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 1.13, 0, 7.87),
(82, '2015-02-01', '2015-02-15', 'EBIZ012', '08:04:00', '05:33:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 9, 0.55, 0),
(83, '2015-02-01', '2015-02-15', 'EBIZ012', '07:31:00', '09:32:00', '11:58:00', '05:09:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 6.57, 0.63, 2.43),
(84, '2015-02-01', '2015-02-15', 'EBIZ012', '08:28:00', '05:07:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 8.78, 0.12, 0.22),
(85, '2015-02-01', '2015-02-15', 'EBIZ012', '07:45:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 9, 0.3, 0),
(86, '2015-02-01', '2015-02-15', 'EBIZ012', '07:51:00', '11:13:00', '11:44:00', '05:23:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 8.48, 0.53, 0.52),
(87, '2015-02-01', '2015-02-15', 'EBIZ012', '07:59:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 9, 0.07, 0),
(88, '2015-02-01', '2015-02-15', 'EBIZ013', '01:54:00', '05:14:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 3.35, 0.23, 5.65),
(89, '2015-02-01', '2015-02-15', 'EBIZ013', '09:42:00', '05:26:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 7.55, 0.43, 1.45),
(90, '2015-02-01', '2015-02-15', 'EBIZ013', '10:17:00', '05:13:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 6.97, 0.22, 2.03),
(91, '2015-02-01', '2015-02-15', 'EBIZ013', '10:05:00', '01:28:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 3.63, 0, 5.37),
(92, '2015-02-01', '2015-02-15', 'EBIZ013', '10:05:00', '01:57:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 4.12, 0, 4.88),
(93, '2015-02-01', '2015-02-15', 'EBIZ014', '08:02:00', '09:22:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 1.37, 0, 7.63),
(94, '2015-02-01', '2015-02-15', 'EBIZ014', '07:54:00', '09:16:00', '09:29:00', '09:49:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 1.6, 0.1, 7.4),
(95, '2015-02-01', '2015-02-15', 'EBIZ014', '08:44:00', '10:25:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 1.93, 0, 7.07),
(96, '2015-02-01', '2015-02-15', 'EBIZ014', '07:42:00', '09:18:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 1.3, 0.3, 7.7),
(97, '2015-02-01', '2015-02-15', 'EBIZ014', '08:10:00', '09:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 1.1, 0, 7.9),
(98, '2015-02-01', '2015-02-15', 'EBIZ014', '08:08:00', '09:35:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 1.58, 0, 7.42),
(99, '2015-02-01', '2015-02-15', 'EBIZ014', '08:07:00', '09:24:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 1.4, 0, 7.6),
(100, '2015-02-01', '2015-02-15', 'EBIZ014', '08:37:00', '09:16:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 0.9, 0, 8.1),
(101, '2015-02-01', '2015-02-15', 'EBIZ014', '07:59:00', '08:59:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0.98, 0.02, 8.02),
(102, '2015-02-01', '2015-02-15', 'EBIZ015', '08:49:00', '09:57:00', '12:12:00', '01:37:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 2.8, 0, 6.2),
(103, '2015-02-01', '2015-02-15', 'EBIZ015', '09:04:00', '02:38:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 5.82, 0, 3.18),
(104, '2015-02-01', '2015-02-15', 'EBIZ015', '08:54:00', '02:39:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 6, 0, 3),
(105, '2015-02-01', '2015-02-15', 'EBIZ015', '08:35:00', '01:28:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 5.13, 0, 3.87),
(106, '2015-02-01', '2015-02-15', 'EBIZ015', '09:32:00', '01:57:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 4.67, 0, 4.33),
(107, '2015-02-01', '2015-02-15', 'EBIZ015', '09:00:00', '01:13:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 4.47, 0, 4.53),
(108, '2015-02-01', '2015-02-15', 'EBIZ015', '09:31:00', '02:04:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 4.8, 0, 4.2),
(109, '2015-02-01', '2015-02-15', 'EBIZ015', '03:13:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0, 0, 9),
(110, '2015-02-01', '2015-02-15', 'EBIZ016', '08:25:00', '09:04:00', '05:20:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 0, 0, 9),
(111, '2015-02-01', '2015-02-15', 'EBIZ016', '08:21:00', '09:37:00', '01:59:00', '02:28:00', '05:04:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 0, 0, 9),
(112, '2015-02-01', '2015-02-15', 'EBIZ016', '11:47:00', '01:17:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 1.75, 0, 7.25),
(113, '2015-02-01', '2015-02-15', 'EBIZ016', '08:36:00', '09:35:00', '11:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 0, 0, 9),
(114, '2015-02-01', '2015-02-15', 'EBIZ016', '08:55:00', '12:59:00', '04:11:00', '05:08:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 5.13, 0.13, 3.87),
(115, '2015-02-01', '2015-02-15', 'EBIZ016', '08:14:00', '05:02:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 8.8, 0.03, 0),
(116, '2015-02-01', '2015-02-15', 'EBIZ016', '08:12:00', '08:44:00', '01:08:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 0, 0, 9),
(117, '2015-02-01', '2015-02-15', 'EBIZ016', '08:14:00', '09:45:00', '01:18:00', '01:52:00', '05:21:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 0, 0, 9),
(118, '2015-02-01', '2015-02-15', 'EBIZ016', '08:06:00', '09:43:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 1.72, 0, 7.28),
(119, '2015-02-01', '2015-02-15', 'EBIZ016', '08:19:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 8.93, 0.05, 0.07),
(120, '2015-02-01', '2015-02-15', 'EBIZ017', '08:15:00', '08:49:00', '05:24:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 0, 0, 9),
(121, '2015-02-01', '2015-02-15', 'EBIZ017', '08:17:00', '09:30:00', '07:53:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-03', 0, 0, 9),
(122, '2015-02-01', '2015-02-15', 'EBIZ017', '08:19:00', '07:36:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 8.93, 2.6, 0.07),
(123, '2015-02-01', '2015-02-15', 'EBIZ017', '09:06:00', '05:07:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 8.15, 0.12, 0.85),
(124, '2015-02-01', '2015-02-15', 'EBIZ017', '08:16:00', '12:08:00', '12:59:00', '07:14:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 8.13, 2.23, 0.87),
(125, '2015-02-01', '2015-02-15', 'EBIZ017', '09:40:00', '10:13:00', '11:32:00', '12:31:00', '05:18:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 0, 0, 9),
(126, '2015-02-01', '2015-02-15', 'EBIZ017', '08:42:00', '11:40:00', '07:29:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 0, 0, 9),
(127, '2015-02-01', '2015-02-15', 'EBIZ017', '11:57:00', '12:11:00', '05:23:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 0, 0, 9),
(128, '2015-02-01', '2015-02-15', 'EBIZ017', '08:43:00', '05:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 8.53, 0, 0.47),
(129, '2015-02-01', '2015-02-15', 'EBIZ017', '08:25:00', '11:38:00', '11:52:00', '12:48:00', '01:12:00', '03:39:00', '05:25:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 5.08, 0, 3.92),
(130, '2015-02-01', '2015-02-15', 'EBIZ018', '09:28:00', '12:06:00', '05:04:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 0, 0, 9),
(131, '2015-02-01', '2015-02-15', 'EBIZ018', '08:04:00', '11:31:00', '03:19:00', '05:05:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 5.2, 0.08, 3.8),
(132, '2015-02-01', '2015-02-15', 'EBIZ018', '09:18:00', '10:28:00', '05:00:00', '05:11:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 1.42, 0.18, 7.58),
(133, '2015-02-01', '2015-02-15', 'EBIZ018', '08:07:00', '11:52:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 3.87, 0, 5.13),
(134, '2015-02-01', '2015-02-15', 'EBIZ018', '08:25:00', '11:51:00', '05:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 0, 0, 9),
(135, '2015-02-01', '2015-02-15', 'EBIZ019', '08:10:00', '08:17:00', '10:25:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-04', 0, 0, 9),
(136, '2015-02-01', '2015-02-15', 'EBIZ019', '08:30:00', '09:19:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-05', 1.07, 0, 7.93),
(137, '2015-02-01', '2015-02-15', 'EBIZ019', '08:26:00', '09:06:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-06', 0.92, 0, 8.08),
(138, '2015-02-01', '2015-02-15', 'EBIZ019', '08:04:00', '09:35:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-09', 1.58, 0, 7.42),
(139, '2015-02-01', '2015-02-15', 'EBIZ019', '08:18:00', '09:24:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 1.35, 0, 7.65),
(140, '2015-02-01', '2015-02-15', 'EBIZ019', '08:22:00', '08:54:00', '09:15:00', '09:22:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 0.9, 0, 8.1),
(141, '2015-02-01', '2015-02-15', 'EBIZ019', '08:26:00', '09:16:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 1.08, 0, 7.92),
(142, '2015-02-01', '2015-02-15', 'EBIZ019', '08:29:00', '09:01:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0.78, 0, 8.22),
(143, '2015-02-01', '2015-02-15', 'EBIZ020', '08:08:00', '09:22:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-02', 1.37, 0, 7.63),
(144, '2015-02-01', '2015-02-15', 'EBIZ020', '08:28:00', '05:01:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-10', 8.78, 0.02, 0.22),
(145, '2015-02-01', '2015-02-15', 'EBIZ020', '07:46:00', '09:23:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 1.38, 0.23, 7.62),
(146, '2015-02-01', '2015-02-15', 'EBIZ020', '08:13:00', '09:44:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-12', 1.73, 0, 7.27),
(147, '2015-02-01', '2015-02-15', 'EBIZ020', '08:08:00', '12:36:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 0.15, 0, 8.85),
(148, '2015-02-01', '2015-02-15', 'EBIZ021', '12:02:00', '05:04:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-11', 5.22, 0.07, 3.78),
(149, '2015-02-01', '2015-02-15', 'EBIZ021', '07:59:00', '10:30:00', '12:15:00', '05:03:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-02-13', 7.25, 0.07, 1.75),
(150, '2015-01-01', '2015-01-15', 'EBIZ010', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-14', 8, 0, 0),
(151, '2015-01-01', '2015-01-15', 'EBIZ010', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '2015-01-15', 0, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `pagibigtb`
--

CREATE TABLE IF NOT EXISTS `pagibigtb` (
  `RangeMin` decimal(6,2) DEFAULT NULL,
  `RangeMax` decimal(9,2) DEFAULT NULL,
  `Perce` decimal(2,2) DEFAULT NULL,
  `Record` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pagibigtb`
--

INSERT INTO `pagibigtb` (`RangeMin`, `RangeMax`, `Perce`, `Record`) VALUES
('0.00', '4999.99', '0.01', 1),
('5000.00', '9999999.99', '0.02', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payrolldatatb`
--

CREATE TABLE IF NOT EXISTS `payrolldatatb` (
  `PId` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `PStart` date NOT NULL,
  `PEnd` date NOT NULL,
  `Gross` float NOT NULL,
  `OT` float NOT NULL,
  `UT` float NOT NULL,
  `tax` float NOT NULL,
  `sss` float NOT NULL,
  `philhealth` float NOT NULL,
  `pagibig` float NOT NULL,
  `regdeduct` float NOT NULL,
  `TotalGross` float NOT NULL,
  `TotalDeduct` float NOT NULL,
  `Net` float NOT NULL,
  PRIMARY KEY (`PId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `payrolldatatb`
--

INSERT INTO `payrolldatatb` (`PId`, `EmpId`, `PStart`, `PEnd`, `Gross`, `OT`, `UT`, `tax`, `sss`, `philhealth`, `pagibig`, `regdeduct`, `TotalGross`, `TotalDeduct`, `Net`) VALUES
(1, 9, '2015-01-01', '2015-01-15', 7250, 0, 0, 0, 263.4, 87.5, 50, 400.9, 7250, 923.93, 6326.07);

-- --------------------------------------------------------

--
-- Table structure for table `payrolltb`
--

CREATE TABLE IF NOT EXISTS `payrolltb` (
  `PId` int(11) NOT NULL AUTO_INCREMENT,
  `PStart` date NOT NULL,
  `PEnd` date NOT NULL,
  `EmpId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`PId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `payrolltb`
--

INSERT INTO `payrolltb` (`PId`, `PStart`, `PEnd`, `EmpId`, `Status`) VALUES
(1, '2015-02-01', '2015-02-15', 5, 0),
(2, '2015-01-01', '2015-01-15', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `philhealthtb`
--

CREATE TABLE IF NOT EXISTS `philhealthtb` (
  `RangeMin` int(5) DEFAULT NULL,
  `EE` decimal(5,2) DEFAULT NULL,
  `ER` decimal(5,2) DEFAULT NULL,
  `RangeMax` decimal(10,2) DEFAULT NULL,
  `Record` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `philhealthtb`
--

INSERT INTO `philhealthtb` (`RangeMin`, `EE`, `ER`, `RangeMax`, `Record`) VALUES
(0, '0.00', '0.00', '7999.99', 1),
(8000, '100.00', '100.00', '8999.99', 2),
(9000, '112.50', '112.50', '9999.99', 3),
(10000, '125.00', '125.00', '10999.99', 4),
(11000, '137.50', '137.50', '11999.99', 5),
(12000, '150.00', '150.00', '12999.99', 6),
(13000, '162.50', '162.50', '13999.99', 7),
(14000, '175.00', '175.00', '14999.99', 8),
(15000, '187.50', '187.50', '15999.99', 9),
(16000, '200.00', '200.00', '16999.99', 10),
(17000, '212.50', '212.50', '17999.99', 11),
(18000, '225.00', '225.00', '18999.99', 12),
(19000, '237.50', '237.50', '19999.99', 13),
(20000, '250.00', '250.00', '20999.99', 14),
(21000, '262.50', '262.50', '21999.99', 15),
(22000, '275.00', '275.00', '22999.99', 16),
(23000, '287.50', '287.50', '23999.99', 17),
(24000, '300.00', '300.00', '24999.99', 18),
(25000, '312.50', '312.50', '25999.99', 19),
(26000, '325.00', '325.00', '26999.99', 20),
(27000, '337.50', '337.50', '27999.99', 21),
(28000, '350.00', '350.00', '28999.99', 22),
(29000, '362.50', '362.50', '29999.99', 23),
(30000, '375.00', '375.00', '30999.99', 24),
(31000, '387.50', '387.50', '31999.99', 25),
(32000, '400.00', '400.00', '32999.99', 26),
(33000, '412.50', '412.50', '33999.99', 27),
(34000, '425.00', '425.00', '34999.99', 28),
(35000, '437.50', '437.50', '99999999.99', 29);

-- --------------------------------------------------------

--
-- Table structure for table `positionstb`
--

CREATE TABLE IF NOT EXISTS `positionstb` (
  `PosId` int(11) NOT NULL AUTO_INCREMENT,
  `Description` text NOT NULL,
  `BasicPay` int(11) NOT NULL,
  `Company` text NOT NULL,
  `Department` text NOT NULL,
  `Definition` varchar(500) NOT NULL,
  PRIMARY KEY (`PosId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `positionstb`
--

INSERT INTO `positionstb` (`PosId`, `Description`, `BasicPay`, `Company`, `Department`, `Definition`) VALUES
(1, 'Admin', 300, 'eBiZolution', 'Admin and Finance', 'Petty Cash Custodian, Payment and Collection Transaction, E-Link Transaction, EFPS â€“ BIR Transaction, QuickBooks Encoding, HR Transactions, Correspondence and Bidding'),
(2, 'Accountant ', 11000, 'eBiZolution', 'Admin and Finance', 'Preparation of Financial Statements, Balance Sheet, Income Statement, Trial Balance, Making Adjustments and Other Year End Reports and Auditing.'),
(3, 'Technical Associate ', 14500, 'eBiZolution', 'IT', 'Client Support, Implementer, Administrator(Client), IT Support'),
(4, 'Support Engineer  ', 400, 'Optomization', 'Opto', 'Null'),
(5, 'Senior Support Engineer ', 500, 'Optomization', 'Opto', 'Null'),
(6, 'Technical Pre-Sales ', 11000, 'eBiZolution', 'Sales', 'Product Presenter, Proof of Concept, Creation Technical Proposal'),
(7, 'Operations Supervisor ', 350, 'eBiZolution', 'IT', 'Handles the management of the technical operations within the company'),
(8, 'Chief Technology Officer ', 350, 'eBiZolution', 'CTO', 'Null'),
(9, 'Chief Operation Officer ', 500, 'eBiZolution', 'COO', 'Handles everything about sales'),
(10, 'Chief Executive Officer ', 500, 'eBiZolution', 'CEO', 'Null'),
(11, 'Chief Finance Officer', 350, 'eBiZolution', 'Admin and Finance', 'Approves and Manages the outputs of the Accounting People as well as the budgets and bidding matters'),
(12, 'Human Resource Manager', 21000, 'eBiZolution', 'Admin and Finance', ''),
(13, 'Front Desk Manager', 5000, 'eBiZolution', 'Frontdesk', 'Manages the Secretarial Duties for all the Employees'),
(14, 'Chief Design Supervisor', 12000, 'Kreativo Koncepto', 'Design and Creatives', 'Null'),
(15, 'Technological Supervisor', 12000, 'eBiZolution', 'IT', 'Manages everything Technical');

-- --------------------------------------------------------

--
-- Table structure for table `recviewtb`
--

CREATE TABLE IF NOT EXISTS `recviewtb` (
  `LevelId` int(11) NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`LevelId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recviewtb`
--

INSERT INTO `recviewtb` (`LevelId`, `Description`) VALUES
(1, 'Insertion of Personal Data'),
(2, 'Initial Screening'),
(3, 'Initial Interview'),
(4, 'Written Examination'),
(5, 'Secondary Interview (Supervisor)'),
(6, 'Tertiary Interview (Division Head)'),
(7, 'Final Interview (President)'),
(8, 'Background Check and Medical Exam'),
(9, 'Submission of Requirements'),
(10, 'Officially Hired!');

-- --------------------------------------------------------

--
-- Table structure for table `skillstb`
--

CREATE TABLE IF NOT EXISTS `skillstb` (
  `SkillsID` int(11) NOT NULL AUTO_INCREMENT,
  `EmpId` int(11) NOT NULL,
  `skills` varchar(50) NOT NULL,
  PRIMARY KEY (`SkillsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `skillstb`
--

INSERT INTO `skillstb` (`SkillsID`, `EmpId`, `skills`) VALUES
(1, 6, 'Microsoft Storage Certified');

-- --------------------------------------------------------

--
-- Table structure for table `ssstb`
--

CREATE TABLE IF NOT EXISTS `ssstb` (
  `Record` int(2) DEFAULT NULL,
  `MinRange` decimal(7,2) DEFAULT NULL,
  `MaxRange` decimal(11,2) DEFAULT NULL,
  `ER` decimal(6,2) DEFAULT NULL,
  `EE` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ssstb`
--

INSERT INTO `ssstb` (`Record`, `MinRange`, `MaxRange`, `ER`, `EE`) VALUES
(1, '0.00', '999.99', '0.00', '0.00'),
(2, '1000.00', '1249.99', '83.70', '36.30'),
(3, '1250.00', '1749.99', '120.50', '54.50'),
(4, '1750.00', '2249.99', '157.30', '72.70'),
(5, '2250.00', '2749.99', '194.20', '90.80'),
(6, '2750.00', '3249.99', '231.00', '109.00'),
(7, '3250.00', '3749.99', '267.80', '127.20'),
(8, '3750.00', '4249.99', '304.70', '145.30'),
(9, '4250.00', '4749.99', '341.50', '163.50'),
(10, '4750.00', '5249.99', '378.30', '181.70'),
(11, '5250.00', '5749.99', '415.20', '199.80'),
(12, '5750.00', '6249.99', '452.00', '218.00'),
(13, '6250.00', '6749.99', '488.80', '236.20'),
(14, '6750.00', '7249.99', '525.70', '254.30'),
(15, '7250.00', '7749.99', '562.50', '272.50'),
(16, '7750.00', '8249.99', '599.30', '290.70'),
(17, '8250.00', '8749.99', '636.20', '308.80'),
(18, '8750.00', '9249.99', '673.00', '327.00'),
(19, '9250.00', '9749.99', '709.80', '345.20'),
(20, '9750.00', '10249.99', '746.70', '363.30'),
(21, '10250.00', '10749.99', '783.50', '381.50'),
(22, '10750.00', '11249.99', '820.30', '399.70'),
(23, '11250.00', '11749.99', '857.20', '417.80'),
(24, '11750.00', '12249.99', '894.00', '436.00'),
(25, '12250.00', '12749.99', '930.80', '454.20'),
(26, '12750.00', '13249.99', '967.70', '472.30'),
(27, '13250.00', '13749.99', '1004.50', '490.50'),
(28, '13750.00', '14249.99', '1041.30', '508.70'),
(29, '14250.00', '14749.99', '1078.20', '526.80'),
(30, '14750.00', '15249.99', '1135.00', '545.00'),
(31, '15250.00', '15749.99', '1171.80', '563.20'),
(32, '15750.00', '999999999.00', '1208.70', '581.30');

-- --------------------------------------------------------

--
-- Table structure for table `taxtb`
--

CREATE TABLE IF NOT EXISTS `taxtb` (
  `taxablemin` decimal(7,2) DEFAULT NULL,
  `taxablemax` decimal(12,2) DEFAULT NULL,
  `status` varchar(7) DEFAULT NULL,
  `dependencies` int(1) DEFAULT NULL,
  `basetax` decimal(7,2) DEFAULT NULL,
  `perce` decimal(2,2) DEFAULT NULL,
  `exemp` decimal(7,2) DEFAULT NULL,
  `Record` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxtb`
--

INSERT INTO `taxtb` (`taxablemin`, `taxablemax`, `status`, `dependencies`, `basetax`, `perce`, `exemp`, `Record`) VALUES
('12083.00', '17916.99', 'Single', 1, '708.33', '0.20', '12083.00', 5),
('0.00', '6249.99', 'Single', 0, '0.00', '0.00', '1.00', 1),
('6250.00', '7082.99', 'Single', 0, '0.00', '0.05', '6250.00', 2),
('7083.00', '8749.99', 'Single', 0, '41.67', '0.10', '7083.00', 3),
('8750.00', '12082.99', 'Single', 0, '208.33', '0.15', '8750.00', 4),
('12083.00', '17916.99', 'Single', 0, '708.33', '0.20', '12083.00', 5),
('17917.00', '27082.99', 'Single', 0, '1875.00', '0.25', '17917.00', 6),
('27083.00', '47916.99', 'Single', 0, '4166.67', '0.30', '27083.00', 7),
('47917.00', '999999999.99', 'Single', 0, '10416.67', '0.32', '47917.00', 8),
('0.00', '4166.99', 'Married', 0, '0.00', '0.00', '1.00', 1),
('4167.00', '4999.99', 'Married', 0, '0.00', '0.05', '4167.00', 2),
('5000.00', '6666.99', 'Married', 0, '41.67', '0.10', '5000.00', 3),
('6667.00', '9999.99', 'Married', 0, '208.33', '0.15', '6667.00', 4),
('10000.00', '15832.99', 'Married', 0, '708.33', '0.20', '10000.00', 5),
('15833.00', '24999.99', 'Married', 0, '1875.00', '0.25', '15833.00', 6),
('25000.00', '45832.99', 'Married', 0, '4166.67', '0.30', '25000.00', 7),
('45833.00', '999999999.99', 'Married', 0, '10416.67', '0.32', '45833.00', 8),
('0.00', '6249.99', 'Single', 1, '0.00', '0.00', '1.00', 1),
('6250.00', '7082.99', 'Single', 1, '0.00', '0.05', '6250.00', 2),
('7083.00', '8749.99', 'Single', 1, '41.67', '0.10', '7083.00', 3),
('8750.00', '12082.99', 'Single', 1, '208.33', '0.15', '8750.00', 4),
('17917.00', '27082.99', 'Single', 1, '1875.00', '0.25', '17917.00', 6),
('27083.00', '47916.99', 'Single', 1, '4166.67', '0.30', '27083.00', 7),
('47917.00', '999999999.99', 'Single', 1, '10416.67', '0.32', '47917.00', 8),
('0.00', '8332.99', 'Single', 2, '0.00', '0.00', '1.00', 1),
('8333.00', '9166.99', 'Single', 2, '0.00', '0.05', '8333.00', 2),
('9167.00', '10832.99', 'Single', 2, '41.67', '0.10', '9167.00', 3),
('10833.00', '14166.99', 'Single', 2, '208.33', '0.15', '10833.00', 4),
('14167.00', '19999.99', 'Single', 2, '708.33', '0.20', '14167.00', 5),
('20000.00', '29166.99', 'Single', 2, '1875.00', '0.25', '20000.00', 6),
('29167.00', '49999.99', 'Single', 2, '4166.67', '0.30', '29167.00', 7),
('50000.00', '9999999999.99', 'Single', 2, '10416.67', '0.32', '50000.00', 8),
('0.00', '10416.99', 'Single', 3, '0.00', '0.00', '1.00', 1),
('10417.00', '11249.99', 'Single', 3, '0.00', '0.05', '10417.00', 2),
('11250.00', '12916.99', 'Single', 3, '41.67', '0.10', '11250.00', 3),
('12917.00', '16249.99', 'Single', 3, '208.33', '0.15', '12917.00', 4),
('16250.00', '22829.99', 'Single', 3, '708.33', '0.20', '16250.00', 5),
('22830.00', '31249.99', 'Single', 3, '1875.00', '0.25', '22083.00', 6),
('31250.00', '52082.99', 'Single', 3, '4166.67', '0.30', '31250.00', 7),
('52083.00', '9999999999.99', 'Single', 3, '10416.67', '0.32', '52083.00', 8),
('0.00', '6249.99', 'Married', 1, '0.00', '0.00', '1.00', 1),
('6250.00', '7082.99', 'Married', 1, '0.00', '0.05', '6250.00', 2),
('7083.00', '8749.99', 'Married', 1, '41.67', '0.10', '7083.00', 3),
('8750.00', '12082.99', 'Married', 1, '208.33', '0.15', '8750.00', 4),
('12083.00', '17916.99', 'Married', 1, '708.33', '0.20', '12083.00', 5),
('17917.00', '27082.99', 'Married', 1, '1875.00', '0.25', '17917.00', 6),
('27083.00', '47916.99', 'Married', 1, '4166.67', '0.30', '27083.00', 7),
('47917.00', '999999999.99', 'Married', 1, '10416.67', '0.32', '47917.00', 8),
('0.00', '8332.99', 'Married', 2, '0.00', '0.00', '1.00', 1),
('8333.00', '9166.99', 'Married', 2, '0.00', '0.05', '8333.00', 2),
('9167.00', '10832.99', 'Married', 2, '41.67', '0.10', '9167.00', 3),
('10833.00', '14166.99', 'Married', 2, '208.33', '0.15', '10833.00', 4),
('14167.00', '19999.99', 'Married', 2, '708.33', '0.20', '14167.00', 5),
('20000.00', '29166.99', 'Married', 2, '1875.00', '0.25', '20000.00', 6),
('29167.00', '49999.99', 'Married', 2, '4166.67', '0.30', '29167.00', 7),
('50000.00', '9999999999.99', 'Married', 2, '10416.67', '0.32', '50000.00', 8),
('0.00', '10416.99', 'Married', 3, '0.00', '0.00', '1.00', 1),
('10417.00', '11249.99', 'Married', 3, '0.00', '0.05', '10417.00', 2),
('11250.00', '12916.99', 'Married', 3, '41.67', '0.10', '11250.00', 3),
('12917.00', '16249.99', 'Married', 3, '208.33', '0.15', '12917.00', 4),
('16250.00', '22829.99', 'Married', 3, '708.33', '0.20', '16250.00', 5),
('22830.00', '31249.99', 'Married', 3, '1875.00', '0.25', '22083.00', 6),
('31250.00', '52082.99', 'Married', 3, '4166.67', '0.30', '31250.00', 7),
('52083.00', '9999999999.99', 'Married', 3, '10416.67', '0.32', '52083.00', 8);

-- --------------------------------------------------------

--
-- Table structure for table `tempuploadstb`
--

CREATE TABLE IF NOT EXISTS `tempuploadstb` (
  `UploadId` int(11) NOT NULL AUTO_INCREMENT,
  `TicketId` int(11) NOT NULL,
  `Location` text NOT NULL,
  `Name` text NOT NULL,
  PRIMARY KEY (`UploadId`),
  KEY `TicketId` (`TicketId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `tempuploadstb`
--

INSERT INTO `tempuploadstb` (`UploadId`, `TicketId`, `Location`, `Name`) VALUES
(1, 1, '../Uploads/Pictures/0a7b1b9.jpg', '0a7b1b9.jpg'),
(4, 2, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(5, 3, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(6, 4, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(7, 5, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(8, 6, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(9, 7, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(10, 8, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(11, 9, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(12, 10, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(13, 11, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(14, 12, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(15, 13, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(16, 14, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(17, 15, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(18, 16, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(19, 17, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(20, 18, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(21, 19, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(22, 20, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(23, 21, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(24, 22, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(25, 23, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(26, 24, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(27, 25, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(28, 26, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(29, 27, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(30, 28, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(31, 29, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(32, 30, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(33, 31, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(34, 32, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(35, 33, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(36, 34, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(37, 35, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png'),
(38, 36, '../Uploads/Pictures/765-default-avatar.png', '765-default-avatar.png');

-- --------------------------------------------------------

--
-- Table structure for table `testtb`
--

CREATE TABLE IF NOT EXISTS `testtb` (
  `testID` int(11) NOT NULL AUTO_INCREMENT,
  `testname` text NOT NULL,
  PRIMARY KEY (`testID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `testtb`
--

INSERT INTO `testtb` (`testID`, `testname`) VALUES
(23, 'Job Responsibilities Evaluation'),
(22, 'Qualitative Factors Evaluation'),
(24, 'Employee Satisfaction Evaluation'),
(25, 'Team Performance Evaluation'),
(26, 'Management Performance Evaluation'),
(27, 'Employee Benefits Evaluation'),
(28, 'Supervisor Performance Evaluation'),
(29, 'Team Progress Evaluation');

-- --------------------------------------------------------

--
-- Table structure for table `ticketeductb`
--

CREATE TABLE IF NOT EXISTS `ticketeductb` (
  `EducId` int(11) NOT NULL AUTO_INCREMENT,
  `TicketId` int(11) NOT NULL,
  `School` text NOT NULL,
  `Date` date NOT NULL,
  `Qualification` text NOT NULL,
  PRIMARY KEY (`EducId`),
  KEY `TicketId` (`TicketId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `ticketeductb`
--

INSERT INTO `ticketeductb` (`EducId`, `TicketId`, `School`, `Date`, `Qualification`) VALUES
(1, 3, 'Pamantasan ng Lungsod ng Maynila', '2011-11-30', 'asd'),
(2, 2, 'Pamantasan ng Lungsod ng Maynila', '2014-11-29', 'a'),
(3, 3, 'Pamantasan ng Lungsod ng Maynila', '2007-10-29', 'sd'),
(4, 4, 'Adamson University', '2005-10-29', 'asd'),
(5, 5, 'Pamantasan ng Lungsod ng Maynila', '2007-07-28', 'asd'),
(6, 6, 'Adamson University', '2014-10-29', 'sad'),
(7, 7, 'Pamantasan ng Lungsod ng Maynila', '2006-10-29', 'asd'),
(8, 8, 'Pamantasan ng Lungsod ng Maynila', '2007-10-29', 'as'),
(9, 9, 'Adamson University', '2007-11-29', 'ada'),
(10, 10, 'Pamantasan ng Lungsod ng Maynila', '2005-11-29', 'ad'),
(11, 11, 'Pamantasan ng Lungsod ng Maynila', '2011-11-30', 'asd'),
(12, 12, 'Pamantasan ng Lungsod ng Maynila', '2013-10-28', 'sds'),
(13, 13, 'Adamson University', '2014-03-04', 'dgdg'),
(14, 14, 'Adamson University', '2011-02-03', 'dgd'),
(15, 15, 'Pamantasan ng Lungsod ng Maynila', '2015-08-03', 'dsdf'),
(16, 16, 'Adamson University', '2015-08-20', 'gddf'),
(17, 17, 'Pamantasan ng Lungsod ng Maynila', '2015-08-14', 'sfffs'),
(18, 18, 'AdU', '2015-07-31', 'ddsf'),
(19, 19, 'Pamantasan ng Lungsod ng Maynila', '2015-08-04', 'gdfd'),
(20, 20, 'Adamson University', '2015-07-30', 'cdcdc'),
(21, 21, 'Pamantasan ng Lungsod ng Maynila', '2015-08-19', 'dffds'),
(22, 22, 'Pamantasan ng Lungsod ng Maynila', '2015-12-31', 'njhh'),
(23, 23, 'Adamson University', '2015-11-30', 'nnm,'),
(24, 24, 'Adamson University', '2014-12-30', 'ndsdm'),
(25, 25, 'Adamson University', '2015-07-27', 'dnddm'),
(26, 26, 'Pamantasan ng Lungsod ng Maynila', '2011-12-26', 'mdmd'),
(27, 27, 'Adamson University', '2012-11-29', 'ndsmdk'),
(28, 28, 'Adamson University', '2014-08-31', 'sdad'),
(29, 29, 'Adamson University', '2013-12-29', 'dns'),
(30, 30, 'Pamantasan ng Lungsod ng Maynila', '2011-08-29', 'sffs'),
(31, 31, 'Adamson University', '2009-11-26', 'sfggg'),
(32, 32, 'Adamson University', '2011-11-28', 'ndkdm'),
(33, 33, 'Adamson University', '2011-12-30', 'mdnd'),
(34, 34, 'Pamantasan ng Lungsod ng Maynila', '2007-11-29', 'dsdd'),
(35, 35, 'De La Salle College Of Saint Benilide', '2015-08-24', 'SAP Certificate Holder'),
(36, 36, 'Adamson University', '1997-01-01', 'BSCS');

-- --------------------------------------------------------

--
-- Table structure for table `ticketpersonaltb`
--

CREATE TABLE IF NOT EXISTS `ticketpersonaltb` (
  `InfoId` int(11) NOT NULL AUTO_INCREMENT,
  `TicketId` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Address` text NOT NULL,
  `CellNum` bigint(20) NOT NULL,
  `Mstatus` text NOT NULL,
  `Email` text NOT NULL,
  `Dob` date NOT NULL,
  `Company` text NOT NULL,
  `Pagibig` varchar(20) NOT NULL,
  `SSS` varchar(20) NOT NULL,
  `Tin` varchar(20) NOT NULL,
  `Philhealth` varchar(20) NOT NULL,
  `PosId` int(11) NOT NULL,
  `Dependency` int(11) DEFAULT NULL,
  PRIMARY KEY (`InfoId`),
  KEY `TicketId` (`TicketId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `ticketpersonaltb`
--

INSERT INTO `ticketpersonaltb` (`InfoId`, `TicketId`, `Name`, `Address`, `CellNum`, `Mstatus`, `Email`, `Dob`, `Company`, `Pagibig`, `SSS`, `Tin`, `Philhealth`, `PosId`, `Dependency`) VALUES
(1, 1, 'Nathaniel Marquez', '900 San Marcelino', 905702796, 'Single', 'nmarquez@ebizolution.com', '2014-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000009', 10, 0),
(2, 2, 'Rodolfo Gobrin', '900 San Marcelino', 905702796, 'Married', 'rgobrin@ebizolution.com', '2013-11-30', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 9, 0),
(3, 3, 'Karen Mae Delmendo', 'Makati City', 905702796, 'Single', 'kdelmendo@ebizolution.com', '2013-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 6, 0),
(4, 4, 'Remedios Villanueva', '900 San Marcelino', 905702796, 'Married', 'rvillanueva@ebizolution.com', '2007-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 2, 0),
(5, 5, 'Jesusa Benecio', '900 San Marcelino', 905702796, 'Married', 'jbenecio@ebizolution.com', '2012-09-30', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 2, 0),
(6, 6, 'Arnold Algere', '900 San Marcelino', 905702796, 'Married', 'aalegre@ebizolution.com', '2009-08-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 9, 0),
(7, 7, 'Efraem Velasco', 'Makati City', 905702796, 'Single', 'evelasco@ebizolution.com', '2003-10-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(8, 8, 'Diana Jane Marquez', '900 San Marcelino', 905702796, 'Single', 'dmarquez@ebizolution.com', '2005-08-28', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 1, 0),
(9, 9, 'Adrian Garcia', '900 San Marcelino', 905702796, 'Single', 'agarcia@ebizolution.com', '2005-09-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(10, 10, 'Melvin De Vera', '900 San Marcelino', 905702796, 'Single', 'mdevera@ebizolution.com', '2009-11-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, 0),
(11, 11, 'Regel Urbano', '900 San Marcelino', 905702796, 'Single', 'rurbano@ebizolution.com', '2006-11-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 6, 0),
(12, 12, 'Joselito Panahon', '900 San Marcelino', 905702796, 'Single', 'jpanahon@ebizolution.com', '2012-09-29', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, NULL),
(13, 13, 'Mark Dominic Daval-Santos', 'Manila City', 905702796, 'Single', 'msantos@ebizolution.com', '2016-03-05', 'eBiZolution', '00000000000', '00000000000', '00000000000', '00000000000', 6, NULL),
(14, 14, 'Al-Jhay Kevin Tadeo', 'Makati City', 0, 'Married', 'atadeo@ebizolution.com', '2013-02-25', 'eBiZolution', '11111111111', '90909090909', '00000000000', '00000000000', 8, NULL),
(15, 15, 'Rochelle De Guzman', 'Makati City', 0, 'Single', 'rdeguzman@ebizolution.com', '2015-02-03', 'eBiZolution', '00000000000', '33144994850', '00000000000', '11111111111', 0, NULL),
(16, 16, 'Babay Akmad', 'Makati City', 905702796, 'Single', 'bakmad@ebizolution.com', '2015-07-30', 'eBiZolution', '00000000000', '12341212312', '23432525345', '00000000000', 3, NULL),
(17, 17, 'JohnMar Pipino', 'Makati City', 905702796, 'Single', 'jpipino@ebizolution.com', '2015-08-27', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, NULL),
(18, 18, 'Christian Jay Balgos', 'Makati City', 905702796, 'Single', 'cbalgos@ebizolution.com', '2015-07-31', 'eBiZolution', '00000000000', '33144994850', '00000000000', '00000000000', 3, NULL),
(19, 19, 'Julius Cesar Arabe', 'Makati City', 905702796, 'Single', 'jarabe@ebizolution.com', '2015-08-07', 'eBiZolution', '00000000000', '90909090909', '00000000000', '00000000000', 3, NULL),
(20, 20, 'Maria Christina Marquez', 'Makati City', 905702796, 'Single', 'cmarquez@kki.com', '2015-08-19', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(21, 21, 'Rhoda Zaguirre', 'Makati City', 905702796, 'Single', 'rzaguirre@kki.com', '2015-08-06', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(22, 22, 'Maria Evangeline Baranquiel', 'Makati City', 905702796, 'Single', 'ebaranquiel@kki.com', '2015-08-04', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(23, 23, 'Maria Erica Simon', 'Makati City', 905702796, 'Single', 'esimon@kki.com', '2015-07-30', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(24, 24, 'Jonathan Zaguirre', 'Makati City', 905702796, 'Single', 'jzaguirre@kki.com', '2015-12-30', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(25, 25, 'Dennis Simon', 'Makati City', 905702796, 'Single', 'dsimon@kki.com', '2015-08-07', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(26, 26, 'Jymbee Zaguirre', 'Makati City', 905702796, 'Single', 'jyzaguirre@kki.com', '2012-12-30', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(27, 27, 'Jeff Ventura', 'Cavite City', 905702796, 'Single', 'jventura@kki.com', '2008-11-24', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(28, 28, 'Jackielyn Cano', 'Cavite City', 905702796, 'Married', 'jcano@kki.com', '2013-11-22', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(29, 29, 'Angel Nevarez', 'Makati City', 905702796, 'Single', 'anevarez@kki.com', '2015-09-09', 'Kreativo Koncepto', '00000000000', '33144994850', '00000000000', '00000000000', 4, NULL),
(30, 30, 'Ponciano Tugonon Jr. ', 'Makati City', 0, 'Single', 'ptugonon@kki.com', '2012-11-28', 'Optomization', '00000000000', '33144994850', '00000000000', '00000000000', 5, NULL),
(31, 31, 'Ricky Pardillo', 'Makati City', 905702796, 'Married', 'rpardillo@kki.com', '2012-11-28', 'Optomization', '00000000000', '90909090909', '00000000000', '00000000000', 5, NULL),
(32, 32, 'Mark Ronald Epino', 'Makati City', 905702796, 'Married', 'mepino@kki.com', '2010-12-02', 'Optomization', '00000000000', '33144994850', '00000000000', '00000000000', 5, NULL),
(33, 33, 'Niko Franz Gepiga', 'Makati City', 905702796, 'Married', 'ngepiga@kki.com', '1995-11-30', 'Optomization', '00000000000', '33144994850', '00000000000', '00000000000', 5, NULL),
(34, 34, 'Venice Jeremaiah Munar', 'Cavite City', 905702796, 'Single', 'vmunar@kki.com', '1973-07-29', 'Optomization', '00000000000', '33144994850', '00000000000', '11111111111', 5, NULL),
(35, 35, 'Carl Domenique Millan Gabuelo', '21 Street Sto Nino Pque City', 9267021992, 'Single', 'blckwolf@gmail.com', '1992-07-19', 'eBiZolution', '00000000000', '00000000000', '00000000000', '00000000000', 3, NULL),
(36, 36, 'Nelson Casambros', '900 San Marcelino', 905702796, 'Single', 'allen@gmail.com', '1976-03-13', 'eBiZolution', '00000000000', '00000000000', '00000000000', '00000000000', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ticketskillstb`
--

CREATE TABLE IF NOT EXISTS `ticketskillstb` (
  `SkillsID` int(20) NOT NULL AUTO_INCREMENT,
  `TicketId` int(20) NOT NULL,
  `skills` varchar(500) NOT NULL,
  PRIMARY KEY (`SkillsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ticketskillstb`
--


-- --------------------------------------------------------

--
-- Table structure for table `tickettb`
--

CREATE TABLE IF NOT EXISTS `tickettb` (
  `TicketId` int(11) NOT NULL,
  `Level` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `Remarks` text NOT NULL,
  PRIMARY KEY (`TicketId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickettb`
--

INSERT INTO `tickettb` (`TicketId`, `Level`, `Status`, `Remarks`) VALUES
(1, 10, 1, 'None'),
(2, 10, 1, 'None'),
(3, 10, 1, 'None'),
(4, 10, 1, 'None'),
(5, 10, 1, 'None'),
(6, 10, 1, 'None'),
(7, 10, 1, 'None'),
(8, 10, 1, 'None'),
(9, 10, 1, 'None'),
(10, 10, 1, 'None'),
(11, 10, 1, 'None'),
(12, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(13, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(14, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(15, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(16, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(17, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(18, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(19, 10, 1, 'Initial Screening: \nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(20, 10, 1, 'Initial Screening: Good!\nInitial Interview: \nWritten Examination: \nSecondary Interview (Supervisor): \nTertiary Interview (Division Head): \nFinal Interview (President): \nBackground Check and Medical Exam: '),
(21, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(22, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(23, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(24, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(25, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(26, 10, 1, 'Initial Screening: Very Good!\nInitial Interview: Very Good!\nWritten Examination: Very Good!\nSecondary Interview (Supervisor): Very Good!\nTertiary Interview (Division Head): Very Good!\nFinal Interview (President): Very Good!\nBackground Check and Medical Exam: Very Good!'),
(27, 10, 1, 'Initial Screening: Very Good!\nInitial Interview: Very Good!\nWritten Examination: Very Good!\nSecondary Interview (Supervisor): Very Good!\nTertiary Interview (Division Head): Very Good!\nFinal Interview (President): Very Good!\nBackground Check and Medical Exam: Very Good!'),
(28, 10, 1, 'Initial Screening: Very Good!\nInitial Interview: Very Good!\nWritten Examination: Very Good!\nSecondary Interview (Supervisor): Very Good!\nTertiary Interview (Division Head): Very Good!\nFinal Interview (President): Very Good!\nBackground Check and Medical Exam: Very Good!'),
(29, 10, 1, 'Initial Screening: Very Good!\nInitial Interview: Very Good!\nWritten Examination: Very Good!\nSecondary Interview (Supervisor): Very Good!\nTertiary Interview (Division Head): Very Good!\nFinal Interview (President): Very Good!\nBackground Check and Medical Exam: Very Good!'),
(30, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(31, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(32, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(33, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(34, 10, 1, 'Initial Screening: Good!\nInitial Interview: Good!\nWritten Examination: Good!\nSecondary Interview (Supervisor): Good!\nTertiary Interview (Division Head): Good!\nFinal Interview (President): Good!\nBackground Check and Medical Exam: Good!'),
(35, 10, 1, 'None'),
(36, 5, 1, 'Initial Screening: Good in speaking and Confident\nInitial Interview: Ok\nWritten Examination: ');

-- --------------------------------------------------------

--
-- Table structure for table `ticketworktb`
--

CREATE TABLE IF NOT EXISTS `ticketworktb` (
  `WorkId` int(11) NOT NULL AUTO_INCREMENT,
  `TicketId` int(11) NOT NULL,
  `Employer` text NOT NULL,
  `Address` text NOT NULL,
  `DateStart` date NOT NULL,
  `DateEnd` date NOT NULL,
  `Position` text NOT NULL,
  `Reason` text NOT NULL,
  PRIMARY KEY (`WorkId`),
  KEY `TicketId` (`TicketId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ticketworktb`
--

